# pylint: disable=invalid-name
"""Query JIRA for MRs."""
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
import re
import sys
import typing

from cki_lib.logger import get_logger

from webhook import common
from webhook import defs
from webhook import libbz
from webhook import libjira
from webhook import table
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin
from webhook.base_mr_mixins import DependsMixin
from webhook.base_mr_mixins import OwnersMixin
from webhook.base_mr_mixins import RHIssueMixin
from webhook.bug import Bug
from webhook.rhissue import RHIssue
from webhook.session import SessionRunner
from webhook.session_events import GitlabMREvent
from webhook.session_events import GitlabNoteEvent
from webhook.session_events import UmbBridgeEvent

LOGGER = get_logger('cki.webhook.jirahook')

COMMENT_TITLE = '**JIRA Hook Readiness Report**'
COMMENT_FOOTER = ("Guidelines for these entries can be found in CommitRules: "
                  "https://red.ht/kwf_commit_rules.  \nTo request re-evalution either remove the "
                  "JIRA label from the MR or add a comment with only the text: "
                  "request-jirahook-evaluation.")


def make_bugs(bug_list, mrs):
    """Return a list of Bug objects with the MR set."""
    if not bug_list:
        return []
    bz_data = libbz.fetch_bugs(bug_list)
    missing_ids = libbz.get_missing_bugs(bug_list, bz_data)
    return [Bug.new_from_bz(bz=bz, mrs=mrs) for bz in bz_data] + \
           [Bug.new_missing(bz_id=bz_id, mrs=mrs) for bz_id in missing_ids]


# The DependsMixin expects any additional fields to have kw_only set!
@dataclass(repr=False, kw_only=True)
class JiraMR(RHIssueMixin, OwnersMixin, DependsMixin, CommitsMixin, BaseMR):
    # pylint: disable=too-many-ancestors
    """Store for Jira MR data."""

    labels_to_add: list[str] = field(default_factory=list, init=False)
    labels_to_remove: list[str] = field(default_factory=list, init=False)

    @cached_property
    def rhissues(self) -> list[RHIssue]:
        """Return the list of all RHIssue objects derived from this MR.

        Includes possible INTERNAL/UNTAGGED objects not handled in the base mixin rhissues.
        """
        rhissues = super().rhissues

        all_mrs = [self] + self.depends_mrs
        if self.has_internal:
            rhissues.append(RHIssue.new_internal(mrs=all_mrs))
        if self.has_untagged:
            rhissues.append(RHIssue.new_untagged(mrs=all_mrs))

        return rhissues

    @cached_property
    def closed_rhissues(self) -> list[RHIssue]:
        """Return the list of RHIssue objects from this MR that are in Closed state."""
        return [rhi for rhi in self.rhissues if rhi.ji_status is defs.JIStatus.CLOSED]

    @property
    def rhissues_with_scopes(self) -> list[RHIssue]:
        """Return the list of RHIssue objects after doing tests."""
        rhissues = self.rhissues
        for rhissue in rhissues:
            rhissue.set_scope()
        return rhissues

    @property
    def linked_rhissues_with_scopes(self) -> list[RHIssue]:
        """Return the list of RHIssue objects after doing tests."""
        rhissues = self.linked_rhissues
        for rhissue in rhissues:
            rhissue.set_scope()
        return rhissues

    @cached_property
    def cves(self) -> list[Bug]:
        """Return the list of BZ Cve objects derived from this MR's description only."""
        if self.is_build_mr:
            return []
        # In other words, CVE tags in the descriptions of dependency MRs are not considered.
        return make_bugs(self.description.cve, mrs=[self] + self.depends_mrs) if \
            self.description else []

    @property
    def cves_with_scopes(self) -> list[Bug]:
        """Return the list of Cves objects after doing tests."""
        cves = self.cves
        for cve in cves:
            cve.set_scope()
        return cves

    def spawn_testing_tasks(self) -> None:
        """Spawn any kernel variant testing Task issues needed."""
        if self.draft:
            return

        # If not a draft MR, spawn kernel variant testing tasks
        LOGGER.debug("Subsystems in this MR request kernel variant testing on: %s",
                     self.test_variants)

        # If there are no test_variants then don't call rhissue.spawn_testing_tasks.
        if not self.test_variants:
            return

        for rhissue in self.rhissues:
            rhissue.spawn_testing_tasks(self.test_variants)

    def sort_labels(self, mr_scope: defs.MrScope) -> None:
        """Sort out which labels need to be added and removed on this MR."""
        to_add = []
        to_remove = []

        def find_labels_to_remove(prefix, keepers):
            return [label for label in self.labels if
                    label.startswith(prefix) and label not in keepers]

        if cves := self.cves_with_scopes:
            highest_cve = max(cves, key=lambda jissue: jissue.severity)

            if highest_cve.severity.value:
                severity_suffix = highest_cve.severity.name.capitalize()
                to_add.append(f'{defs.SEVERITY_PREFIX}{severity_suffix}')
                to_remove = find_labels_to_remove(defs.SEVERITY_PREFIX, to_add)

        rhissues = [rhi for rhi in self.rhissues_with_scopes if rhi not in self.closed_rhissues]

        if any(rhi.ji_priority >= defs.JIPriority.MAJOR for rhi in rhissues):
            to_add.append(defs.TIME_CRITICAL_LABEL)
        else:
            to_remove += find_labels_to_remove(defs.TIME_CRITICAL_LABEL, to_add)

        for rhi in rhissues:
            if rhi.ji_pt_status in (defs.JIPTStatus.REQUESTED, defs.JIPTStatus.PASS):
                for task in rhi.testing_tasks:
                    task_label = f'{task.ji_component} testing::Requested'
                    if 'PrelimTestingPass' in task.failed_tests:
                        to_add.append(task_label)
                    else:
                        to_remove += find_labels_to_remove(task_label, to_add)

        to_add.append(mr_scope.label('JIRA'))

        LOGGER.info("Labels to add: %s, to remove: %s", to_add, to_remove)
        self.labels_to_add = to_add
        self.labels_to_remove = to_remove

    def update_statuses(self, all_issues, comment) -> None:
        """Wrap old API stuff to set the labels, check dependencies and set testing requested."""
        self.session.update_webhook_comment(self.gl_mr, comment,
                                            bot_name=self.session.gl_user.username,
                                            identifier=COMMENT_TITLE)

        # We don't need the info of fresh labels here
        self.remove_labels(self.labels_to_remove)

        current_labels = self.add_labels(self.labels_to_add)
        LOGGER.debug('Current MR labels: %s', current_labels)
        common.add_merge_request_to_milestone(self.rh_branch, self.gl_project, self.gl_mr)
        if defs.READY_FOR_QA_LABEL in current_labels:
            libjira.request_preliminary_testing(all_issues)


class TagRow(table.TableRow):
    # pylint: disable=too-few-public-methods
    """Generic functions for all Tag TableRow objects."""

    @staticmethod
    def _format_cve(cve_name):
        """Return the cve_name as a markdown link."""
        def make_cve_link(cve_match):
            """Return the CVE in the match object as a markdown link."""
            return f'[{cve_match.group()}](https://bugzilla.redhat.com/{cve_match.group()})'

        return re.sub(r'CVE-\d{4}-\d{4,7}', make_cve_link, cve_name)

    @staticmethod
    def _format_JIRA_Issues(rhissue):
        """Format the JIRA Issue field."""
        status_str = rhissue.ji_status.name
        if rhissue.ji_status is defs.JIStatus.CLOSED:
            status_str += f': {rhissue.ji_resolution.name}'
        return f'{rhissue.id} ({status_str})' if isinstance(rhissue.id, str) else rhissue.id

    def _format_CVEs(self, cves):
        """Format the CVEs field."""
        if not cves:
            cve_str = 'None'
        else:
            cve_str = ''
            for cve in cves:
                cve_str += f'{self._format_cve(cve)}<br>'
        return cve_str

    @staticmethod
    def _format_Commits(commits):
        """Format the list of commits."""
        if len(commits) > defs.MAX_COMMITS_PER_COMMENT_ROW:
            commits = list(commits)[:defs.MAX_COMMITS_PER_COMMENT_ROW]
            commits.append('(...)')
        return commits if commits else 'None'

    @staticmethod
    def _format_Notes(notes):
        """Format the Notes field."""
        return '<br>'.join(f'See {note}' for note in notes) if notes else '-'

    @staticmethod
    def _format_Policy_Check(issue):
        """Create a string for the Policy Check field."""
        check_passed, check_msg = issue.policy_check_ok
        if check_passed is None:
            return check_msg
        return 'Passed' if check_passed else 'Failed:<br>' + check_msg


class IssueRow(TagRow):
    """Format a TableRow for RHIssue objects."""

    def __init__(self):
        """Init."""
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Commits = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Notes = ''

    def populate(self, tag, footnotes):
        """Populate the row with data from a RHIssue tag."""
        jtag = f'[{tag.id}]({tag.jira_link}) ({tag.ji_status.name})'
        self.set_value('JIRA_Issue', jtag)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Commits', tag.commits)
        self.set_value('Readiness', tag.scope.name)
        self.set_value('Policy_Check', tag)
        self.set_value('Notes', footnotes)
        return self


class CveRow(TagRow):
    """Format a TableRow for Cve objects."""

    def __init__(self):
        """Init."""
        self.CVEs = ''
        self.Priority = ''
        self.Commits = ''
        self.Clones = ''
        self.Readiness = ''
        self.Notes = ''

    @staticmethod
    def _format_Clones(cve):
        """Format the Clones field."""
        # If no parent_mr we can't proceed.
        if not cve.parent_mr:
            return 'Unknown'
        # Skip over low prio CVEs and rhel-6 (it has no zstreams)
        if cve.bz_priority < defs.BZPriority.HIGH or cve.parent_mr.rh_project.name == 'rhel-6':
            return 'N/A'
        if not (clones := cve.jira_clones):
            return 'None'
        parent_clone = next(
            (issue for issue in clones if issue.ji_branch == cve.parent_mr.rh_branch), None
        )
        clones_str = ''
        for clone in clones:
            target = clone.ji_fix_version
            component = clone.ji_component or 'Unknown'
            status = clone.ji_status.name
            if clone.ji_status is defs.JIStatus.CLOSED:
                status += f' {clone.ji_resolution.name}'
            clone_url = f'[{clone.id}]({clone.jira_link})'
            clone_str = f'{target} ({component}): {clone_url} ({status})'
            clones_str += f'{clone_str}<br>' if parent_clone != clone else f'**{clone_str}**<br>'
        return clones_str

    def populate(self, cve, footnotes):
        """Populate the row with data from a CVE JIRA Issue."""
        self.set_value('CVEs', cve.cve_ids)
        self.set_value('Priority', cve.bz_priority.name.capitalize())
        self.set_value('Commits', cve.commits)
        self.set_value('Clones', cve)
        self.set_value('Readiness', cve.scope.name)
        self.set_value('Notes', footnotes)
        return self


class DepRow(TagRow):
    """Format a TableRow for RHIssue objects that are dependencies."""

    def __init__(self):
        """Init."""
        self.MR = ''
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Commits = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Notes = ''

    @staticmethod
    def _format_MR(rhissue):
        """Set the MR field."""
        return f'!{rhissue.mr.iid} ({rhissue.mr.rh_branch.name})' if rhissue.mr else '?'

    def populate(self, tag, footnotes):
        """Populate the row with data from a RHIssue tag."""
        jtag = f'[{tag.id}]({tag.jira_link}) ({tag.ji_status.name})'
        self.set_value('JIRA_Issue', jtag)
        self.set_value('MR', tag)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Commits', tag.commits)
        self.set_value('Notes', footnotes)
        if tag.mr.state is not defs.MrState.MERGED:
            self.set_value('Readiness', tag.scope.name)
            self.set_value('Policy_Check', tag)
        return self


class LinkedIssueRow(TagRow):
    """Format a TableRow for RHIssue objects."""

    def __init__(self):
        """Init."""
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Component = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Variant_Subsystems = ''
        self.Notes = ''

    def populate(self, tag, footnotes):
        """Populate the row with data from a RHIssue tag."""
        jtag = (f'[{tag.id}]({tag.jira_link}) '
                f'({tag.ji_type.name}: {tag.ji_status.name})')
        self.set_value('JIRA_Issue', jtag)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Component', tag.ji_component)
        self.set_value('Readiness', tag.scope.name)
        self.set_value('Policy_Check', tag)
        self.set_value('Variant_Subsystems', tag.subsystems_with_test_variants)
        self.set_value('Notes', footnotes)
        return self


def find_needed_footnotes(rhissue_list):
    """Return a dict of test_name: test.notes needed for the given list of Issues."""
    return {test_name: next((test.note for test in rhissue.test_list if test.__name__ == test_name))
            for rhissue in rhissue_list for test_name in rhissue.failed_tests}


def create_ji_table(row_class, rhissue_list):
    """Return a Table object populated with the given row class."""
    footnotes = find_needed_footnotes(rhissue_list) if rhissue_list else {}
    tag_table = table.Table(footnote_list=footnotes.values())
    for rhissue in rhissue_list:
        note_idx = sorted([list(footnotes.keys()).index(test) + 1 for test in rhissue.failed_tests])
        row = row_class().populate(rhissue, note_idx)
        tag_table.add_row(row)
    return tag_table


def generate_comment(this_mr, mr_scope, tables):
    """Generate the comment string."""
    status = 'fails' if mr_scope < defs.MrScope.READY_FOR_QA else 'passes'
    label = mr_scope.label("JIRA")
    total_rows = len(tables[0]) + len(tables[1]) + len(tables[2])
    issue_table = str(tables[0])
    dep_table = str(tables[1])
    cve_table = str(tables[2])
    links_table = str(tables[3])

    post = COMMENT_TITLE + '\n\n'

    if this_mr.is_build_mr:
        post += ('**Warning**: The jira evaluation done on Build MRs is somewhat limited. Any '
                 'CVEs and linked issues are ignored, along with INTERNAL checks, for performance '
                 'and sanity reasons. We assume that individual MRs were all readyForMerge when '
                 'they were added, and bypass these extra steps to allow a Build MR jira label to '
                 'resolve to OK more reliably.  \n\n')

    post += f'Target Branch: {this_mr.rh_branch.name}  \n\n'
    post += (f'This merge request **{status}** jirahook validation: '
             f'~"{label}"  \n\n')

    if total_rows > 10:
        post += '<details><summary>Click to expand</summary>  \n\n'
    if issue_table:
        post += '##### JIRA Issue tags:  \n' + issue_table + '\n'
    if dep_table:
        post += '##### Depends tags:  \n' + dep_table + '\n'
    if cve_table:
        post += '##### CVE tags:  \n' + cve_table + '\n'
    if links_table:
        post += '##### Linked JIRA Issues:  \n' + links_table + '\n'
    if total_rows > 10:
        post += '</details>  \n'

    post += '\n' + COMMENT_FOOTER
    return post


def get_lowest_scope(issues):
    """Return the lowest scope of all the RHIssues/CVEs."""
    scope = defs.MrScope.READY_FOR_MERGE
    for issue in issues:
        scope = min(scope, issue.scope)
    return scope


def process_mr(session, mr_url):
    # pylint: disable=too-many-locals
    """Process the given MR."""
    session.rh_projects.do_load_policies()

    # Fetch and parse MR data.
    this_mr = JiraMR.new(session, mr_url, source_path=session.args.repo_path, merge_subsystems=True)

    # Skip funny MRs.
    if not this_mr.description:
        LOGGER.info('This MR has no description, nothing to do.')
        return
    if not this_mr.commits:
        LOGGER.info('This MR has no non-merge commits, nothing to do.')
        return

    # mr_issues for our purposes here should only be the issues that correspond to a JIRA tag
    # in the MR Description, which are not Closed.
    mr_issues = [rhissue.ji for rhissue in this_mr.rhissues if
                 rhissue.ji and rhissue.id in this_mr.description.jira_tags and
                 rhissue not in this_mr.closed_rhissues]
    open_issues = mr_issues + [issue.ji for issue in this_mr.linked_rhissues]

    if open_issues and not this_mr.draft:
        # Move open_issues to Planning or In Progress.
        libjira.move_issue_states_forward(open_issues)
        # Add cross-reference to GitLab MR in JIRA Issue
        libjira.add_gitlab_link_in_issues(session, open_issues, this_mr)
    else:
        LOGGER.info('MR is in draft state, not moving JIRA Issue states forward.')

    # Get the JIRA issue lists with their scopes set
    rhissues = [rhi for rhi in this_mr.rhissues_with_scopes if rhi not in this_mr.closed_rhissues]
    cves = this_mr.cves_with_scopes
    # This call must be before getting linked_rhissues_with_scopes to reflect ITM/DTM updates
    libjira.sync_cve_issue_fields(rhissues, this_mr.linked_rhissues)
    linked_rhissues = this_mr.linked_rhissues_with_scopes

    all_rhissues = rhissues + this_mr.closed_rhissues
    # Generate a tuple of results tables: Issues, Deps, and CVEs.
    tables = (create_ji_table(IssueRow, [rhi for rhi in all_rhissues if rhi.mr is this_mr]),
              create_ji_table(DepRow, [rhi for rhi in all_rhissues if rhi.mr is not this_mr]),
              create_ji_table(CveRow, cves),
              create_ji_table(LinkedIssueRow, linked_rhissues)
              )

    mr_scope = get_lowest_scope(all_rhissues + cves + linked_rhissues)
    comment = generate_comment(this_mr, mr_scope, tables)

    this_mr.spawn_testing_tasks()
    this_mr.sort_labels(mr_scope)

    # Update the MR labels and comment
    this_mr.update_statuses(open_issues, comment)


def process_event(
    _: dict,
    session: SessionRunner,
    event: GitlabMREvent | GitlabNoteEvent | UmbBridgeEvent,
    **__: typing.Any
) -> None:
    """Process an event."""
    LOGGER.info('Processing %s event for %s', getattr(event, 'kind', event.type).name, event.mr_url)
    process_mr(session, event.mr_url)


HANDLERS = {
    GitlabMREvent: process_event,
    GitlabNoteEvent: process_event,
    UmbBridgeEvent: process_event
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('JIRAHOOK')
    args = parser.parse_args(args)
    if not args.repo_path:
        raise RuntimeError('--repo-path (REPO_PATH) must be set')

    session = SessionRunner.new('jirahook', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
