"""Clone or update the Linux Security Vulnerabilities repo."""
from argparse import ArgumentParser
from argparse import Namespace
import contextlib
import os
import subprocess
import sys
import time

from cki_lib import logger
from cki_lib import misc
import sentry_sdk

from .. import common

LOGGER = logger.get_logger('cki.webhook.utils.update_vulnerabilities')


# Shamelessly copied from update_git_mirror and trimmed/adapted for vulns.git
class VulnRepoManager:
    """Manage a local git mirror repo."""

    def __init__(self, args):
        """Create a new repo manager instance."""
        self.repo_path = args.vulns_path
        self.remote_url = args.vulns_url

    def _git(self, args, **kwargs):
        """Run the git executable in the local repo directory."""
        kwargs.setdefault('check', True)
        # pylint: disable=subprocess-run-check
        LOGGER.debug("Running command: git %s", " ".join(args))
        return subprocess.run(['git'] + args, cwd=self.repo_path, **kwargs)

    def clone(self):
        """Clone the vulns repo."""
        os.makedirs(self.repo_path, exist_ok=True)
        self._git(['clone', self.remote_url, self.repo_path])

    def fetch(self, only_log_exceptions=False):
        """Fetch all remote git repos."""
        LOGGER.info('Updating vulnerabilities from %s', self.remote_url)
        with misc.only_log_exceptions() if only_log_exceptions else contextlib.nullcontext():
            try:
                self._git(['fetch', 'origin'])
            except subprocess.CalledProcessError as err:
                LOGGER.warning('Fetch of origin failed: %s', err)

    def checkout(self, only_log_exceptions=False):
        """Reset the checkout to the first remote with checkout!=null."""
        LOGGER.info('Resetting checkout to origin/master')
        with misc.only_log_exceptions() if only_log_exceptions else contextlib.nullcontext():
            self._git(['reset', '--hard', 'origin/master'])

    def garbage_collection(self, force=False):
        """Run git garbage collection and repack."""
        LOGGER.info('Running git garbage collection')
        self._git(['gc'] + ([] if force else ['--auto']))
        self._git(['repack', '--geometric', '2', '-d'])


def _get_parser_args(args: list[str]) -> Namespace:
    """Set up arg parser, parse and return args."""
    parser = ArgumentParser(description='Clone or update the upstream vulns git repo')

    parser.add_argument('--sentry-ca-certs', default=os.getenv('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    parser.add_argument('--update-interval-minutes', type=int,
                        help='Continuously update with the given interval and ignore exceptions')
    parser.add_argument('--vulns-path', **common.get_argparse_environ_opts('VULNS_PATH'),
                        help='Local path where the git mirror repo is stored')
    parser.add_argument('--vulns-url', **common.get_argparse_environ_opts('VULNS_URL'),
                        help='URL for the Linux Security Vulnerabilities git repo')
    parser.add_argument('--force-gc', action='store_true', default=False,
                        help='Do not use --auto when running garbage collection')
    parser.add_argument('--mode', choices=('create-repo', 'update-repo'),
                        required=True, help='create a local repo via git clone (create-repo), '
                                            'or update a local repo (update-repo)')
    return parser.parse_args(args)


def main(args: list[str]) -> None:
    """Run main loop."""
    parsed_args = _get_parser_args(args)

    misc.sentry_init(sentry_sdk, ca_certs=parsed_args.sentry_ca_certs)

    repo_manager = VulnRepoManager(parsed_args)

    if parsed_args.mode == 'create-repo':
        LOGGER.info('Populating local vulnerabilities git repo mirror from %s',
                    parsed_args.vulns_url)
        repo_manager.clone()

    elif parsed_args.mode == 'update-repo':
        while True:
            LOGGER.info('Updating local vulnerabilities git repo mirror')
            repo_manager.fetch(only_log_exceptions=bool(parsed_args.update_interval_minutes))
            repo_manager.checkout(only_log_exceptions=bool(parsed_args.update_interval_minutes))

            if not parsed_args.update_interval_minutes:
                break

            LOGGER.info('Sleeping for %s minutes', parsed_args.update_interval_minutes)
            time.sleep(60 * parsed_args.update_interval_minutes)


if __name__ == "__main__":
    main(sys.argv[1:])
