"""Clone or update the owners file."""
import argparse
import os
import pathlib
import sys
import time

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
import sentry_sdk

from .. import common

LOGGER = logger.get_logger('cki.webhook.utils.update_owners')
SESSION = session.get_session('cki.webhook.utils.update_owners')


def download_file(url, local_path):
    """Download a file from GitLab."""
    response = SESSION.get(url,
                           headers={'Private-Token': os.environ["COM_GITLAB_RO_REPO_TOKEN"]})
    response.raise_for_status()
    temp_path = local_path + '.tmp'
    pathlib.Path(temp_path).write_bytes(response.content)
    os.replace(temp_path, local_path)


def main(args):
    """Run main loop."""
    parser = argparse.ArgumentParser(description='Clone or update the owners file')
    parser.add_argument('--sentry-ca-certs', default=os.getenv('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    parser.add_argument('--update-interval-minutes', type=int,
                        help='Continuously update with the given interval')
    parser.add_argument('--local-path', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Local path where the owners file is saved')
    parser.add_argument('--owners-url', **common.get_argparse_environ_opts('OWNERS_YAML_URL'),
                        help='URL of the owners file')
    args = parser.parse_args(args)

    misc.sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)

    while True:
        if os.path.isfile(args.local_path):
            LOGGER.info('Updating owners file')
            with misc.only_log_exceptions():
                download_file(args.owners_url, args.local_path)
        else:
            LOGGER.info('Cloning owners file')
            download_file(args.owners_url, args.local_path)

        if not args.update_interval_minutes:
            break

        LOGGER.info('Sleeping for %s minutes', args.update_interval_minutes)
        time.sleep(60 * args.update_interval_minutes)


if __name__ == "__main__":
    main(sys.argv[1:])
