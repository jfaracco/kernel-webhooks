#!/usr/bin/env python
"""Look for MRs that cannot be merged and/or are stale."""

from dataclasses import dataclass
from datetime import timedelta
from email.message import EmailMessage
import os
import re
import smtplib

from cki_lib import logger
from cki_lib import misc
from ldap import initialize
from ldap.filter import escape_filter_chars
from ldapurl import LDAP_SCOPE_SUBTREE
import sentry_sdk

from webhook import defs
from webhook.base_mr_mixins import DiscussionsMixin
from webhook.common import get_arg_parser
from webhook.jirahook import JiraMR
from webhook.rhissue_tests import AssigneeIsNotBot
from webhook.rhissue_tests import CentOSZStream
from webhook.rhissue_tests import DevApproved
from webhook.rhissue_tests import JIisNotClosed
from webhook.rhissue_tests import PrelimTestingPass
from webhook.rhissue_tests import QEApproved
from webhook.rhissue_tests import TargetReleaseSet
from webhook.session import SessionRunner

LOGGER = logger.get_logger('webhook.utils.report_generator')

KWF_METRICS_URL = ('https://docs.engineering.redhat.com/pages/viewpage.action?'
                   'spaceKey=RHELPLAN&title=KWF+Metrics')
NULL_SST = 'NO-RHEL-SST-FOUND'

# Get all open MRs.
MR_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after, targetBranches: $branches) {
      pageInfo {hasNextPage endCursor}
      nodes {
        iid
        author{username, name, email: publicEmail}
        title
        targetBranch
        webUrl
        project{name fullPath}
        updatedAt
        createdAt
        draft
        labels{nodes{title description}}
      }
    }
  }
}
"""


def get_open_mrs(session, namespace):
    """Return a list of the MR objects we're interested in."""
    gql = session.graphql
    result = gql.check_query_results(
        gql.client.query(MR_QUERY, variable_values={'namespace': namespace, 'branches': 'main'},
                         paged_key='project/mergeRequests'),
        check_keys={'project'})
    if result and result['project'] is None:
        raise RuntimeError(f"Namespace '{namespace}' is not visible!")
    mrs = {}
    for mreq in misc.get_nested_key(result, 'project/mergeRequests/nodes'):
        report_mr = ReportMR.new(session, mreq['webUrl'], source_path=session.args.repo_path)
        if report_mr.is_build_mr:
            LOGGER.info("Skipping maintainer build MR: %s", report_mr.title)
            continue
        mreq['report_mr'] = report_mr
        namespace = mreq['project']['fullPath']
        if namespace in mrs:
            mrs[namespace].append(mreq)
            continue
        mrs[namespace] = [mreq]

    LOGGER.debug('Project %s MRs: found %s', namespace, mrs)
    return mrs


def get_author_sst_from_ldap(pargs, author):
    """Fetch author's SST from ldap."""
    sst = ''
    email = escape_filter_chars(author.get('email') or '')
    name = escape_filter_chars(author.get('name') or '')

    ldap_server = f"ldap://{pargs.ldap_server}"
    base_dn = pargs.base_dn
    attrs = list(pargs.ldap_attrs.split())

    ldc = initialize(ldap_server, trace_level=0)
    ldc.simple_bind_s()

    if email and ('@redhat.com' in email or '@fedoraproject.org' in email):
        look_for = f"(|(rhatPreferredAlias={email})(rhatPrimaryMail={email}))"
    else:
        look_for = f"(CommonName={name})"

    results = ldc.search_s(base_dn, LDAP_SCOPE_SUBTREE, look_for, attrs)

    if email and not results:
        LOGGER.warning("No ldap entry found for email %s. Misconfigured alias?", email)
        look_for = f"(CommonName={name})"
        results = ldc.search_s(base_dn, LDAP_SCOPE_SUBTREE, look_for, attrs)
        if not results:
            LOGGER.warning("Still unable to find an SST for %s <%s>", name, email)
            return 'EMAIL-NOT-FOUND'
    elif not results:
        if len(name.split()) > 1:
            look_for = f"(&(givenName={name.split()[1]})(sn=*{name.split()[-1]}*))"
        else:
            look_for = f"(&(givenName=name[0]*)(sn={name[1:]}*))"
        results = ldc.search_s(base_dn, LDAP_SCOPE_SUBTREE, look_for, attrs)
        if not results:
            LOGGER.warning("Cannot find entry for %s (@%s).", name, author['username'])
            return 'UNKNOWN-SST'

    for _, attr in results:
        members_list = attr['memberOf']
        for member in members_list:
            cn_name = str(member).split(',', maxsplit=1)[0].split('=')[1]
            if re.match('rhel-sst-', cn_name):
                sst = cn_name
                break

    if email and not sst:
        LOGGER.warning("No SST found in ldap for email %s.", email)
        sst = NULL_SST

    LOGGER.debug("Got ldap results of %s", results)
    return sst


def get_author_info(pargs, mreq, authors):
    """Extract necessary MR author info."""
    gl_author = mreq['author']
    username = gl_author['username']
    if username is None:
        LOGGER.info("No username for mreq %s", mreq['iid'])
        return {}
    if username in authors:
        LOGGER.debug("User %s already in authors", username)
        return {}
    new_author = {}
    new_author[username] = gl_author
    # We already know the backport bot isn't in ldap, and most of its backports will only have
    # one subsystem, so just set it to NULL_SST and let owners sort out the SST grouping
    if username == defs.CKI_BACKPORT_BOT_ACCOUNT:
        new_author[username]['sst'] = NULL_SST
    else:
        new_author[username]['sst'] = get_author_sst_from_ldap(pargs, gl_author)
    LOGGER.debug("Got user info of: %s", new_author[username])
    return new_author


def format_mr_entry(mr_data):
    """Format output from mr_data."""
    mr_id = mr_data['iid']
    title = mr_data['title']
    author = mr_data['author']['username']
    mr_url = mr_data['webUrl']
    proj = mr_data['project']['name']
    target_branch = mr_data['targetBranch']

    mr_entry = f"  MR {mr_id}: {title} (@{author})\n"
    mr_entry += f"    {mr_url}\n"
    mr_entry += f"    Project: {proj}, Target Branch: {target_branch}\n"

    return mr_entry


def parse_labels(mreq_labels):
    """Get the description text for the labels of interest."""
    label_info = ''
    interesting_prefixes = ('Acks', 'CKI', 'CKI_RT', 'CommitRefs',
                            'Dependencies', 'Merge', 'Signoff')

    for label in mreq_labels:
        if label['title'].startswith(interesting_prefixes) and not label['title'].endswith('::OK'):
            label_info += f"    - {label['description']}\n"

    label_info += "\n"

    return label_info


def get_subsystems(mreq_labels):
    """Get list of subsystems affecting an MR from its label list."""
    subsystems = []
    for label in mreq_labels:
        if label['title'].startswith('Subsystem:'):
            subsystems.append(f"{label['title'].split(':')[1]}")

    return subsystems


def format_subsystem_entries(reports, section):
    """Format individual report entries."""
    body = ''
    for subsystem, entries in reports[section].items():
        if subsystem.startswith('rhel-sst-'):
            subsystem = '-'.join(subsystem.split('-')[2:])
        if entries:
            body += f"*** {subsystem} {section} ***\n{entries}\n"

    return body


def section_info_builder(pargs, report_list):
    """Build TOC and section info headers."""
    s_info = dict.fromkeys(report_list, '')
    s_info['conflicts'] = '1. Cannot be merged to target branch (conflicts)\n'
    s_info['warnings'] = '2. Merge requests conflicting with other MRs (warnings)\n'
    s_info['closed_issues'] = '3. Merge requests filed against closed issues (closed_issues)\n'
    s_info['wrong_stream'] = '4. Merge requests filed against the wrong stream (wrong_stream)\n'
    s_info['stale_dev'] = f'5. MRs stalled in development for {pargs.stale}+ days (stale_dev)\n'
    s_info['stale_qa'] = f'6. MRs stalled in QA for {pargs.stale}+ days (stale_qa)\n'
    s_info['stale_km'] = f'7. MRs stalled for merge for {pargs.stale}+ days (stale_km)\n'
    s_info['stale_draft'] = f'8. MR drafts stalled for {pargs.draft_stale}+ days (stale_draft)\n'

    return s_info


def format_section_header(text):
    """Print a header wrapped with dashes."""
    dashes = "-" * len(text) + "\n"
    return dashes + text + dashes


def format_report_body(pargs, reports, section_info):
    """Format the body of the report from the individual section reports."""
    report_body = '================= Table of Contents =================\n'
    report_body += 'Format is "Long Description (short_tag)"\n'
    report_body += '(NOTE: Empty sections will NOT be shown in the report body)\n'
    report_body += section_info['conflicts']
    report_body += section_info['warnings']
    report_body += section_info['closed_issues']
    report_body += section_info['wrong_stream']
    report_body += section_info['stale_dev']
    report_body += section_info['stale_qa']
    report_body += section_info['stale_km']
    report_body += section_info['stale_draft']
    report_body += '\n'

    if pargs.conflicts:
        entries = format_subsystem_entries(reports, 'conflicts')
        if entries:
            report_body += format_section_header(section_info['conflicts']) + entries

        entries = format_subsystem_entries(reports, 'warnings')
        if entries:
            report_body += format_section_header(section_info['warnings']) + entries

    entries = format_subsystem_entries(reports, 'closed_issues')
    if entries:
        report_body += format_section_header(section_info['closed_issues']) + entries

    entries = format_subsystem_entries(reports, 'wrong_stream')
    if entries:
        report_body += format_section_header(section_info['wrong_stream']) + entries

    if pargs.stale:
        entries = format_subsystem_entries(reports, 'stale_dev')
        if entries:
            report_body += format_section_header(section_info['stale_dev']) + entries

        entries = format_subsystem_entries(reports, 'stale_qa')
        if entries:
            report_body += format_section_header(section_info['stale_qa']) + entries

        entries = format_subsystem_entries(reports, 'stale_km')
        if entries:
            report_body += format_section_header(section_info['stale_km']) + entries

    if pargs.draft_stale:
        entries = format_subsystem_entries(reports, 'stale_draft')
        if entries:
            report_body += format_section_header(section_info['stale_draft']) + entries

    return report_body


def send_emailed_report(pargs, body, cc_list):
    """Create and send an email containing the report output."""
    if not (pargs.email and pargs.from_address and pargs.smtp_url):
        return

    now = misc.now_tz_utc()

    msg = EmailMessage()
    msg['Subject'] = f'Weekly KWF Problematic MR summary ({now.strftime("%Y.%m.%d")})'
    msg['From'] = pargs.from_address
    msg['To'] = ', '.join(pargs.email)
    if pargs.cc_sst_lists:
        msg['Cc'] = ', '.join(cc_list)
    msg.set_content(body)
    with smtplib.SMTP(pargs.smtp_url) as smtp:
        smtp.send_message(msg)


def _get_parser_args():
    parser = get_arg_parser("REPORT_GENERATOR")

    # Global options
    parser.add_argument('-c', '--conflicts', action='store_true', default=False,
                        help='include MRs with conflicts in the report.')
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-s', '--stale', default=15, type=int, required=False,
                        help='include MRs that have not been updated in X days.')
    parser.add_argument('-D', '--draft-stale', default=90, type=int, required=False,
                        help='include draft MRs that have not been updated in X days.')
    parser.add_argument('-e', '--email', default=os.environ.get('EMAIL_TO', '').split(),
                        help='email report to address(es)', nargs='+', required=False)
    parser.add_argument('-f', '--from-address', default=os.environ.get('REPORTER_EMAIL_FROM', ''),
                        help='email report from address', required=False)
    parser.add_argument('-S', '--smtp-url', default=os.environ.get('SMTP_URL', 'localhost'),
                        help='smtp server to use to send report', required=False)
    parser.add_argument('-C', '--cc-sst-lists', action='store_true', default=False,
                        help='Add subsystem team mailing lists to email cc list', required=False)
    parser.add_argument('-l', '--ldap-server', default=os.environ.get('LDAP_SERVER', ''),
                        help='Hostname of LDAP server to query for SSTs')
    parser.add_argument('-b', '--base-dn', default=os.environ.get('LDAP_BASE_DN', ''),
                        help='Base DN on LDAP server to query for SSTs')
    parser.add_argument('-a', '--ldap-attrs', default=os.environ.get('LDAP_ATTRS', ''),
                        help='Attributes to fetch from LDAP server to query for SSTs')
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def check_age_of_mr(pargs, updated_at):
    """Check age of MR vs. stale and draft-stale timestamps."""
    now = misc.now_tz_utc()
    stale_ts = now - timedelta(days=pargs.stale)
    stale_draft_ts = now - timedelta(days=pargs.draft_stale)

    if updated_at < str(stale_draft_ts):
        return 2

    if updated_at < str(stale_ts):
        return 1

    return 0


def extract_subsystems_data(owners, labels):
    """Extract subsystem teams from owners."""
    subsystem_teams = []
    cc_list = []
    subsystems = get_subsystems(labels)
    for subsystem in subsystems:
        for sst_list in owners.get_matching_subsystems_by_label(subsystem):
            if sst_list.devel_sst:
                for sst in sst_list.devel_sst:
                    short_sst = sst
                    if sst.startswith('rhel-sst-'):
                        short_sst = '-'.join(sst.split('-')[2:])
                    if short_sst not in subsystem_teams:
                        subsystem_teams.append(short_sst)
            if sst_list.mailing_list and sst_list.mailing_list not in cc_list:
                cc_list.append(sst_list.mailing_list)

    return subsystem_teams, cc_list


def get_qa_contact_info(report_mr):
    """Extract the QA contact name and email address for each JIRA Issue in the MR."""
    qa_contacts = set()
    for rhi in report_mr.rhissues:
        if rhi.qa_contact:
            qa_contacts.add(f'{rhi.qa_contact.displayName} <{rhi.qa_contact.emailAddress}>')
    return ", ".join(qa_contacts)


def check_waiting_on_dev(mreq, rhissues):
    """Check if there's something we're waiting on development for w/this MR."""
    waiting_on_dev = False

    # Check if the DTM is set on this MR's issues
    if any(rhi.test_failed("DevApproved") for rhi in rhissues):
        mreq['extra_info'] += f"    - {DevApproved.note}\n"
        waiting_on_dev = True

    # Check if the MR's issues are actually assigned to developers
    if any(rhi.test_failed("AssigneeIsNotBot") for rhi in rhissues):
        mreq['extra_info'] += f"    - {AssigneeIsNotBot.note}\n"
        waiting_on_dev = True

    return waiting_on_dev


def check_waiting_on_qa(mreq, rhissues):
    """Check if there's something we're waiting on QE for w/this MR."""
    waiting_on_qa = mreq['report_mr'].ready_for_qa

    # Check if the ITM is set on this MR's issues
    if any(rhi.test_failed("QEApproved") for rhi in rhissues):
        mreq['extra_info'] += f"    - {QEApproved.note}\n"
        waiting_on_qa = True

    # Check if Preliminary Testing: Pass is set on this MR's issues
    # For reasons not yet understood, CommitPolicyApproved fails, so PrelimTestingPass never runs
    # if any(rhi.test_failed("PrelimTestingPass") for rhi in rhissues):
    if not all(PrelimTestingPass(rhi) for rhi in rhissues):
        mreq['extra_info'] += f"    - {PrelimTestingPass.note}\n"
        waiting_on_qa = True

    return waiting_on_qa


def update_report_categories(pargs, reports, mreq, author_sst):
    """Update report fields as necessary with stale/conflicting MR info."""
    # pylint: disable=too-many-branches
    age = check_age_of_mr(pargs, mreq['updatedAt'])

    rmr = mreq['report_mr']
    mr_issues = ' '.join(rmr.all_rhissue_ids)
    mreq['mr_info'] += f"    Referenced jira issues: {mr_issues}\n"

    # Run all the jira tests on each issue
    rhissues = rmr.rhissues_with_scopes

    # Report any MRs filed against closed issues as only that
    if any(rhi.test_failed("JIisNotClosed") for rhi in rhissues):
        mreq['extra_info'] += f"    - {JIisNotClosed.note}\n"
        reports['closed_issues'][author_sst] += mreq['mr_info'] + mreq['extra_info'] + "\n"
        return
    # Report any MRs with target branch merge conflicts as only that
    if rmr.merge_conflict:
        reports['conflicts'][author_sst] += mreq['mr_info'] + "\n"
        return

    wrong_stream = False
    # Check if this MR's issue is for a z-stream, but MR is in CentOS Stream
    if any(rhi.test_failed("CentOSZStream") for rhi in rhissues):
        mreq['extra_info'] += f"    - {CentOSZStream.note}\n"
        wrong_stream = True

    # Check if the MR's issues all have appropriate target releases set
    if any(rhi.test_failed("TargetReleaseSet") for rhi in rhissues):
        mreq['extra_info'] += f"    - {TargetReleaseSet.note}\n"
        wrong_stream = True

    waiting_on_dev = check_waiting_on_dev(mreq, rhissues)
    if waiting_on_qa := check_waiting_on_qa(mreq, rhissues):
        mreq['mr_info'] += f"    QA Contact(s): {get_qa_contact_info(rmr)}\n"

    label_info = parse_labels(mreq['labels']['nodes'])
    mreq['extra_info'] += label_info

    if age >= 1:
        if not mreq['draft'] and wrong_stream:
            reports['wrong_stream'][author_sst] += mreq['mr_info'] + mreq['extra_info']
        elif rmr.ready_for_merge:
            reports['stale_km'][author_sst] += mreq['mr_info'] + mreq['extra_info']
        elif rmr.ready_for_qa:
            reports['stale_qa'][author_sst] += mreq['mr_info'] + mreq['extra_info']
        else:
            if not mreq['draft']:
                if waiting_on_dev:
                    reports['stale_dev'][author_sst] += mreq['mr_info'] + mreq['extra_info']
                elif waiting_on_qa:
                    reports['stale_qa'][author_sst] += mreq['mr_info'] + mreq['extra_info']
                else:
                    LOGGER.warning("Uncategorized MR: %s", mreq)
            elif age >= 2:
                reports['stale_draft'][author_sst] += mreq['mr_info'] + mreq['extra_info']

    if rmr.merge_warning and not mreq['draft']:
        reports['warnings'][author_sst] += mreq['mr_info'] + "\n"


def build_reports_categories(reports, report_list, authors):
    """Build out reports with categories and sst data."""
    author_ssts = []

    for _, entry in authors.items():
        if entry['sst'] not in author_ssts:
            author_ssts.append(entry['sst'])
    LOGGER.debug("Authors SSTs: %s", author_ssts)
    for category in report_list:
        reports[category] = dict.fromkeys(author_ssts, '')

    return author_ssts


def extend_reports_categories(reports, report_list, new_sst):
    """Extend the reports with an additional sst if needed."""
    for category in report_list:
        if new_sst not in reports[category]:
            reports[category][new_sst] = ''


def find_last_relevant_update(mreq):
    """Find the last update to the MR done by a human."""
    updated_at = mreq['createdAt']
    for discussion in mreq['report_mr'].discussions:
        for note in discussion.notes:
            if note.author.username in defs.BOT_ACCOUNTS:
                continue
            if note.updatedAt > updated_at:
                LOGGER.debug("Updating updated_at from note %s", note.body)
                updated_at = note.updatedAt

    for discussion in mreq['report_mr'].system_discussions:
        for note in discussion.notes:
            if note.author.username != defs.KWF_BOT_ACCOUNT:
                continue
            if 'approved this merge request' in note.body and note.updatedAt > updated_at:
                updated_at = note.updatedAt

    mreq['updatedAt'] = updated_at or mreq['updatedAt']

    return updated_at


def examine_mrs(session, report_list, authors, mrs_to_conflict_check):
    """Look over MRs and sort into buckets."""
    reports = dict.fromkeys(report_list, '')
    known_ssts = build_reports_categories(reports, report_list, authors)

    for mrs in mrs_to_conflict_check.values():
        for mreq in mrs:
            subsystem_teams, cc_list = extract_subsystems_data(session.owners,
                                                               mreq['labels']['nodes'])
            author_sst = authors[mreq['author']['username']]['sst'] or NULL_SST
            if author_sst == NULL_SST and len(subsystem_teams) == 1:
                author_sst = f'rhel-sst-{subsystem_teams[0]}'
                if author_sst not in known_ssts:
                    known_ssts.append(author_sst)
                    for category in report_list:
                        reports[category][author_sst] = ''
            elif author_sst == NULL_SST and mreq['report_mr'].rhissues:
                pool_team = mreq['report_mr'].rhissues[0].pool_team
                if pool_team is not None:
                    author_sst = mreq['report_mr'].rhissues[0].pool_team.replace("sst_", "")
                    if author_sst not in known_ssts:
                        extend_reports_categories(reports, report_list, author_sst)
            if not author_sst:
                LOGGER.warning("Got empty author_sst for %s", mreq)
            mr_info = format_mr_entry(mreq)
            if subsystem_teams:
                mr_info += f"    Affected Subsystems: {' '.join(subsystem_teams)}\n"
            extra_info = f"    Last Updated: {find_last_relevant_update(mreq)}\n"
            mreq['mr_info'] = mr_info
            mreq['extra_info'] = extra_info

            update_report_categories(session.args, reports, mreq, author_sst)

    return reports, cc_list


@dataclass(repr=False)
class ReportMR(DiscussionsMixin, JiraMR):
    # pylint: disable=too-many-ancestors
    """A JiraMR object with discussions mixin added."""


def main(pargs):
    """Find open MRs, check for merge conflicts and/or stalled MRs."""
    LOGGER.info('Finding open MRs...')
    session = SessionRunner.new('report_generator', args=pargs)

    mrs_to_conflict_check = {}
    for project in session.args.projects:
        mrs_to_conflict_check.update(get_open_mrs(session, project))

    LOGGER.debug("MR lists: %s", mrs_to_conflict_check)
    if not mrs_to_conflict_check:
        LOGGER.info('No open MRs to process.')
        return

    report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km',
                   'conflicts', 'warnings', 'closed_issues', 'wrong_stream']
    section_info = section_info_builder(session.args, report_list)
    authors = {}
    for _, mrs in mrs_to_conflict_check.items():
        for mreq in mrs:
            authors.update(get_author_info(session.args, mreq, authors))

    reports, cc_list = examine_mrs(session, report_list, authors, mrs_to_conflict_check)

    report_body = format_report_body(session.args, reports, section_info)

    if report_body:
        report_body += f"Additional info: {KWF_METRICS_URL}"
        send_emailed_report(session.args, report_body, cc_list)
    else:
        LOGGER.info("Nothing to report!")

    if not pargs.email and report_body:
        LOGGER.info("Found SST Mailing lists to cc: %s", cc_list)
        LOGGER.info("Report:\n%s\n", report_body)


if __name__ == '__main__':
    args = _get_parser_args()
    misc.sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)
    main(args)
