#!/usr/bin/env python
"""Look for MRs that cannot be merged and have stale Merge::OK labels."""

import argparse
import os
import subprocess

from cki_lib import logger
from cki_lib.gitlab import get_instance
from cki_lib.misc import sentry_init
import sentry_sdk

from webhook import defs
from webhook import kgit
from webhook.base_mr import BASE_MR_FIELDS
from webhook.common import add_label_to_merge_request
from webhook.mergehook import MergeMR
from webhook.mergehook import build_mr_conflict_string
from webhook.session import SessionRunner

LOGGER = logger.get_logger('webhook.utils.merge_conflicts_check')

# Get all open MRs.
PROJECT_MRS_QUERY = """
query mrData($namespace: ID!, $first: Boolean = true, $skip_files: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after,
                  not: {labels: ["NoCommits", "Oversize", "InactiveBranch", "ZStreamBuild"]}) {
      pageInfo {hasNextPage endCursor}
      nodes {webUrl ...BaseMR}
    }
  }
}
"""  # noqa: E501

PROJECT_MRS_QUERY += BASE_MR_FIELDS


def get_open_mrs(session, namespace):
    """Return a dict of valid open MR lists sorted by namespace/target-branch."""
    merge_mrs = MergeMR.new_from_query(session, query_string=PROJECT_MRS_QUERY,
                                       query_params={'namespace': namespace},
                                       paged_key='project/mergeRequests',
                                       mrs_key='project/mergeRequests/nodes',
                                       linux_src=session.args.rhkernel_src,
                                       kerneloscope_server_url=session.args.kerneloscope_server_url)
    filtered_mrs = {}
    for merge_mr in merge_mrs:
        if merge_mr.is_build_mr:
            LOGGER.info("Skipping maintainer build MR: %s", merge_mr.title)
            continue
        if not merge_mr.rh_branch:
            LOGGER.info("Skipping MR from unsupported branch: %s (%s)",
                        merge_mr.target_branch, merge_mr.title)
            continue
        ns_tb = f'{namespace}_{merge_mr.target_branch}'
        if ns_tb in filtered_mrs:
            filtered_mrs[ns_tb].append(merge_mr)
            continue
        filtered_mrs[ns_tb] = [merge_mr]

    LOGGER.debug('Project %s MRs found: %s', namespace, filtered_mrs)
    return filtered_mrs


def prep_branch_for_merges(rhkernel_src, mr_params):
    """Prepare a git branch/worktree for doing bulk merges."""
    # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
    # where everything is an additional remote on top of kernel-ark, w/remote name == project name
    proj_target_branch = f"{mr_params['gl_project'].name}/{mr_params['target_branch']}"
    merge_branch = f"{mr_params['gl_project'].name}-{mr_params['target_branch']}-megamerge"
    worktree_dir = f"/{'/'.join(rhkernel_src.strip('/').split('/')[:-1])}/{merge_branch}/"
    # This is the lookaside cache we maintain for examining diffs between revisions of a
    # merge request, which we're going to create temporary worktrees off of
    LOGGER.info("Creating git worktree at %s with branch %s for mergeability testing, please hold",
                worktree_dir, merge_branch)
    kgit.worktree_add(rhkernel_src, merge_branch, worktree_dir, proj_target_branch)
    return worktree_dir


def get_reset_branch_name(merge_dir):
    """Extract reset branch name from merge_dir."""
    merge_base = merge_dir.strip('/').split('/')[-1]
    reset_branch = f"{merge_base}-reset"
    return reset_branch


def try_nested_merges(mrs, conflict_mrs, mr_params):
    """Try merging MRs with conflicts first, then all other MRs."""
    nested_conflict_mrs = []
    pname = mr_params['gl_project'].name
    merge_dir = mr_params['merge_dir']
    reset_branch = get_reset_branch_name(merge_dir)
    interim_branch = f"{reset_branch}-interim"
    nested_interim_branch = f"{reset_branch}-interim-nested"

    kgit.hard_reset(merge_dir, reset_branch)
    for conflict_mr in conflict_mrs:
        kgit.branch_copy(merge_dir, interim_branch)
        try:
            kgit.merge(merge_dir, f'{pname}/merge-requests/{conflict_mr.iid}')
        except subprocess.CalledProcessError:
            kgit.hard_reset(merge_dir, interim_branch)
            kgit.branch_delete(merge_dir, interim_branch)
            continue

        for merge_mr in mrs:
            if merge_mr in conflict_mrs:
                continue
            kgit.branch_copy(merge_dir, nested_interim_branch)
            try:
                kgit.merge(merge_dir, f'{pname}/merge-requests/{merge_mr.iid}')
            except subprocess.CalledProcessError:
                LOGGER.debug("Could not merge %s in third pass", merge_mr.iid)
                kgit.hard_reset(merge_dir, nested_interim_branch)
                if merge_mr not in conflict_mrs:
                    nested_conflict_mrs.append(merge_mr)
            kgit.branch_delete(merge_dir, nested_interim_branch)
        kgit.branch_delete(merge_dir, interim_branch)

    return nested_conflict_mrs


def try_merging_all(mrs, mr_params):
    """Try merging all MRs together both directions to identify MRs with conflicts."""
    conflict_mrs = []
    pname = mr_params['gl_project'].name
    merge_dir = mr_params['merge_dir']
    reset_branch = get_reset_branch_name(merge_dir)
    interim_branch = f"{reset_branch}-interim"

    for merge_mr in list(reversed(mrs)):
        kgit.branch_copy(merge_dir, interim_branch)
        try:
            kgit.merge(merge_dir, f'{pname}/merge-requests/{merge_mr.iid}')
        except subprocess.CalledProcessError:
            LOGGER.debug("Could not merge %s in first pass", merge_mr.iid)
            kgit.hard_reset(merge_dir, interim_branch)
            if merge_mr not in conflict_mrs:
                conflict_mrs.append(merge_mr)
        kgit.branch_delete(merge_dir, interim_branch)

    kgit.hard_reset(merge_dir, reset_branch)
    for merge_mr in mrs:
        kgit.branch_copy(merge_dir, interim_branch)
        try:
            kgit.merge(merge_dir, f'{pname}/merge-requests/{merge_mr.iid}')
        except subprocess.CalledProcessError:
            LOGGER.debug("Could not merge %s in second pass", merge_mr.iid)
            kgit.hard_reset(merge_dir, interim_branch)
            if merge_mr not in conflict_mrs:
                conflict_mrs.append(merge_mr)
        kgit.branch_delete(merge_dir, interim_branch)

    # Now do a pass to catch cases where the same MR conflicts with multiple others,
    # by trying to merge each conflict MR first, then the other MRs.
    nested_conflict_mrs = try_nested_merges(mrs, conflict_mrs, mr_params)

    kgit.hard_reset(merge_dir, reset_branch)

    return conflict_mrs + nested_conflict_mrs


def merge_mr_to_entry(merge_mr):
    """Convert a merge_mr into an entry suitable for build_mr_conflict_string."""
    mr_entry = {}
    mr_entry['iid'] = merge_mr.iid
    mr_entry['author'] = {'username': merge_mr.author.username}
    mr_entry['title'] = merge_mr.title

    return mr_entry


def check_pending_conflicts(merge_mr, conflict_mrs):
    """Check for direct conflicts with other MRs with merge issues."""
    LOGGER.debug("Checking other pending %s MRs for conflicts with MR %s",
                 merge_mr.target_branch, merge_mr.iid)

    reset_branch = get_reset_branch_name(merge_mr.worktree_dir)
    save_branch = f"{reset_branch}-{merge_mr.iid}"
    kgit.branch_copy(merge_mr.worktree_dir, save_branch)

    target_branch = f"{merge_mr.project_remote}/{merge_mr.target_branch}"
    has_conflicts = False
    for conflict_mr in conflict_mrs:
        cmr_id = conflict_mr.iid
        if cmr_id == merge_mr.iid:
            continue

        LOGGER.debug("Checking if MR %s conflicts with MR %s in %s",
                     merge_mr.iid, cmr_id, merge_mr.target_branch)
        # First, make sure this MR can actually be merged by itself
        if not kgit.branch_mergeable(merge_mr.worktree_dir, target_branch,
                                     f'{merge_mr.project_remote}/merge-requests/{cmr_id}'):
            LOGGER.debug("MR %d can't be merged by itself, skipping conflict check", cmr_id)
            continue

        # Now, try to merge it on top of this MR, so we can see if it has any conflicts
        kgit.hard_reset(merge_mr.worktree_dir, save_branch)
        has_conflicts |= merge_mr.check_for_replicants(conflict_mr.iid)
        try:
            kgit.merge(merge_mr.worktree_dir, f'{merge_mr.project_remote}/merge-requests/{cmr_id}')
        except subprocess.CalledProcessError as err:
            has_conflicts = True
            merge_mr.conflicts.append(build_mr_conflict_string(merge_mr_to_entry(conflict_mr)))
            merge_mr.conflicts.append(err.output)

    kgit.branch_delete(merge_mr.worktree_dir, save_branch)
    return has_conflicts


def find_direct_conflicts(conflict_mrs, mr_params):
    """Find which MRs directly conflict with others."""
    reset_branch = get_reset_branch_name(mr_params['merge_dir'])

    for merge_mr in conflict_mrs:
        merge_mr.worktree_dir = mr_params['merge_dir']
        LOGGER.debug('Checking for merge conflicts on %s MR %s', mr_params['proj_tb'], merge_mr.iid)

        kgit.hard_reset(merge_mr.worktree_dir, reset_branch)
        if merge_mr.check_for_merge_conflicts():
            LOGGER.debug('Target branch merge conflicts in %s/%s MR %s:\n%s',
                         merge_mr.project_remote, merge_mr.target_branch, merge_mr.iid,
                         merge_mr.format_conflict_info())
            merge_mr.merge_label = defs.MERGE_CONFLICT_LABEL
            if not merge_mr.session.args.testing:
                merge_mr.update_mr(only_if_changed=True)
            continue

        if check_pending_conflicts(merge_mr, conflict_mrs):
            LOGGER.debug('Other pending MR merge conflicts found on %s/%s MR %s:\n%s',
                         merge_mr.project_remote, merge_mr.target_branch, merge_mr.iid,
                         merge_mr.format_conflict_info())
            merge_mr.merge_label = defs.MERGE_WARNING_LABEL
            if not merge_mr.session.args.testing:
                merge_mr.update_mr(only_if_changed=True)
            continue

        LOGGER.warning('%s/%s MR %s had errors earlier, and now does not?!?',
                       merge_mr.project_remote, merge_mr.target_branch, merge_mr.iid)

    kgit.branch_delete(mr_params['merge_dir'], reset_branch)


def check_for_duplicated_backports(no_conflicts_mrs, mr_params):
    """Check for MRs that are backporting a commit already backported in-tree."""
    reset_branch = get_reset_branch_name(mr_params['merge_dir'])

    duplicates_mrs = []
    for merge_mr in no_conflicts_mrs:
        merge_mr.worktree_dir = mr_params['merge_dir']
        LOGGER.debug('Checking for duplicates on %s MR %s', mr_params['proj_tb'], merge_mr.iid)

        kgit.hard_reset(merge_mr.worktree_dir, reset_branch)
        if merge_mr.check_for_existing_backports():
            merge_mr.merge_label = defs.MERGE_CONFLICT_LABEL
            if not merge_mr.session.args.testing:
                merge_mr.update_mr(only_if_changed=True)
            duplicates_mrs.append(merge_mr)

    return duplicates_mrs


def clean_up_temp_merge_branches(args, merge_dirs):
    """Clean up all our temp merge branches."""
    for _, merge_dir in merge_dirs.items():
        merge_base = merge_dir.strip('/').split('/')[-1]
        kgit.clean_up_temp_merge_branch(args.rhkernel_src, merge_base, merge_dir)


def set_merge_ok_labels(args, no_conflict_mrs, mr_params):
    """Set the Merge::OK label on any MR that doesn't have conflicts if not preset."""
    gl_project = mr_params['gl_project']
    for merge_mr in no_conflict_mrs:
        LOGGER.debug('Merge OK for %s/%s MR %s from %s',
                     gl_project.name, merge_mr.target_branch, merge_mr.iid, merge_mr.author)
        if not args.testing:
            add_label_to_merge_request(gl_project, merge_mr.iid, [f'Merge::{defs.READY_SUFFIX}'])


def filter_out_conflicting(mrs, conflict_mrs):
    """Return the list of MRs that don't have direct merge conflicts."""
    return [mr for mr in mrs if mr not in conflict_mrs]


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check open MRs for merge conflicts')

    # Global options
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-s', '--since', default=24,
                        help='check if target branch changed since X hours ago.',
                        type=int, required=False)
    parser.add_argument('-r', '--rhkernel-src', default=os.environ.get('RHKERNEL_SRC', ''),
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-k', '--kerneloscope-server-url',
                        default=os.environ.get('KERNELOSCOPE_SERVER_URL', ''),
                        help='Kerneloscope server URL')
    parser.add_argument('--no-kerneloscope', action='store_true',
                        default=os.environ.get('NO_KERNELOSCOPE', ''),
                        help='Override use of kerneloscope, fall back to git log methods')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode: do not update gitlab labels")
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def main(session):
    """Find open MRs and check for merge conflicts."""
    # pylint: disable=too-many-locals
    if not session.args.rhkernel_src:
        LOGGER.warning("No path to RH Kernel source git found, aborting!")
        return

    LOGGER.info('Fetching from git remotes to ensure we have the latest data needed')
    for project in session.args.projects:
        remote_name = project.rsplit("/", 1)[-1]
        kgit.fetch_remote(session.args.rhkernel_src, remote_name)

    LOGGER.info('Finding open MRs for projects: %s', session.args.projects)
    gl_instance = get_instance(defs.GITFORGE)

    mrs_to_conflict_check = {}
    for project in session.args.projects:
        mrs_to_conflict_check.update(get_open_mrs(session, project))

    LOGGER.debug("MR lists: %s", mrs_to_conflict_check)
    if not mrs_to_conflict_check:
        LOGGER.info('No open MRs to process.')
        return

    gl_projects = {}
    merge_dirs = {}
    # Build per-project/target_branch lists of MRs
    for mr_ns_tb, mrs in mrs_to_conflict_check.items():
        mr_params = {}
        mr_params['namespace'] = mr_ns_tb.split('_')[0]
        if not (gl_project := gl_projects.get(mr_params['namespace'])):
            gl_project = gl_instance.projects.get(mr_params['namespace'])
            gl_projects[mr_params['namespace']] = gl_project
        mr_params['gl_project'] = gl_project
        mr_params['target_branch'] = mr_ns_tb.split('_')[1]
        mr_params['proj_tb'] = f"{mr_params['gl_project'].name}_{mr_params['target_branch']}"
        if not (merge_dir := merge_dirs.get(mr_params['proj_tb'])):
            merge_dir = prep_branch_for_merges(session.args.rhkernel_src, mr_params)
            merge_dirs[mr_params['proj_tb']] = merge_dir

        mr_params['merge_dir'] = merge_dir
        kgit.branch_copy(merge_dir, get_reset_branch_name(merge_dir))

        conflict_mrs = try_merging_all(mrs, mr_params)
        no_conflict_mrs = filter_out_conflicting(mrs, conflict_mrs)

        duplicates_mrs = check_for_duplicated_backports(no_conflict_mrs, mr_params)
        no_conflict_mrs = filter_out_conflicting(no_conflict_mrs, duplicates_mrs)

        set_merge_ok_labels(session.args, no_conflict_mrs, mr_params)

        find_direct_conflicts(conflict_mrs, mr_params)

    clean_up_temp_merge_branches(session.args, merge_dirs)


if __name__ == '__main__':
    pargs = _get_parser_args()
    sentry_init(sentry_sdk, ca_certs=pargs.sentry_ca_certs)
    main(SessionRunner.new('mergehook', args=pargs))
