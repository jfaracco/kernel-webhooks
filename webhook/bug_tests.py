"""Readiness tests for Bug objects."""
# pylint: disable=invalid-name
from functools import wraps

from cki_lib.logger import get_logger

from webhook.defs import BZStatus
from webhook.defs import MrScope

LOGGER = get_logger('cki.webhook.bug_tests')


# Decorator function that runs each test.
def test_runner(passed=MrScope.READY_FOR_MERGE, failed=MrScope.NEEDS_REVIEW, keep_going=True,
                skip_if_failed=None):
    """Decorator to run one Bug Test."""
    def decorate_test(func):
        @wraps(func)
        def run_test(*args, **kwargs):
            bug = kwargs['bug'] if 'bug' in kwargs else args[0]
            f_name = func.__name__

            if skip_if_failed and any(bug.test_failed(test) for test in skip_if_failed):
                LOGGER.debug('Passing %s test as it has failed one of these tests: %s',
                             f_name, skip_if_failed)
                result = True
                result_str = 'skipped'
            else:
                result = func(*args, **kwargs)
                result_str = 'passed' if result else 'failed'
            if not result:
                bug.failed_tests.append(f_name)

            scope = passed if result else failed
            LOGGER.debug('[%s] %s %s, scope is: %s', bug.alias, f_name, result_str, scope.name)
            return result, scope, result if not keep_going else True
        return run_test
    return decorate_test


# This InMrDescription test is also used by jirahook so the failure scope needs to be compatible
# with jirahook as well as bughook! There is no NeedsReview label in jirahook!
@test_runner(failed=MrScope.MISSING)
def InMrDescription(bug):
    """Pass if the Bug appears in the MR Description, otherwise Fail."""
    # Skip this test for any Bug that doesn't have commits such as the dummy UNTAGGED Bug.
    return bug.in_mr_description if bug.commits else True


InMrDescription.note = ("These commits have a `Bugzilla:` or `CVE:` tag which references "
                        "a BZ or CVE ID that was not listed in the merge request description.  "
                        "Please verify the tag is correct, or, add them to your MR description "
                        "as either a `Bugzilla`, `CVE`, or `Depends` tag.")


# This HasCommits test is also used by jirahook so the failure scope needs to be compatible
# with jirahook as well as bughook! There is no NeedsReview label in jirahook!
@test_runner(failed=MrScope.MISSING)
def HasCommits(bug):
    """Pass if the Bug is tagged in some commits, otherwise Fail."""
    return bool(bug.commits)


HasCommits.note = ("This tag is referenced in the MR description but is not referenced in "
                   "any of the MR's commits.  Please ensure the tag is correct and update "
                   "commit descriptions with `Bugzilla`, `Depends`, or `CVE` tags as needed.")


# This BZisNotUnknown test is also used by jirahook so the failure scope needs to be compatible
# with jirahook as well as bughook! There is no NeedsReview label in jirahook!
@test_runner(failed=MrScope.INVALID, keep_going=False)
def BZisNotUnknown(bug):
    """Pass if the BZ status is not UNKNOWN, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return bug.bz_status is not BZStatus.UNKNOWN


BZisNotUnknown.note = ("There was a problem retrieving data from Bugzilla. Please check "
                       "that the Bugzilla information is correct and contact a maintainer "
                       "if the problem persists.")

# Tests to run on CVE: Bugs
CVE_TESTS = [InMrDescription,
             HasCommits,
             BZisNotUnknown,
             ]
