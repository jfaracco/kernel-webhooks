"""GraphQL Fragments to use with queries.

See https://graphql.org/learn/queries/#fragments

"""

CKI_PIPELINE = """
fragment CkiPipeline on Pipeline {
  id
  createdAt
  project {
    id
  }
  sourceJob {
    name
  }
  status
  stages {
    nodes {
      name
      jobs {
        nodes {
          status
        }
      }
    }
  }
}
"""

CURRENT_USER = """
fragment CurrentUser on Query {
  currentUser {
    ...GlUser
  }
}
"""

GL_USER = """
fragment GlUser on User {
  gid: id
  name
  email: publicEmail
  username
}
"""

GL_USER_CORE = GL_USER.replace('GlUser on User', 'GlUserCore on UserCore')

MR_COMMITS = """
fragment MrCommits on MergeRequest {
  commits: commitsWithoutMergeCommits(
    first: $per_page
    before: $before
    after: $after
  ) {
    pageInfo {
      hasNextPage
      endCursor
    }
    nodes {
      author {
        ...GlUser
      }
      authorEmail
      authorName
      authoredDate
      committerEmail
      committerName
      description
      sha
      title
      diffs @include(if: $with_diffs) {
        diff
        oldPath
        newPath
        aMode
        bMode
        newFile
        renamedFile
        deletedFile
      }
    }
  }
}
"""

MR_FILES = """
fragment MrFiles on MergeRequest {
  files: diffStats {
    path
  }
}
"""

MR_LABELS = """
fragment MrLabels on MergeRequest {
  labels {
    nodes {
      title
    }
  }
}
"""

MR_LABELS_PAGED = """
fragment MrLabelsPaged on MergeRequest {
  labels(after: $after) {
    pageInfo {
      hasNextPage
      endCursor
    }
    nodes {
      title
    }
  }
}
"""

GL_NOTE = """
fragment GlNote on Note {
  author {
    ...GlUser
  }
  body
  id
  createdAt
  updatedAt
}
"""

MR_APPROVAL_RULE = """
fragment MrApprovalRule on ApprovalRule {
  approvalsRequired
  id
  name
  type
  eligibleApprovers {
    ...GlUser
  }
}
"""

PROJECT_APPROVAL_RULE = """
fragment ProjectApprovalRule on ApprovalProjectRule {
  approvalsRequired
  id
  name
  type
  eligibleApprovers {
    nodes {
      ...GlUser
    }
  }
}
"""
