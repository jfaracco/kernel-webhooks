"""A basic MR class using Graphql."""
import dataclasses
from functools import cached_property
from textwrap import dedent
from time import sleep
import typing

from cki_jira.common import GitlabURL
from cki_jira.description import Description
from cki_lib import misc
from cki_lib.footer import Footer
from cki_lib.logger import get_logger
from gitlab import GitlabHttpError
import prometheus_client as prometheus

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.commits import VersionManager
from webhook.description import MRDescription
from webhook.jira import delete_resource
from webhook.jira import get_issues_with_link
from webhook.jira import get_matching_comments
from webhook.kwf_tracker import KwfIssueTracker
from webhook.kwf_tracker import TrackerCache
from webhook.kwf_tracker import make_trackers
from webhook.session import SessionRunner
from webhook.users import UserCache

if typing.TYPE_CHECKING:
    from gitlab.client import Gitlab
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest
    from gitlab.v4.objects.projects import Project
    from jira.resources import Comment

    from webhook import rh_metadata
    from webhook.commits import Commit
    from webhook.session import BaseSession
    from webhook.users import User

LOGGER = get_logger('cki.webhook.base_mr')

METRIC_KWF_QUERY_RETRIES = prometheus.Counter(
    'kwf_query_retries',
    'Number of times query data was found on an MR only after retrying'
)

# https://docs.gitlab.com/ee/api/merge_requests.html#get-merge-request-dependencies
BLOCKS_ENDPOINT = '/projects/{project_id}/merge_requests/{mr_id}/blocks'

# "Global" ID string to set on jira remote links.
KWF_REMOTE_LINK_GID = 'kwf-mr-link:{mr_url}'

# URL of icon to use for MRs in jira remote links.
GITLAB_REMOTE_LINK_ICON_URL = \
    ('{base_url}/assets/favicon-'
     '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')


def check_mr_query_results(results: dict) -> bool:
    """Check the query results are useful."""
    if 'project' not in results or not results['project'].get('mr'):
        LOGGER.warning('Merge request does not exist?')
        return False
    return True


class MergeRequestProtocol(typing.Protocol):
    # pylint: disable=too-many-public-methods
    """Expected attributes of a BaseMR."""

    session: 'BaseSession'

    # Set attributes.

    approved: bool
    author: 'User | None'
    kwf_description: Description
    commit_count: int
    draft: bool
    files: list[str]
    global_id: defs.GitlabGID | None
    head_pipeline_gid: defs.GitlabGID | None
    head_sha: str
    labels: list[defs.Label]
    project_gid: defs.GitlabGID | None
    source_branch: str
    state: defs.MrState
    target_branch: str
    title: str
    url: GitlabURL

    _description: MRDescription | str

    # Derived cached properties.

    @cached_property
    def gl_instance(self) -> 'Gitlab':
        """Return a gitlab.client.Gitlab object."""
        return self.session.get_gl_instance(self.url.base_url)

    @cached_property
    def gl_mr(self) -> 'ProjectMergeRequest':
        """Return a gl_mergerequest object."""
        return self.gl_project.mergerequests.get(self.url.id)

    @cached_property
    def gl_project(self) -> 'Project':
        """Return a gl_project object."""
        return self.session.get_gl_project(self.url.namespace)

    @cached_property
    def rh_project(self) -> 'rh_metadata.Project | None':
        """Return the matching rh_metadata.Project object, or None."""
        return self.session.rh_projects.get_project_by_namespace(self.url.namespace)

    @cached_property
    def tracker_cache(self) -> TrackerCache:
        """Return a TrackerCache object."""
        return TrackerCache(session=self.session)

    @cached_property
    def user_cache(self) -> UserCache:
        """Return a UserCache object."""
        return self.session.get_user_cache(self.url.namespace)

    @cached_property
    def author_can_merge(self) -> bool:
        """Return True if the MR author has permission to merge this MR, otherwise False."""
        if not self.author:
            return False

        return self.session.get_graphql(self.url.base_url).user_can_merge(
            self.namespace,
            self.global_id,
            self.author.username
        )

    @cached_property
    def dependency_mrs(self) -> list[typing.Self]:
        """Construct new BaseMR objects from the direct_dependencies and return them as a list."""

    @cached_property
    def version_manager(self) -> VersionManager:
        """Return a commits.VersionManager object."""
        return VersionManager(self.session, self.gl_mr, self.dependency_mrs)

    # Derived properties.

    @property
    def description(self) -> MRDescription:
        """Return the MRDescription, creating it if necessary."""
        if not isinstance(self._description, MRDescription):
            self._description = MRDescription(self._description or '', self.url.namespace,
                                              self.session.get_graphql(self.url.base_url))

        return self._description

    @property
    def rh_branch(self) -> 'rh_metadata.Branch | None':
        """Return the matching rh_metadata.Branch object, or None."""
        return self.rh_project.get_branch_by_name(self.target_branch) if self.rh_project else None

    @property
    def iid(self) -> int:
        """Return the MR internal ID as an int."""
        return self.url.id

    @property
    def namespace(self) -> str:
        """Return the MR namespace."""
        return self.url.namespace

    @property
    def kwf_code_changes(self) -> list[str]:
        """Return the list of code changes between the latest MR revision and the prior."""
        # This is an empty list if there are less than two revisions of the MR!
        return self.version_manager.latest_code_changes

    @property
    def code_changes_version(self) -> int:
        """Return the vision # when the code last had a meaningful change."""
        return self.version_manager.latest_code_changes_version

    @property
    def kwf_commits(self) -> dict[str, 'Commit']:
        """Return a dict of Commit objects associated with the latest MrVersion with sha as key."""
        return self.version_manager.latest_commits

    @property
    def descriptions(self) -> list[Description]:
        """Return the list of all the Description objects associated with this MR."""
        return [self.kwf_description] + [commit.description for commit in self.kwf_commits.values()]

    @property
    def kwf_files(self) -> list[str]:
        """Return the list of files affected by this MR's non-dependency commits."""
        if not self.kwf_description.depends or not self.version_manager.latest:
            return self.files

        latest_files = self.version_manager.latest_files
        return latest_files if latest_files is not None else self.files

    @property
    def kwf_first_dep_sha(self) -> str | None:
        """Return the sha of the first dependency commit, or None if no depends or not found."""
        return self.version_manager.latest_first_dep_sha

    @property
    def has_depends(self) -> bool:
        """Return True if the MR description lists any dependencies, otherwise False."""
        return bool(self.description.depends or self.description.depends_mrs)

    @property
    def is_build_mr(self) -> bool:
        """Return True if this is a z-stream build MR in Draft state, otherwise False."""
        if not self.draft or not self.rh_branch:
            return False

        if 'CKI_BUILD_ONLY' in self.title and self.rh_branch.ystream:
            return True

        return (defs.ZSTREAM_BUILD_LABEL in self.labels and self.rh_branch.zstream)

    @property
    def is_autobot_mr(self) -> bool:
        """Return True if this MR was created by our auto-backport infrastructure."""
        return self.author and self.author.username == defs.CKI_BACKPORT_BOT_ACCOUNT

    @property
    def manage_jiras(self) -> bool:
        """Return if the project for this MR uses Jira."""
        return self.rh_project and self.rh_project.manage_jiras if self.rh_project else False

    @property
    def merge_conflict(self) -> bool:
        """Return True if the MR has merge conflicts."""
        return defs.MERGE_CONFLICT_LABEL in self.labels

    @property
    def merge_warning(self) -> bool:
        """Return True if the MR has merge warnings."""
        return defs.MERGE_WARNING_LABEL in self.labels

    @property
    def ready_for_qa(self) -> bool:
        """Return True if the MR has the readyForQA label on it."""
        return defs.READY_FOR_QA_LABEL in self.labels

    @property
    def ready_for_merge(self) -> bool:
        """Return True if the MR has the readyForMerge label on it."""
        return defs.READY_FOR_MERGE_LABEL in self.labels

    # Methods.

    def add_labels(self, to_add: list[str], remove_scoped: bool = False) -> list[str]:
        """Use common code to add the list of labels to the MR."""
        fresh_labels = common.add_label_to_merge_request(self.gl_project, self.iid, to_add,
                                                         remove_scoped=remove_scoped)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_add).issubset(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_add: {to_add}'))
        return fresh_labels

    def remove_labels(self, to_remove: list[str]) -> list[str]:
        """Use common code to remove the list of labels from the MR."""
        fresh_labels = common.remove_labels_from_merge_request(self.gl_project, self.iid, to_remove)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_remove).isdisjoint(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_remove: {to_remove}'))
        return fresh_labels

    def labels_with_prefix(self, prefix: str) -> list[defs.Label]:
        """Return the list of scoped MR labels with the given prefix."""
        return [label for label in self.labels if label.scoped and label.prefix == prefix]

    def labels_with_scope(self, scope: defs.MrScope) -> list[defs.Label]:
        """Return the list of scoped MR labels with the given MrScope."""
        return [label for label in self.labels if label.scoped and label.scope is scope]

    def _blocking_data(self) -> list[dict[str, typing.Any]]:
        """Return the raw list of 'blocks' data for this MR."""
        blocks_endpoint = BLOCKS_ENDPOINT.format(project_id=self.project_gid.id, mr_id=self.iid)
        return self.gl_instance.http_list(blocks_endpoint)

    def add_blocking_mr(self, mr_gid: defs.GitlabGID) -> dict[str, typing.Any] | None:
        """Add the MR global ID as a 'block' (dependency) & return the API result or None."""
        mr_gid = mr_gid.id
        gl_instance = self.gl_instance
        blocks_endpoint = BLOCKS_ENDPOINT.format(project_id=self.project_gid.id, mr_id=self.iid)

        # In the non-live case we just return an empty dict?
        if not self.session.is_production_or_staging:
            return {}

        try:
            result = gl_instance.http_post(f'{blocks_endpoint}?blocking_merge_request_id={mr_gid}')
        except GitlabHttpError as err:
            LOGGER.warning('Failed to add a block on %s for MR with GID %s: %s',
                           self.url, mr_gid, err)
            return None

        return result

    def remove_blocking_id(self, block_id: int) -> None:
        """Remove the 'block' (dependency) by its ID."""
        blocks_endpoint = BLOCKS_ENDPOINT.format(project_id=self.project_gid.id, mr_id=self.iid)

        if not self.session.is_production_or_staging:
            return

        try:
            self.gl_instance.http_delete(f'{blocks_endpoint}/{block_id}')
        except GitlabHttpError as err:
            LOGGER.warning('Failed to remove block on %s with ID %s: %s', self.url, block_id, err)

    def link_to_tracker(self, tracker: KwfIssueTracker, footer: str | None = None) -> None:
        """Add or update a Remote Link to this MR for the given jira issue & leave a comment."""
        if not (tracker.is_jira and isinstance(tracker, KwfIssueTracker)):
            LOGGER.warning('Linking is only supported on jira KwfIssueTracker, not %s.', self)
            return

        title = f'Merge Request {self.url.reference}: {self.title}'
        link_gid = KWF_REMOTE_LINK_GID.format(mr_url=self.url)
        extra_destination = {
            'icon': {
                'url16x16': GITLAB_REMOTE_LINK_ICON_URL.format(base_url=self.url.base_url),
                'title': 'Gitlab Merge Request'
            }
        }

        result = tracker.set_remote_link(
            url=self.url,
            title=title,
            link_gid=link_gid,
            extra_destination=extra_destination,
        )

        # set_remote_link() returns True if a new RemoteLink was created in which
        # case we want to post a comment about it.
        if result:
            if footer is None:
                footer = self.session.comment.jira_footer() if \
                    isinstance(self.session, SessionRunner) else ''

            comment_body = _mr_link_comment(self.url, self.title, self.target_branch, footer)

            tracker.post_comment(comment_body, pinned=True)

    def unlink_from_tracker(self, tracker: KwfIssueTracker) -> None:
        """Remove the Remote Link to this MR (and any comment) from the given jira issue."""
        if not (tracker.is_jira and isinstance(tracker, KwfIssueTracker)):
            LOGGER.warning('Unlinking is only supported on jira KwfIssueTracker, not %s.', self)
            return

        # remove_remote_link() returns True when it found an existing RemoteLink to remove, in which
        # case there may be a comment to delete as well.
        if tracker.remove_remote_link(self.url):
            # We're going to delete any comment made by us that has this MR's URL in it :shrug:.
            matching_comments: list[Comment] = get_matching_comments(
                comments=tracker.comments(),
                substring=self.url,
                author_usernames=[self.session.jira.current_user()]
            )

            if not matching_comments:
                LOGGER.info('No matching comments in %s to delete.', tracker.id)
                return

            for comment in matching_comments:
                delete_resource(comment)

    def unlink_from_all(self):
        """Remove all Remote Links to this MR (and comments) from any KWF issue."""
        # Search jira for every KWF issue that has a Remote Link with our MR URL and
        # unlink them all.
        if jira_issues := get_issues_with_link(self.session.jira, self.url):
            for tracker in make_trackers(jira_issues, self.session):
                # If somehow we got a non-KWF issue here we should ignore it since we
                # don't want to modify non-KWF issues.
                if not isinstance(tracker, KwfIssueTracker):
                    continue

                self.unlink_from_tracker(tracker)

    # Static methods.

    @staticmethod
    def query(
        session: 'BaseSession',
        query_string: str,
        query_params: typing.Optional[dict] = None,
        paged_key: typing.Optional[str] = None,
        check_function: typing.Optional[typing.Callable] = check_mr_query_results,
        query_retries: int = 2,
        query_retry_delay: int = 5,
        operation_name: typing.Optional[str] = None
    ) -> dict[str, typing.Any] | None:
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """Run the given query and process it with the given function."""
        query_params = query_params or {}
        results: dict[str, typing.Any] | None = None

        for retry in range(query_retries):
            results = session.graphql.client.query(
                query_string,
                variable_values=query_params,
                paged_key=paged_key,
                operation_name=operation_name
            )
            if not results:
                LOGGER.warning('No results for query with params: %s\n%s', query_params,
                               query_string)
                return None
            # If there is no check_function then just return whatever results we have.
            if check_function is None:
                return results
            if check_function and check_function(results):
                if retry:
                    LOGGER.warning('API returned data on the second try 🙄.')
                    METRIC_KWF_QUERY_RETRIES.inc()
                break
            if retry:
                LOGGER.info('Still no data after retry 🤷.')
                break
            LOGGER.info('No data found, trying again in %s seconds.', query_retry_delay)
            sleep(query_retry_delay)

        return results


BASE_MR_FIELDS = """
fragment BaseMR on MergeRequest {
  approved
  author {
    ...GlUser
  }
  commitCount
  description
  global_id: id
  diffHeadSha
  ...MrLabelsPaged
  state
  draft
  ...MrFiles @skip(if: $skip_files)
  preparedAt
  sourceBranch
  targetBranch
  title
  headPipeline {
    id
  }
  project {
    id
  }
  webUrl
}
"""

BASE_MR_FIELDS += fragments.GL_USER + fragments.MR_LABELS_PAGED + fragments.MR_FILES


@dataclasses.dataclass(repr=False, kw_only=True)
class BaseMR(MergeRequestProtocol):
    # pylint: disable=too-many-instance-attributes
    """Hold basic MR data and provide basic functions."""

    MR_QUERY_BASE: typing.ClassVar[str] = dedent("""
    query mrDetails($mr_id: String!, $namespace: ID!, $skip_files: Boolean = false, $after: String = "") {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...BaseMR
        }
      }
    }
    """)  # noqa: E501

    MR_QUERY: typing.ClassVar[str] = MR_QUERY_BASE + BASE_MR_FIELDS

    session: 'BaseSession'
    url: GitlabURL

    approved: bool = False
    author: 'User | None' = None
    kwf_description: Description = dataclasses.field(default_factory=Description)
    commit_count: int = 0
    draft: bool = False
    files: list[str] = dataclasses.field(default_factory=list)
    global_id: defs.GitlabGID | None = None
    head_pipeline_gid: defs.GitlabGID | None = None
    head_sha: str = ''
    labels: list[defs.Label] = dataclasses.field(default_factory=list)
    project_gid: defs.GitlabGID | None = None
    source_branch: str = ''
    state: defs.MrState = defs.MrState.UNKNOWN
    target_branch: str = ''
    title: str = ''

    _description: MRDescription | str = ''
    raw_data: dict = dataclasses.field(default_factory=dict)

    def __repr__(self) -> str:
        """Describe yourself."""
        values_str = ', '.join(f'{k}: {v}' for k, v in self.__repr_dict__().items())
        return f'{self.__class__.__name__}({self.url.reference} {values_str})'

    def __repr_dict__(self) -> dict[str, typing.Any]:
        """Return a dict of values for a repr string."""
        return {
            'approved': self.approved,
            'commits': self.commit_count,
            'state': self.state.name.lower(),
            'draft': self.draft,
            'project': self.rh_project.name if self.rh_project else 'None',
            'branch': self.rh_branch.name if self.rh_branch else 'None'
        }

    def __post_init__(self) -> None:
        """Fix up the url and labels just to be safe."""
        self.url = GitlabURL(self.url)
        self.labels = [defs.Label(label) for label in self.labels]
        LOGGER.info('Created %s', self)

    @classmethod
    def _parse_mr_query(
        cls,
        session: 'BaseSession',
        url: defs.GitlabURL,
        raw_data: dict[str, typing.Any]
    ) -> dict[str, typing.Any]:
        """Parse a raw BaseMR.MR_QUERY result into proper values."""
        author = None
        if author_dict := raw_data.get('author'):
            user_cache = session.get_user_cache(url.namespace)
            author = user_cache.get(author_dict)

        description_text = raw_data.get('description', '')

        if global_id := raw_data.get('global_id'):
            global_id = defs.GitlabGID(global_id)

        if head_pipeline_gid := misc.get_nested_key(raw_data, 'headPipeline/id'):
            head_pipeline_gid = defs.GitlabGID(head_pipeline_gid)

        labels = make_labels(session, raw_data, url.namespace, url.id)

        if project_gid := misc.get_nested_key(raw_data, 'project/id'):
            project_gid = defs.GitlabGID(project_gid)

        return {
            'approved': raw_data.get('approved', False),
            'author': author,
            'kwf_description': Description(description_text),
            'commit_count': raw_data.get('commitCount', 0),
            '_description': description_text,
            'draft': raw_data.get('draft', False),
            'files': [path['path'] for path in raw_data.get('files', [])],
            'global_id': global_id,
            'head_pipeline_gid': head_pipeline_gid,
            'head_sha': raw_data.get('diffHeadSha', ''),
            'labels': labels,
            'project_gid': project_gid,
            'source_branch': raw_data.get('sourceBranch', ''),
            'state': defs.MrState.from_str(raw_data.get('state', '')),
            'target_branch': raw_data.get('targetBranch', ''),
            'title': raw_data.get('title', '')
        }

    @classmethod
    def new(
        cls,
        session: 'BaseSession',
        mr_url: str,
        skip_files: bool = False,
        **kwargs
    ) -> typing.Self:
        """Do a query and use its results to construct a new BaseMR."""
        mr_url = GitlabURL(mr_url)
        mr_data = cls.do_mr_query(session, mr_url, skip_files)

        parsed_data = cls._parse_mr_query(session, mr_url, mr_data) if mr_data else {}

        return cls(session=session, url=mr_url, raw_data=mr_data, **parsed_data, **kwargs)

    @classmethod
    def new_from_query(
        cls,
        session: 'BaseSession',
        query_string: str,
        query_params: dict[str, typing.Any],
        paged_key: str | None = None,
        mrs_key: str = 'project/mergeRequests/nodes',
        operation_name: str | None = None,
        **kwargs: typing.Any
    ) -> list[typing.Self]:
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """Construct a list of new instances using the given query."""
        query_result = cls.query(
            session,
            query_string,
            query_params=query_params,
            paged_key=paged_key,
            check_function=None,
            operation_name=operation_name
        )

        mrs: list[cls] = []

        for mr_data in misc.get_nested_key(query_result, mrs_key, default=[]):
            mr_url = GitlabURL(mr_data['webUrl'])
            parsed_data = cls._parse_mr_query(session, mr_url, mr_data)
            mrs.append(cls(session=session, url=mr_url, raw_data=mr_data, **parsed_data, **kwargs))

        return mrs

    def _new_instance(self, mr_url: str) -> typing.Self:
        """Construct a new instance of self but using the given URL."""
        # We may be a BaseMR subclass that needs extra kwargs passed in. Get the list of
        # fields of self, excluding the standard BaseMR ones and then copy
        # those field values out of self and pass them as kwargs to BaseMR.new().
        # There has to be a better way.
        excluded_fields = [field.name for field in dataclasses.fields(BaseMR)]
        kwargs = {field.name: getattr(self, field.name) for field in dataclasses.fields(self) if
                  field.name not in excluded_fields and field.init and field.kw_only}

        return self.new(self.session, mr_url, **kwargs)

    @cached_property
    def dependency_mrs(self) -> list[typing.Self]:
        """Construct new BaseMR objects from the direct_dependencies and return them as a list."""
        base_mr_url = self.url.rsplit('/', 1)[0]
        dependency_iids = self.kwf_description.match('depends', namespace=self.namespace)

        return [self._new_instance(f'{base_mr_url}/{iid}') for iid in dependency_iids]

    @classmethod
    def do_mr_query(
        cls,
        session: 'BaseSession',
        mr_url: GitlabURL,
        skip_files: bool
    ) -> dict:
        """Run the MR_QUERY and return the results dict."""
        query_params = {
            'namespace': mr_url.namespace,
            'mr_id': str(mr_url.id),
            'skip_files': skip_files
        }
        query_result = cls.query(session, cls.MR_QUERY, query_params, operation_name='mrDetails')

        # BaseMR.query can return None.
        if not query_result:
            return {}

        return misc.get_nested_key(query_result, 'project/mr', {})


def make_labels(
    session: 'BaseSession',
    mr_data: dict,
    namespace: str,
    mr_id: int
) -> list[defs.Label]:
    """Return the list of Label objects in the raw_data."""
    raw_labels = mr_data.get('labels', {})

    if not raw_labels or 'nodes' not in raw_labels:
        return []

    if 'pageInfo' in raw_labels:
        return session.graphql.get_all_mr_labels(namespace, mr_id, raw_labels)

    return [defs.Label(label['title']) for label in raw_labels['nodes']]


def _mr_link_comment(url: str, title: str, target_branch: str, footer: str = '') -> str:
    """Return the jira-formatted 'MR link added' comment string for the given parameters."""
    mr_url = GitlabURL(url)

    mr_link_title = f'{mr_url.reference}: {title}'
    mr_link = Footer.jira_link(mr_link_title, mr_url)
    branch_link = Footer.jira_link(
        target_branch, f'{mr_url.base_url}/{mr_url.namespace}/-/tree/{target_branch}'
    )

    return f'Merge request {mr_link} targeting branch {branch_link} resolves this issue.\n' + footer
