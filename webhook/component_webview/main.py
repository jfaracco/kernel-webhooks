"""A small program to simplify userspace data lookup."""
import os

import Levenshtein
from cki_lib import misc
from cki_lib import session
from nicegui import ui
import sentry_sdk

SESSION = session.get_session('webhook.component_webview', raise_for_status=True)

# GET on webpage load to act as cached JSON for performance
COMPONENTS = SESSION.get(os.environ['COMPONENTS_JSON_URL']).json()


@ui.page("/")
def main_page():
    """Create the UI for the landing page."""
    # Search Handler Function
    async def search() -> None:
        search_field.classes("mt-2", remove="mt-2")  # move the search field up
        results.clear()

        if search_field.value == "":
            return

        def createui(component):
            ui.label(component["Name"])
            with ui.grid(columns="150px 1fr").classes("gap-2"):
                ui.label("Assignee:").classes()
                ui.label((component["Default Assignee"]))
                ui.label("QA:").classes()
                ui.label(component["QA Contact"])
                ui.label("CC-List:").classes()
                ui.label(component["Cc List"])
                ui.label("SST:").classes()
                ui.label(component["SST Pool"])
                ui.label("Filename:")
                ui.label(component["Filename"])
                ui.label("Description:").classes()
                ui.label(component["Description"])

        with results:  # enter the context of the the results row
            for product_dict in COMPONENTS["bugzilla_data"]:
                for key in product_dict.keys():
                    if radio.value == key:
                        for component in product_dict[radio.value] or []:
                            if (
                                Levenshtein.ratio(search_field.value, component["Name"])
                                >= 0.70
                            ):
                                with ui.card():
                                    createui(component=component)
                    elif radio.value == "ALL":
                        for component in product_dict[key] or []:
                            # Make fuzzy search more precise to compare across ALL products
                            if (
                                Levenshtein.ratio(search_field.value, component["Name"])
                                >= 0.75
                            ):
                                with ui.card():
                                    createui(component=component)

    # UI Elements

    # Website Header
    ui.label("Userspace Component Data").classes("text-h2 self-center")
    ui.label("Pulled from BZ Component Data").classes("text-h4 self-center")

    # Control product filtering
    radio = (
        ui.radio(
            [
                "RHEL7",
                "RHEL8",
                "RHEL9",
                "RHEL10",
                "ALL",
            ],
            value="RHEL9",
            on_change=search,
        )
        .props("inline")
        .classes("mt-10 mb-5 self-center")
    )

    # Create a search field which is initially focused and leaves space at the top
    # Assign handler function to search function defined above
    search_field = (
        ui.input()
        .on(
            "keydown.enter",
            handler=search,
        )
        .props('autofocus rounded item-aligned input-class="ml-3"')
        .classes("w-96 self-center mt-30 transition-all")
    )
    results = ui.row().classes(replace="row-span-1")


# Run the application
if __name__ in {"__main__", "__mp_main__"}:
    misc.sentry_init(sentry_sdk)
    ui.run(title="BZData", favicon="🐞", dark=True, show=False)
