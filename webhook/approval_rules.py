"""Library to represent MR Approval Rules."""
import dataclasses
from enum import StrEnum
from enum import auto
from functools import cached_property
import typing

from cki_lib.logger import get_logger
from cki_lib.misc import is_production_or_staging

from webhook import defs
from webhook.users import User

if typing.TYPE_CHECKING:
    from cki_lib.owners import Subsystem
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest

    from webhook.graphql import GitlabGraph
    from webhook.users import UserCache

LOGGER = get_logger('cki.webhook.approval_rules')

ACKS_OKAY = ' - Approval Rule "%s" already has %d ACK(s) (%d required).'
ACKS_OPTIONAL = ' - Approval Rule "%s" requests optional ACK(s) from set (%s).'
ACKS_REQUIRED = ' - Approval Rule "%s" requires at least %d ACK(s) (%d given) from set (%s).'


class ApprovalRuleType(StrEnum):
    """Possible Types for an Approval Rule."""

    # https://docs.gitlab.com/ee/api/graphql/reference/#approvalruletype
    ANY_APPROVER = auto()
    CODE_OWNER = auto()
    REGULAR = auto()
    REPORT_APPROVER = auto()


class ApprovalRuleStatus(StrEnum):
    """Status of an ApprovalRule."""

    OPTIONAL = auto()      # No approvals given, but none required.
    SATISFIED = auto()     # Has approvals >= required, or 1+ approvals if none required.
    UNSATISFIED = auto()   # Has less approvals than approvals required.


@dataclasses.dataclass(frozen=True)
class BaseApprovalRule:
    """A basic Approval Rule."""

    name: str
    type: ApprovalRuleType
    approvals_required: int
    eligible_approvers: set[User]
    mr_approved_by: set[User]

    def __post_init__(self) -> None:
        """Show yourself."""
        LOGGER.debug('Created %s', self)

    def __eq__(self, other) -> bool:
        """Return True if the name, approvals_required, and eligible_approvers match."""
        return (self.name, self.approvals_required, self.eligible_approvers) == \
            (other.name, other.approvals_required, other.eligible_approvers)

    def __repr__(self):
        """Return a string representing the Rule."""
        if self.type is ApprovalRuleType.ANY_APPROVER:
            eligible = '∞'
        else:
            eligible = len(self.eligible_approvers)
        rule_str = f"'{self.name}' ({self.type.name}):"
        rule_str += f' eligible: {eligible}, required: {self.approvals_required}'
        rule_str += f', approved by: {len(self.approved_by)}, {self.status.name}'
        return f'<{self.__class__.__name__} {rule_str}>'

    @property
    def status(self) -> ApprovalRuleStatus:
        """Return the ApprovalRuleStatus of this ApprovalRule."""
        if not self.approvals_required:
            if self.approved_by:
                return ApprovalRuleStatus.SATISFIED
            return ApprovalRuleStatus.OPTIONAL

        if self.approved:
            return ApprovalRuleStatus.SATISFIED

        return ApprovalRuleStatus.UNSATISFIED

    @property
    def status_str(self) -> str:
        """Return a string describing the status."""
        usernames_str = ', '.join(self.usernames)
        approvals = len(self.approved_by)

        if self.status is ApprovalRuleStatus.OPTIONAL:
            return ACKS_OPTIONAL % (self.name, usernames_str)

        if self.status is ApprovalRuleStatus.SATISFIED:
            return ACKS_OKAY % (self.name, approvals, self.approvals_required)

        return ACKS_REQUIRED % (self.name, self.approvals_required, approvals, usernames_str)

    @property
    def approved(self) -> bool:
        """Return True if self.approved_by >= self.required_approvals."""
        return len(self.approved_by) >= self.approvals_required

    @cached_property
    def usernames(self) -> list[str]:
        """Return the sorted list of usernames in eligible_approvers."""
        return sorted([user.username for user in self.eligible_approvers])

    @property
    def approved_by(self) -> set[User]:
        """Return the set of Users that have approved the MR which match this ApprovalRule."""
        if self.type is ApprovalRuleType.ANY_APPROVER:
            return self.mr_approved_by
        return self.eligible_approvers & self.mr_approved_by

    @cached_property
    def approvals_still_needed(self) -> int:
        """Return the number of approvals this rule still needs."""
        return max(self.approvals_required - len(self.approved_by), 0)

    def create(self, gl_mr: 'ProjectMergeRequest') -> None:
        """Create this ApprovalRule on the given MR merge request."""
        rule_data = {
            'name': self.name,
            'approvals_required': self.approvals_required
        }

        if self.eligible_approvers:
            rule_data['usernames'] = [user.username for user in self.eligible_approvers]

        # If we are creating a project approval rule on an MR then we need to set this?
        if isinstance(self, ProjectApprovalRule):
            gid: defs.GitlabGID = getattr(self, 'gid')
            rule_data['approval_project_rule_id'] = gid.id

        LOGGER.info("Creating approval rule: %s", rule_data)
        if not is_production_or_staging():
            return

        gl_mr.approval_rules.create(data=rule_data)


@dataclasses.dataclass(frozen=True, repr=False)
class ProjectApprovalRule(BaseApprovalRule):
    """A Gitlab project-level ApprovalRule."""

    # The 'overridden' attribute only exists at the MR level but we
    # have it here because an MR 'source approval rule' will have it.

    gid: defs.GitlabGID

    def update(
        self,
        graphql: 'GitlabGraph',
        mr_url: defs.GitlabURL,
        approvals_required: int,
        eligible_approvers: list[User]
    ) -> None:
        """Do a graphql mergeRequestUpdateApprovalRule mutation to update this rule."""
        LOGGER.info("Updating %s to %d approvals_required and eligible_approvers %s",
                    self, approvals_required, eligible_approvers)
        if not is_production_or_staging():
            return
        graphql.update_mr_approval_rule(
            mr_url=mr_url,
            rule_gid=self.gid,
            rule_name=self.name,
            approvals_required=approvals_required,
            eligible_approvers=eligible_approvers
        )


@dataclasses.dataclass(frozen=True, repr=False)
class MergeRequestApprovalRule(ProjectApprovalRule):
    """A Gitlab MergeRequest ApprovalRule."""

    source_rule: ProjectApprovalRule | None = None


def make_owners_approval_rule(
    entry: 'Subsystem',
    user_cache: 'UserCache',
    author: User | None = None,
    mr_approved_by: set[User] | None = None
) -> BaseApprovalRule:
    """Construct a new BaseApprovalRule from the given owners.Subsystem entry."""
    approvals_required = 1 if entry.required_approvals else 0
    eligible_approvers: set[User] = set()

    # The eligible approvers for a given subsystem are the set of all the listed reviewers and
    # maintainers but we exclude restricted users if the subsystem requires approvals.
    for approver in entry.reviewers + entry.maintainers:
        if approvals_required and approver.get('restricted'):
            LOGGER.info('Excluding %s from %s rule as they are restricted.',
                        approver['gluser'], entry.subsystem_label)
            continue

        # Exclude the given author from the list of approvers.
        if author and author.username == approver['gluser']:
            LOGGER.info('Excluding %s from %s rule as they are the author.',
                        approver['gluser'], entry.subsystem_label)
            continue

        # It is possible an owners entry username isn't a project member which means we won't be
        # able to make them part of an approval rule :/.
        if not (user := user_cache.get_by_username(approver['gluser'])):
            LOGGER.warning('User not found: %s', approver)
            continue

        eligible_approvers.add(user)

    return BaseApprovalRule(
        name=entry.subsystem_label,
        type=ApprovalRuleType.REGULAR,
        approvals_required=approvals_required,
        eligible_approvers=eligible_approvers,
        mr_approved_by=mr_approved_by or set()
    )


def make_gl_approval_rule(
    rule_dict: dict,
    user_cache: 'UserCache',
    mr_approved_by: set[User] | None = None
) -> MergeRequestApprovalRule | ProjectApprovalRule:
    """Construct the appropriate type of approval rule."""
    gid = defs.GitlabGID(rule_dict['id'])

    if gid.record_type == 'ApprovalMergeRequestRule':
        make_func = make_mr_approval_rule

    elif gid.record_type == 'ApprovalProjectRule':
        make_func = make_project_approval_rule

    else:
        raise ValueError(f'Unrecognized GID: {gid}')

    return make_func(rule_dict, user_cache, mr_approved_by)


def make_project_approval_rule(
    rule_dict: dict,
    user_cache: 'UserCache',
    mr_approved_by: set[User] | None = None
) -> ProjectApprovalRule:
    """Construct a new ProjectApprovalRule from the given graphql dict."""
    gid = defs.GitlabGID(rule_dict['id'])

    if gid.record_type != 'ApprovalProjectRule':
        raise RuntimeError("This constructor expects a global ID record type of "
                           "'ApprovalProjectRule', got: {gid}")

    # Project rules have nodes but MR's and their source project rules don't :shrug:.
    if isinstance(rule_dict['eligibleApprovers'], dict):
        raw_eligible = rule_dict['eligibleApprovers']['nodes']
    else:
        raw_eligible = rule_dict['eligibleApprovers']

    eligible_approvers = {user_cache.get_by_dict(user_dict) for user_dict in raw_eligible}

    return ProjectApprovalRule(
        gid=gid,
        name=rule_dict['name'],
        type=ApprovalRuleType[rule_dict['type']],
        approvals_required=rule_dict['approvalsRequired'],
        eligible_approvers=eligible_approvers,
        mr_approved_by=mr_approved_by or set(),
    )


def make_mr_approval_rule(
    rule_dict: dict,
    user_cache: 'UserCache',
    mr_approved_by: set[User] | None = None
) -> MergeRequestApprovalRule:
    """Construct a new MergeRequestApprovalRule from the given graphql dict."""
    gid = defs.GitlabGID(rule_dict['id'])

    if gid.record_type != 'ApprovalMergeRequestRule':
        raise RuntimeError("This constructor expects a global ID record type of "
                           "'ApprovalMergeRequestRule', got: {gid}")

    raw_eligible = rule_dict['eligibleApprovers']
    eligible_approvers = {user_cache.get_by_dict(user_dict) for user_dict in raw_eligible}

    source_rule = None
    if raw_source_rule := rule_dict.get('sourceRule'):
        source_rule = make_project_approval_rule(raw_source_rule, user_cache, mr_approved_by)

    return MergeRequestApprovalRule(
        gid=gid,
        name=rule_dict['name'],
        type=ApprovalRuleType[rule_dict['type']],
        approvals_required=rule_dict['approvalsRequired'],
        eligible_approvers=eligible_approvers,
        mr_approved_by=mr_approved_by or set(),
        source_rule=source_rule
    )


AnyApprovalRule = BaseApprovalRule | MergeRequestApprovalRule | ProjectApprovalRule
MRApprovalRules = dict[str, MergeRequestApprovalRule]
OwnersApprovalRules = dict[str, BaseApprovalRule]
ProjectApprovalRules = dict[str, ProjectApprovalRule]
