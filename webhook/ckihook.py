"""Manage the CKI labels."""
from collections import defaultdict
from dataclasses import dataclass
import sys
import typing
from urllib.parse import urlencode

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_variables
import prometheus_client as prometheus

from webhook import common
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import PipelinesMixin
from webhook.defs import GitlabObjectKind
from webhook.defs import GitlabURL
from webhook.defs import Label
from webhook.defs import MrScope
from webhook.defs import MrState
from webhook.pipelines import CKI_LABEL_PREFIXES
from webhook.pipelines import PipelineResult
from webhook.pipelines import PipelineStatus
from webhook.pipelines import PipelineType
from webhook.session import SessionRunner
from webhook.session_events import DataWarehouseEvent
from webhook.session_events import GitlabMREvent
from webhook.session_events import GitlabNoteEvent
from webhook.session_events import GitlabPipelineEvent
from webhook.session_events import GitlabPushEvent

if typing.TYPE_CHECKING:
    from .graphql import GitlabGraph
    from .pipelines import CkiFailure
    from .rh_metadata import Branch

LOGGER = logger.get_logger('cki.webhook.ckihook')

MAX_RETRIES_MISMATCHING_STATUS = misc.get_env_int('MAX_RETRIES_MISMATCHING_STATUS', 2)

METRIC_KWF_CKIHOOK_PIPELINES_RETRIED = prometheus.Counter(
    'kwf_ckihook_pipelines_retried',
    'Number of CKI pipelines retried by ckihook',
    ['bridge_name']
)

METRIC_KWF_CKIHOOK_RESULTS_RETRIED = prometheus.Counter(
    'kwf_ckihook_results_retried',
    'Number of CKI "check-kernel-results" retried by ckihook',
    ['project_name', 'reason']
)

REPORT_HEADER = '**CKI Pipelines Status:**'

NEW_ISSUE_CREATE_URL = '{jira_server}/secure/CreateIssueDetails!init.jspa'

# These are the format strings that will be used for the CreateIssueDetails link.
NEW_ISSUE_SUMMARY = 'Test failure in {test_name} for arches: {arches}'
NEW_ISSUE_DESCRIPTION = """
This issue was opened from MR {mr_url}.

The following KCIDB Tests have failed:\n{test_urls}

Please do not edit below this line!
{footer}
""".strip()


@dataclass(repr=False)
class PipeMR(PipelinesMixin, BaseMR):
    """An MR class for pipelines."""


OPEN_MRS_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mrs: mergeRequests(
      after: $after
      state: opened
      labels: ["CKI_RT::Warning"]
      targetBranches: $branches
    ) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        headPipeline {
          jobs {
            nodes {
              allowFailure
              id
              name
              createdAt
              pipeline {
                id
              }
              status
              downstreamPipeline {
                id
                project {
                  id
                }
                status
                stages {
                  nodes {
                    name
                    jobs {
                      nodes {
                        id
                        name
                        status
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
"""

RETRY_JOB_QUERY = """
mutation retryJob($job_gid: CiProcessableID!) {
  jobRetry(input: {id: $job_gid}) {
    job {
      id
    }
  }
}
"""

ALL_JOBS_QUERY = """
query allJobs($namespace: ID!, $pipeline_iid: ID!, $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    pipeline(iid: $pipeline_iid) {
      retriedJobs: jobs(retried:true) @include(if: $first) {
        nodes {
          name
        }
      }
      jobs(retried:false, after: $after) {
        nodes {
          id
          status
          name
          retryable
          stage {
            name
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
}
"""


def cki_label_changed(changes):
    """Return True if any CKI label changed, or False."""
    return any(common.has_label_prefix_changed(changes, f'{prefix}::') for
               prefix in list(CKI_LABEL_PREFIXES.values()) + ['CKI'])


def retry_pipeline(graphql, mr_id, pipeline):
    """Get a gitlab instance and retry the failed pipelines."""
    LOGGER.info('Retrying downstream pipeline %s for MR %s', pipeline, mr_id)
    if misc.is_production_or_staging():
        query_params = {'job_gid': pipeline.bridge_gid}
        graphql.client.query(RETRY_JOB_QUERY, query_params, operation_name='retryJob')
        METRIC_KWF_CKIHOOK_PIPELINES_RETRIED.labels(pipeline.bridge_name).inc()


def retry_check_results_or_prepare_python(
    session: "SessionRunner", pipeline_url: str, reason: str, max_attempts: int = 0
) -> None:
    """Retry "results" jobs in the given pipeline, or "prepare python" if there are none."""
    stage_names = [
        "check-results",  # CKI pipelines
        "kernel-results",
        "triager",  # qe-pipelines
    ]
    fallback_job_name = "prepare python"

    # Fetch Pipeline to handle its iid and status
    gl_url = GitlabURL(pipeline_url)
    gl_pipeline = (
        session.gl_instance.projects.get(gl_url.namespace, lazy=True).pipelines.get(gl_url.id)
    )

    # If the pipeline didn't finish, the job certainly didn't run, therefore can't retry
    if gl_pipeline.status not in {"failed", "success"}:
        LOGGER.info("Skipping retry on pipeline %d because it didn't finish (status=%r)",
                    gl_pipeline.id, gl_pipeline.status)
        return

    # Query pipeline jobs (plus a list of the names of all retried jobs)
    query_params = {
        'namespace': gl_url.namespace, 'pipeline_iid': gl_pipeline.iid,
    }
    query_result = session.graphql.client.query(
        ALL_JOBS_QUERY, query_params, operation_name="allJobs", paged_key="project/pipeline/jobs"
    )
    if not (jobs_list := misc.get_nested_key(query_result, 'project/pipeline/jobs/nodes', [])):
        LOGGER.info("Skipping retry on pipeline %d because there are no jobs", gl_pipeline.id)
        return

    filtered_jobs = [job for job in jobs_list if job["stage"]["name"] in stage_names]

    # if no kernel-results jobs are found, fallback to the prepare python job
    if not filtered_jobs:
        filtered_jobs = [job for job in jobs_list if job["name"] == fallback_job_name]

    for job in filtered_jobs:
        retry_count = sum(
            j["name"] == job["name"]
            # Most likely the retried jobs will show up in the first page, so no need to paginate
            for j in misc.get_nested_key(query_result, "project/pipeline/retriedJobs/nodes", [])
        )
        attempt_to_retry_job(
            session,
            job,
            pipeline_id=gl_pipeline.id,
            retry_count=retry_count,
            max_attempts=max_attempts,
            callback=lambda: METRIC_KWF_CKIHOOK_RESULTS_RETRIED.labels(
                project_name=gl_url.namespace, reason=reason
            ).inc(),
        )


def attempt_to_retry_job(  # noqa: PLR0913
    session: "SessionRunner",
    job_data: dict,
    *,
    pipeline_id: int,
    retry_count: int,
    max_attempts: int = 0,
    callback: typing.Callable,
) -> None:
    """Attempt to retry a single job."""
    # pylint: disable=too-many-arguments
    job_name = job_data["name"]
    # Can't retry if the job didn't finish
    if (job_status := job_data["status"]) not in {"FAILED", "SUCCESS"}:
        LOGGER.info(
            "Skipping %r retry on pipeline %d because the job didn't finish (status=%r)",
            job_name,
            pipeline_id,
            job_status,
        )
        return

    # Simply can't retry
    if not job_data["retryable"]:
        LOGGER.info(
            "Skipping %r retry on pipeline %d because the job was not retryable",
            job_name,
            pipeline_id,
        )
        return

    # Sometimes we can't retry forever
    LOGGER.debug("Did %d retries on %r so far", retry_count, job_name)
    if max_attempts and retry_count >= max_attempts:
        LOGGER.error(
            "Skipping %r retry on pipeline %d because we exhausted retry attempts (%d)",
            job_name,
            pipeline_id,
            retry_count,
        )
        return

    # Send request to retry
    if misc.is_production_or_staging():
        query_params = {"job_gid": job_data["id"]}
        session.graphql.client.query(RETRY_JOB_QUERY, query_params, operation_name="retryJob")

        callback()
        LOGGER.info("Retrying %r on pipeline %d", job_name, pipeline_id)
    else:
        LOGGER.info(
            "Skipping %r retry on pipeline %d because we're not in production/staging",
            job_name,
            pipeline_id,
        )


def failed_rt_mrs(graphql, namespace, branches):
    """Return a dict of namespace MRs open on the branches that failed CKI_RT with a warning."""
    query_params = {'namespace': namespace, 'branches': branches}
    result = graphql.client.query(OPEN_MRS_QUERY, query_params, paged_key='project/mrs')
    return {
        mr['iid']: misc.get_nested_key(mr, 'headPipeline/jobs/nodes')
        for mr in misc.get_nested_key(result, 'project/mrs/nodes', [])
    }


def retrigger_failed_pipelines(graphql: 'GitlabGraph', branch: 'Branch') -> None:
    """Retrigger any RT pipelines that failed on or before the MERGE stage."""
    if PipelineType.REALTIME not in branch.pipelines:
        LOGGER.info('This branch does not expect a realtime pipeline: %s', branch.pipelines)
        return
    branch_names = [branch.name, branch.name.removesuffix('-rt')]
    namespace = branch.project.namespace
    if not (mrs := failed_rt_mrs(graphql, namespace, branch_names)):
        LOGGER.info('No open MRs for %s on branches %s, nothing to do.', namespace, branch_names)
        return
    for mr_id, raw_bridge_jobs in mrs.items():
        pipelines = PipelineResult.prepare_pipelines(raw_bridge_jobs)
        if pipe := next((p for p in pipelines
                         if (stage := p.failed_stage) and (stage.name == 'merge')), None):
            retry_pipeline(graphql, mr_id, pipe)


def get_project_pipeline_var(gl_project, pipeline_id, key):
    """Return the value of the pipeline variable matching key, or None."""
    gl_pipeline = gl_project.pipelines.get(pipeline_id)
    return get_variables(gl_pipeline).get(key)


def get_pipeline_target_branch(gl_instance, project_id, pipeline_id):
    """Return the 'branch' pipeline variable value for the given project/pipeline."""
    gl_project = gl_instance.projects.get(project_id)
    return get_project_pipeline_var(gl_project, pipeline_id, 'branch')


def get_downstream_pipeline_branch(gl_instance, pipe_result):
    """Return the 'branch' var value if found, otherwise None."""
    ds_project_id = pipe_result.ds_project_id
    ds_pipeline_id = pipe_result.ds_pipeline_id

    if not (ds_branch := get_pipeline_target_branch(gl_instance, ds_project_id, ds_pipeline_id)):
        LOGGER.debug("Could not get 'branch' variable from downstream pipeline %s", ds_pipeline_id)
    return ds_branch


def branch_changed(payload):
    """Return True if it seems like the MR branch changed, otherwise False."""
    LOGGER.info('Checking if MR target branch matches latest pipeline target branch.')
    # If the MR doesn't have a head pipeline then we're done here.
    if not payload['object_attributes']['head_pipeline_id']:
        LOGGER.info('No head pipeline, skipping target branch check.')
        return False
    # This is the signature of an MR event when the target branch changes. Maybe?
    if 'merge_status' in payload['changes']:
        return True
    LOGGER.info("'changes' dict does not have 'merge_status' key, skipping target branch check.")
    return False


def process_possible_branch_change(session, pipe_mr, payload):
    """Confirm MR branch changed and if so, Return True and cancel/trigger pipelines."""
    if not pipe_mr.pipelines:
        LOGGER.info('MR has no pipelines, nothing to do.')
        return False
    ds_branch = get_downstream_pipeline_branch(session.gl_instance, pipe_mr.pipelines[0])
    mr_branch = payload['object_attributes']['target_branch']
    if ds_branch and ds_branch != mr_branch:
        LOGGER.info('Target branch changed: MR %s != downstream pipeline %s, triggering...',
                    mr_branch, ds_branch)
        if not misc.is_production_or_staging():
            LOGGER.info('Not production, skipping work.')
            return True
        head_pipeline_id = pipe_mr.head_pipeline_id
        gl_mr = pipe_mr.gl_mr
        common.cancel_pipeline(pipe_mr.gl_project, head_pipeline_id)
        new_pipeline_id = common.create_mr_pipeline(gl_mr)
        new_pipeline_url = f'{pipe_mr.gl_project.web_url}/-/pipelines/{new_pipeline_id}'
        note_text = ("This MR's current target branch has changed since the last pipeline"
                     f" was triggered. The last pipeline {head_pipeline_id} has been canceled"
                     f" and a new pipeline has been triggered: {new_pipeline_url}  \n"
                     f"Last pipeline MR target branch: {ds_branch}  \n"
                     f"Current MR target branch: {mr_branch}  ")
        session.update_webhook_comment(gl_mr, note_text)
        return True
    LOGGER.info("MR current target branch '%s' matches latest pipeline.", mr_branch)
    return False


def pipeline_is_waived(pipeline, mr_labels):
    """Return True if the pipeline is currently waived, otherwise False."""
    # Only possible to waive RT and Automotive pipelines.
    waiveable = (PipelineType.REALTIME, PipelineType.AUTOMOTIVE)
    # You can't waive a pipeline that isn't failed.
    return pipeline.type in waiveable and f'{pipeline.type.prefix}::Waived' in mr_labels and \
        pipeline.status is PipelineStatus.FAILED


def context_status_dict(
    pipelines: list[PipelineResult],
    mr_labels: list[Label]
) -> dict[PipelineStatus, list[PipelineResult]]:
    """Return a dict of failed, canceled, running, success, and waived PipelineResults."""
    waived = [pipe for pipe in pipelines if pipeline_is_waived(pipe, mr_labels)]

    failed = [p for p in pipelines if p not in waived and p.status is PipelineStatus.FAILED]
    canceled = [p for p in pipelines if p not in waived and p.status is PipelineStatus.CANCELED]
    running = [pipe for pipe in pipelines if pipe.status is PipelineStatus.RUNNING]
    success = [pipe for pipe in pipelines if pipe.status is PipelineStatus.SUCCESS]

    return {
        PipelineStatus.FAILED: failed,
        PipelineStatus.CANCELED: canceled,
        PipelineStatus.RUNNING: running,
        PipelineStatus.SUCCESS: success,
        PipelineStatus.WAIVED: waived
    }


def sort_pipeline_results(
    pipelines: dict[PipelineType, PipelineResult]
) -> tuple[list[PipelineResult], list[PipelineResult], list[PipelineType]]:
    """Sort the pipelines into lists of blocking, nonblocking, and missing types."""
    blocking = [pipe for pipe in pipelines.values() if pipe and not pipe.allow_failure]
    nonblocking = [pipe for pipe in pipelines.values() if pipe and pipe.allow_failure]
    missing = [ptype for ptype, pipeline in pipelines.items() if pipeline is None]

    LOGGER.info('Blocking pipelines: %s', blocking)
    LOGGER.info('Non-blocking pipelines: %s', nonblocking)
    LOGGER.info('Missing pipelines: %s', [ptype.name for ptype in missing])
    return blocking, nonblocking, missing


def generate_pipeline_labels(mr_cki_labels: typing.List[Label], pipelines) -> typing.Set[Label]:
    """Return the set of CKI labels derived from the given dict of PipelineResults."""
    cki_labels = set()
    for pipeline_type, pipeline_result in pipelines.items():
        if pipeline_result is None:
            LOGGER.info('Missing result for %s', repr(pipeline_type))
            cki_labels.add(Label(f'{pipeline_type.prefix}::Missing'))
            continue
        if not pipeline_result.label:
            LOGGER.info('%s has no label, ignoring result.', pipeline_result)
            continue
        if pipeline_is_waived(pipeline_result, mr_cki_labels):
            LOGGER.info('MR has Waived label for %s, ignoring result.', pipeline_result)
            # Include the waived label in the list we return since this is the list of labels
            # we expect to be present on the MR.
            cki_labels.add(Label(f'{pipeline_result.label.prefix}::Waived'))
            continue
        cki_labels.add(pipeline_result.label)
    LOGGER.info('Calculated CKI labels: %s', cki_labels)
    return cki_labels


def generate_status_label(
    pipelines: dict[PipelineType, PipelineResult | None],
    current_labels: list[Label],
    branch_pipeline_types: list[PipelineType],
) -> Label:
    """Return a Label string representing the overall status of all required pipelines."""
    # If the MR's target branch metadata lists expected pipeline types then always apply a Missing
    # label if any of those are not present.
    if missing_pipe_types := [ptype for ptype in branch_pipeline_types if not pipelines.get(ptype)]:
        LOGGER.info('Missing results for expected pipelines: %s.', missing_pipe_types)
        return Label(f'CKI::{PipelineStatus.MISSING.title}')

    default_label = PipelineStatus.MISSING
    # Get the lowest PipelineStatus of expected results which are not allowed to fail & not waived.
    lowest_status = min((pipe.status for pipe in pipelines.values() if
                         pipe is not None and
                         not pipe.allow_failure and
                         not pipeline_is_waived(pipe, current_labels)),
                        default=default_label)
    return Label(f'CKI::{lowest_status.title}')


def waiver_url(tests: list['CkiFailure'], new_issue_format: dict[str, str]) -> str:
    """Return the URL for creating a jira issue for the given CkiFailures."""
    test_name = tests[0].name

    if not all(test.name == test_name for test in tests):
        raise RuntimeError('This method requires all input CkiFailures have the same name.')

    arches = ', '.join(sorted({test.arch for test in tests}))

    new_issue_format = new_issue_format | {
        'test_urls': '\n'.join(f'[{test.pretty_name}|{test.url}]' for test in tests)
    }

    url_params = {
        'pid': 12346528,  # RHELTEST
        'issuetype': 1,   # Bug
        'labels': 'KernelQENeedsInvestigation',
        'summary': NEW_ISSUE_SUMMARY.format(test_name=test_name, arches=arches),
        'description': NEW_ISSUE_DESCRIPTION.format(**new_issue_format)
    }
    url_params = urlencode(url_params)

    url = NEW_ISSUE_CREATE_URL.format(jira_server=new_issue_format.pop('jira_server'))
    return f'{url}?{url_params}'


CkiFailureMap = dict[str, list['CkiFailure']]


def _report_pipeline_state(
    status_dict: dict[PipelineStatus, list[PipelineResult]],
    missing_pipeline_types: list[PipelineType]
) -> str:
    """Generate a markdown table string showing the current state of each pipeline."""
    report_dict = {}

    for status, pipelines in status_dict.items():
        for pipe in pipelines:
            url = pipe.checkout_url or pipe.ds_url
            name_url = f'[{pipe.type.pretty_name}]({url})'

            status_str = status.title
            if status is PipelineStatus.FAILED and pipe.failed_stage:
                status_str += f' ({pipe.failed_stage.name})'

            report_dict[name_url] = status_str

    for pipeline_type in missing_pipeline_types:
        report_dict[pipeline_type.pretty_name] = 'Missing'

    report_dict = dict(sorted(report_dict.items()))

    report = '| Pipeline | State |\n'
    report += '| --- | --- |\n'
    report += '\n'.join(f'| {name} | {status} |' for name, status in report_dict.items())

    return report


def _report_merge_failures(merge_failures: list[PipelineResult]) -> str:
    """Generate a markdown table string showing merge failures."""
    report = '| Pipeline |\n'
    report += '| --- |\n'

    for pipeline in sorted(merge_failures, key=lambda pipe: pipe.type.pretty_name):
        report += f'| [{pipeline.type.pretty_name}]({pipeline.ds_url}) |\n'

    return report


def _report_build_failures(build_failures: CkiFailureMap) -> str:
    """Generate a markdown table string showing the given CkiFailure builds."""
    report = '| Pipeline | Arches |\n'
    report += '| --- | --- |\n'

    for pipeline_name, failures in sorted(build_failures.items()):
        dw_links = [f'[{failure.pretty_arch}]({failure.url})' for
                    failure in sorted(failures, key=lambda x: x.pretty_arch)]
        pipeline_arch = ', '.join(dw_links)

        report += f'| {pipeline_name} | {pipeline_arch} |\n'

    return report


def _report_test_failures(
    test_failures: CkiFailureMap,
    new_issue_format: dict[str, str],
    include_action: bool = True
) -> str:
    """Generate a markdown table string showing the given CkiFailure tests."""
    if include_action:
        report = '| Test name | Pipeline / Arch | Action |\n'
        report += '| --- | --- | --- |\n'
    else:
        report = '| Test name | Pipeline / Arch |\n'
        report += '| --- | --- |\n'

    for test_name, failures in sorted(test_failures.items()):
        dw_links = [f'[{failure.type.pretty_name} {failure.pretty_arch}]({failure.url})' for
                    failure in failures]
        pipeline_arch = '<br>'.join(sorted(dw_links))

        if include_action:
            action = f'[Request Waiver]({waiver_url(failures, new_issue_format)})'

            report += f'| {test_name} | {pipeline_arch} | {action} |\n'
        else:
            report += f'| {test_name} | {pipeline_arch} |\n'

    return report


def _map_cki_failures(
    failed_pipes: list[PipelineResult]
) -> tuple[CkiFailureMap, CkiFailureMap, CkiFailureMap]:
    """Return a mapping of CkiFailure builds and CkiFailure tests."""
    boot_failures = defaultdict(list)  # key is PipelineType.pretty_name!
    build_failures = defaultdict(list)  # key is PipelineType.pretty_name!
    test_failures = defaultdict(list)  # key is CkiFailure.name!

    for pipeline in failed_pipes:
        if pipe_boot_failures := pipeline.boot_failures:
            boot_failures[pipeline.type.pretty_name].extend(pipe_boot_failures)
            continue

        if pipe_build_failures := pipeline.build_failures:
            build_failures[pipeline.type.pretty_name].extend(pipe_build_failures)
            continue

        for test_failure in pipeline.test_failures:
            test_failures[test_failure.name].append(test_failure)

    return boot_failures, build_failures, test_failures


REPORT_HEADER_GOOD = 'All required pipelines have passed!'

REPORT_HEADER_BAD = """
The following CKI pipelines are all required to be in OK state for this
MR to move forward.  For pipelines in a Failed state more details are given below this table.
Please contact the kernel maintainers or CKI team if a pipeline
persists in a Canceled or Missing state.
""".strip()

BOOT_FAILURE_HEADER = """
These builds have failed to boot.  Please investigate the logs in
datawarehouse to determine whether the failure is caused by the code
changes in the MR.  If that is not the case, reach out to
the kernel maintainers for assistance.
""".strip()

BUILD_FAILURE_HEADER = """
These builds have failed to compile.  Please investigate the logs in
datawarehouse to determine whether the failure is caused by the code
changes in the MR.  If that is not the case, reach out to
the kernel maintainers for assistance.
""".strip()

TEST_FAILURE_HEADER = """
These tests have failed.  Please investigate the logs in
datawarehouse to determine whether the failure is caused by the code
changes in the MR.  If that is not the case, use the provided link
to report the test issue to QE and request a waiver.
""".strip()


def generate_simple_report(
    pipelines: dict[PipelineType, PipelineResult],
    current_mr_labels: list[Label],
    new_issue_format: dict[str, str]
) -> str:
    """Generate a simplified comment reporting the CKI pipeline status for this MR."""
    blocking_pipes, _, missing_pipetypes = sort_pipeline_results(pipelines)
    pipelines_context = context_status_dict(blocking_pipes, current_mr_labels)

    failed_pipes = pipelines_context[PipelineStatus.FAILED]

    state_table = _report_pipeline_state(pipelines_context, missing_pipetypes)

    # All the pipelines must be in one of these pipelines_context lists for the report
    # to be considered "good".
    good_status = (PipelineStatus.OK, PipelineStatus.WAIVED)

    if any(pipes and status not in good_status for status, pipes in pipelines_context.items()) or \
       missing_pipetypes:
        report = f'\n### Summary\n{REPORT_HEADER_BAD}\n'
        report += f'{state_table}\n'

        boot_failures, build_failures, test_failures = _map_cki_failures(failed_pipes)

        if boot_failures:
            report += f'\n### Boot failures\n{BOOT_FAILURE_HEADER}\n'
            report += _report_build_failures(boot_failures)

        if build_failures:
            report += f'\n### Build failures\n{BUILD_FAILURE_HEADER}\n'
            report += _report_build_failures(build_failures)

        if test_failures:
            report += f'\n### Test failures\n{TEST_FAILURE_HEADER}\n'
            report += _report_test_failures(test_failures, new_issue_format, misc.is_staging())
    else:
        report = f'\n### Summary\n{REPORT_HEADER_GOOD}\n'
        report += f'{state_table}\n'

    return report


def update_mr(session, pipe_mr, pipelines):
    """Update the given MR with a comment and set CKI labels."""
    current_mr_cki_labels = set(session.webhook.webhooks_labels(pipe_mr.labels))
    new_cki_status_label = generate_status_label(
        pipelines, pipe_mr.labels, pipe_mr.rh_branch.pipelines
    )
    new_cki_pipeline_labels = generate_pipeline_labels(current_mr_cki_labels, pipelines)
    # Only include an individual pipeline's label if its status is not OK or RUNNING.
    expected_cki_labels = {new_cki_status_label}
    expected_cki_labels.update(label for label in new_cki_pipeline_labels if
                               label.scope not in (MrScope.READY_FOR_MERGE, MrScope.RUNNING))
    to_add = expected_cki_labels - current_mr_cki_labels
    to_remove = current_mr_cki_labels - expected_cki_labels
    LOGGER.info('Expected pipeline types for branch %s: %s',
                pipe_mr.rh_branch.name, pipe_mr.rh_branch.pipelines)
    LOGGER.info('Overall status label: %s', new_cki_status_label)
    LOGGER.debug('to_add: %s', to_add)
    LOGGER.debug('to_remove: %s', to_remove)
    # Update the MR labels if needed.
    if to_add:
        pipe_mr.add_labels(to_add)
    if to_remove:
        pipe_mr.remove_labels(to_remove)
    if not to_add and not to_remove:
        LOGGER.info('MR already has all the expected labels, nothing to do.')

    # Update the report comment.
    new_issue_format = {
        'jira_server': session.jira.server_url,
        'footer': session.comment.jira_footer(),
        'mr_url': pipe_mr.url
    }
    report_text = generate_simple_report(pipelines, pipe_mr.labels, new_issue_format)

    gitlab_comment_header = f'{REPORT_HEADER} ~"{new_cki_status_label}"'
    report_text = f'{gitlab_comment_header}\n\n{report_text}'

    session.update_webhook_comment(pipe_mr.gl_mr, report_text,
                                   bot_name=session.gl_user.username,
                                   identifier=REPORT_HEADER)


def process_pipe_mr(session, pipe_mr: PipeMR) -> None:
    """Populate a sorted dict of the MR's pipelines and call update_mr()."""
    # If the MR state is not 'opened' then don't even bother.
    if pipe_mr.state is not MrState.OPENED:
        LOGGER.info('MR state is %s, nothing to do.', pipe_mr.state.name)
        return
    # If the MR does not have a recognized branch then don't even bother.
    if not pipe_mr.rh_branch:
        LOGGER.info("MR target branch '%s' is not recognized, nothing to do.",
                    pipe_mr.target_branch)
        return
    # Create a mapping of PipelineType:PipelineResult pairs.
    pipelines = {}
    for pipeline in pipe_mr.pipelines:
        if pipeline.type in pipelines:
            raise ValueError(f'Multiple pipelines have the same type: {pipeline.type}')
        if pipeline.type == PipelineType.INVALID:
            continue
        pipelines[pipeline.type] = pipeline
    # Insert placeholders keys for any missing pipelines.
    for ptype in pipe_mr.rh_branch.pipelines:
        if ptype not in pipelines:
            pipelines[ptype] = None
    # Sort the pipelines so we process them in a consistent order.
    update_mr(session, pipe_mr, dict(sorted(pipelines.items())))


def fix_possible_mismatching_status(session: 'SessionRunner', event: GitlabPipelineEvent,
                                    pipe_mr: PipeMR):
    """Fix mismatching status between upstream trigger job and downstream pipeline.

    This is a workaround for the bug, usually with the upstream failing and downstream passing,
    currently tracked at: https://gitlab.com/gitlab-org/gitlab/-/issues/340064
    """
    reason = "fix_possible_mismatching_status"

    event_mr_pipeline_id = misc.get_nested_key(event.body, 'source_pipeline/pipeline_id')
    latest_mr_pipeline_id = pipe_mr.head_pipeline_gid.id if pipe_mr.head_pipeline_gid else 0
    if event_mr_pipeline_id != latest_mr_pipeline_id:
        LOGGER.info("Skipping %r on pipeline %r because it's outdated in detriment of %r",
                    reason, event_mr_pipeline_id, latest_mr_pipeline_id)
        return

    # Only try to fix when downstream pipeline has passed
    downstream_status = PipelineStatus.get(event.object_attributes["status"])
    if downstream_status is not PipelineStatus.OK:
        LOGGER.debug("Skipping %r on pipeline %r because the downstream pipeline didn't succeed",
                     reason, event.url)
        return

    # Warn if we can't find the upstream trigger job
    trigger_job_name = common.get_pipeline_variable(event.body, "trigger_job_name")
    find_status = (p.bridge_status for p in pipe_mr.pipelines if p.bridge_name == trigger_job_name)
    upstream_status = next(find_status, None)
    if upstream_status is None:
        LOGGER.warning("Skipping %r on pipeline %r because failed to find the trigger job in %r",
                       reason, event.url, pipe_mr.pipelines)
        return

    # Only try to fix when trigger job has failed
    if upstream_status is not PipelineStatus.FAILED:
        LOGGER.debug("Skipping %r on pipeline %r because the trigger job didn't fail (status=%r)",
                     reason, event.url, upstream_status.name)
        return

    # status of the upstream trigger job doesn't match downstream's pipeline
    LOGGER.error(
        ("Mismatching status (%r != %r) between upstream trigger job and"
         " downstream pipeline: %s, tentatively retrying an idempotent job."),
        upstream_status.name, downstream_status.name, event.url,
    )
    # Retrying a job is usually enough to fix it
    retry_check_results_or_prepare_python(
        session, pipeline_url=event.url, reason=reason, max_attempts=MAX_RETRIES_MISMATCHING_STATUS
    )


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: GitlabMREvent | GitlabNoteEvent | GitlabPipelineEvent,
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    pipe_mr = PipeMR.new(session, event.mr_url)
    if event.kind is GitlabObjectKind.MERGE_REQUEST and branch_changed(event.body):
        if process_possible_branch_change(session, pipe_mr, event.body):
            return

    if event.kind == GitlabObjectKind.PIPELINE and event.is_downstream:
        fix_possible_mismatching_status(session, event, pipe_mr)

    process_pipe_mr(session, pipe_mr)


def process_push_event(
    _: dict,
    session: 'SessionRunner',
    event: GitlabPushEvent,
    **__: typing.Any
) -> None:
    """Process a push event message."""
    LOGGER.info('Processing push event for %s on branch %s.',
                event.rh_branch.project.namespace, event.rh_branch)
    if not event.rh_branch.name.endswith('-rt'):
        LOGGER.info('Ignoring push to non-rt branch.')
        return
    retrigger_failed_pipelines(session.graphql, event.rh_branch)


def process_datawarehouse_event(
    _: dict,
    session: SessionRunner,
    event: DataWarehouseEvent,
    **__: typing.Any
) -> None:
    """Process an event triggered by a message from DataWarehouse."""
    LOGGER.info('Processing %s event: %r', event.type.name, event)

    # DataWarehouseEvent should've filtered event status and object_type at this point
    provenances = misc.get_nested_key(event.object, "misc/provenance", default=[])
    if not (urls := [p["url"] for p in provenances if p.get("service_name") == "gitlab"]):
        LOGGER.info("Nothing to be done to event %r. Checkout has no pipeline url", event)

    for pipeline_url in urls:
        retry_check_results_or_prepare_python(session, pipeline_url, reason=event.status)


HANDLERS = {
    GitlabMREvent: process_gl_event,
    GitlabNoteEvent: process_gl_event,
    GitlabPipelineEvent: process_gl_event,
    GitlabPushEvent: process_push_event,
    DataWarehouseEvent: process_datawarehouse_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    session = SessionRunner.new('ckihook', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
