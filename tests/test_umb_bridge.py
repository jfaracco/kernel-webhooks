"""Webhook interaction tests."""
import copy
from time import sleep
import typing
from unittest import mock

from cki_jira.common import JiraKey
from cki_jira.common import TrackerLinks
from cki_jira.common import TrackerType
from gitlab.exceptions import GitlabListError
from pika.exceptions import AMQPError

from tests.helpers import KwfTestCase
from webhook import defs
from webhook import session
from webhook import umb_bridge
from webhook.common import get_arg_parser
from webhook.kwf_tracker import KwfIssueTracker
from webhook.rh_metadata import Projects


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestUmbBridge(KwfTestCase):
    """ Test Webhook class."""

    UMB_PUBLIC = {"event": {"target": "bug",
                            "routing_key": "bug.modify",
                            "time": "2021-05-13T08:38:22",
                            "action": "modify",
                            "user": {"real_name": "Bugzilla User",
                                     "id": 12345,
                                     "login": "buguser@example.com"
                                     },
                            "bug_id": 1234567,
                            "change_set": "140589.1620895102.43339"
                            },
                  "bug": {"priority": "high",
                          "is_private": False,
                          "last_change_time": "2021-05-13T08:34:23",
                          "keywords": ["Reproducer", "Triaged"],
                          "url": "",
                          "assigned_to": {"real_name": "Bugzilla User",
                                          "id": 12345,
                                          "login": "buguser@example.com"
                                          },
                          "whiteboard": "",
                          "id": 1234567,
                          "creation_time": "2021-01-13T13:49:16",
                          "resolution": "",
                          "classification": "Red Hat",
                          "alias": [],
                          "status": {"name": "POST", "id": 26},
                          "reporter": {"real_name": "Bugzilla User",
                                       "id": 12345,
                                       "login": "omosnace@redhat.com"
                                       },
                          "summary": "A bad bug >:(",
                          "severity": "medium",
                          "flags": [],
                          "version": {"name": "8.3", "id": 5979},
                          "component": {"name": "kernel", "id": 130532},
                          "product": {"name": "Red Hat Enterprise Linux 8", "id": 370},
                          }
                  }

    UMB_PRIVATE = {"event": {"time": "2021-05-13T13:23:38",
                             "rule_id": 123,
                             "bug_id": 5454545,
                             "target": "bug",
                             "routing_key": "bug.modify",
                             "user": {"real_name": "Bugzilla User",
                                      "id": 12345,
                                      "login": "buguser@example.com"
                                      },
                             "action": "modify",
                             "change_set": "133863.1620912223.24249"
                             }
                   }

    GL_MR_URL = f'{defs.GITFORGE}/group/project/-/merge_requests/3344'
    GL_MESSAGE = {'object_kind': 'merge_request',
                  'object_attributes': {'action': 'open',
                                        'description': 'hello',
                                        'state': 'opened',
                                        'target_branch': '7.9',
                                        'url': GL_MR_URL
                                        },
                  'changes': {'description': {'previous': 'hi',
                                              'current': 'hello'
                                              }
                              },
                  'project': {'path_with_namespace': 'group/project',
                              'id': 6543}
                  }

    def setUp(self):
        """Reset session.SESSION."""
        session.SESSION = None

    def test_process_jira_event(self):
        """Adds relevant Jiras to the send_queue."""
        mock_send_queue = mock.Mock(send_function=None, environment='development', fake_queue=False)
        headers = {'message-type': 'jira'}

        mr_infos = [
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/123'),
                jiras=frozenset([defs.JiraKey('RHEL-123'), defs.JiraKey('RHEL-999')])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/334'),
                jiras=frozenset([defs.JiraKey('RHEL-123'), defs.JiraKey('RHEL-999')])
            )
        ]
        mr_info_cache = umb_bridge.MRInfoCache({mr.url: mr for mr in mr_infos})

        args = get_arg_parser('TEST').parse_args([])
        test_session = session.SessionRunner.new('umb_bridge', args, umb_bridge.HANDLERS)

        # Event is not for a kernel component issue, ignored.
        event = {'issue': {'key': 'RHEL-999', 'fields': {}}, 'user': {'name': 'not_a_bot'}}
        event['issue_event_type_name'] = 'issue_updated'
        event['webhookEvent'] = 'jira:issue_updated'
        event['issue']['fields']['components'] = [{'name': 'systemd'}]
        test_event = session.create_event(test_session, headers, event)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session, test_event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event for non-kwf issue', logs.output[-1])
            mock_send_queue.send_jira_key.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Jira is not in the cache, ignored.
        event = {'issue': {'key': 'RHEL-456', 'fields': {}}, 'user': {'name': 'example_name'}}
        event['issue_event_type_name'] = 'issue_updated'
        event['webhookEvent'] = 'jira:issue_updated'
        event['issue']['fields']['components'] = [{'name': 'kernel-rt'}]
        event['issue']['fields']['issuetype'] = {'name': 'Bug'}
        test_event = session.create_event(test_session, headers, event)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session, test_event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event not relevant to any known MRs', logs.output[-1])
            mock_send_queue.send_jira_key.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Jira not in cache, but has commit hashes populated, send to backporter
        event = {'issue': {'key': 'RHEL-456', 'fields': {}}, 'user': {'name': 'example_name'}}
        event['issue_event_type_name'] = 'issue_updated'
        event['webhookEvent'] = 'jira:issue_updated'
        event['issue']['fields']['components'] = [{'name': 'kernel'}]
        event['issue']['fields']['issuetype'] = {'name': 'Bug'}
        event['issue']['fields']['customfield_12324041'] = 'abcdef0123456789'
        test_event = session.create_event(test_session, headers, event)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session, test_event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            self.assertIn(
                'No backport labels and commit hashes populated, pass to backporter.',
                [record.message for record in logs.records]
            )
            mock_send_queue.send_gitlab_url.assert_not_called()
            mock_send_queue.send_jira_key.assert_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Jira is in the cache, queue MRs.
        event = {'issue': {'key': 'RHEL-123', 'fields': {}}, 'user': {'name': 'example_name'}}
        event['issue_event_type_name'] = 'issue_updated'
        event['webhookEvent'] = 'jira:issue_updated'
        event['issue']['fields']['components'] = [{'name': 'kernel'}]
        event['issue']['fields']['issuetype'] = {'name': 'Bug'}
        test_event = session.create_event(test_session, headers, event)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session, test_event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            msg = 'Event matches'
            self.assertIn(msg, logs.output[-1])
            self.assertEqual(mock_send_queue.send_gitlab_url.call_count, 2)
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

    def test_process_gitlab_mr(self):
        headers = {'message-type': 'gitlab'}
        mr_infos = [
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/123'),
                jiras=frozenset(['RHEL-2233445'])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/3344'),
                jiras=frozenset(['RHEL-2233445'])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/321'),
                jiras=frozenset(['RHEL-5454545'])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/111'),
                jiras=frozenset(['RHEL-5454545'])
            ),
        ]
        mr_info_cache = umb_bridge.MRInfoCache({mr.url: mr for mr in mr_infos})

        test_session = mock.Mock()

        # MR has been closed, removed it from the cache.
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            msg = copy.deepcopy(self.GL_MESSAGE)
            msg['object_attributes']['action'] = 'close'
            event = session.create_event(test_session, headers, msg)

            umb_bridge.process_gitlab_mr({}, test_session, event, mr_info_cache=mr_info_cache)
            self.assertIn('Removed <MRInfo group/project!3344', logs.output[-1])

        # MR has an unknown target branch, nothing to do.
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            msg = copy.deepcopy(self.GL_MESSAGE)
            msg['object_attributes']['target_branch'] = 'bad value'
            event = session.create_event(test_session, headers, msg)
            event.rh_branch = None

            umb_bridge.process_gitlab_mr({}, test_session, event, mr_info_cache=mr_info_cache)
            self.assertIn('Event is not associated with a recognized branch', logs.output[-1])

        # MR is added to bug_data.
        msg = copy.deepcopy(self.GL_MESSAGE)
        description = ('JIRA: https://issues.redhat.com/browse/RHEL-1234567\n'
                       'JIRA: https://issues.redhat.com/browse/RHEL-2233445')
        old_description = ('JIRA: https://issues.redhat.com/browse/RHEL-5544332\n'
                           'JIRA: https://issues.redhat.com/browse/RHEL-2233445')
        msg['changes']['description']['current'] = description
        msg['changes']['description']['previous'] = old_description
        msg['object_attributes']['description'] = description
        event = session.create_event(test_session, headers, msg)
        event.rh_branch = mock.Mock(fix_versions=[])
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_gitlab_mr({}, test_session, event, mr_info_cache=mr_info_cache)
            self.assertIn('Adding <MRInfo group/project!3344', logs.output[-1])

    def test_SendQueue(self):
        exchange = 'cki.exchange.test'
        route = 'cki.kwf.test.notice'
        send_queue = umb_bridge.SendQueue(exchange, route)
        target_hook = 'jirahook'

        mr_urls = [
            'https://gitlab.com/group/project/-/merge_requests/123',
            'https://gitlab.com/group/project/-/merge_requests/234',
            'https://gitlab.com/group/project/-/merge_requests/345'
        ]
        msg = []
        for mr_url in mr_urls:
            msg.append(
                umb_bridge.KwfMessage.create(
                    queue_name=route,
                    exchange=exchange,
                    source_hook='umb_bridge',
                    target_hook=target_hook,
                    gitlab_url=mr_url
                )
            )

        # Ensure nothing happens if send_function is not set.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='WARNING') as logs:
            send_queue.send_gitlab_url(target_hook, mr_urls[0])
            self.assertIn('send_function is not set!', logs.output[-1])
            self.assertEqual(len(send_queue.data), 0)

        # Nothing to send.
        send_function = mock.Mock()
        send_queue.send_function = send_function
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('Empty.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])

        # Add an item to the queue.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='INFO') as logs:
            send_queue.send_gitlab_url(target_hook, mr_urls[0])
            self.assertIn(f'Adding {mr_urls[0]} for jirahook', logs.output[-1])
            send_queue.send_gitlab_url(target_hook, mr_urls[1])
            self.assertIn(f'Adding {mr_urls[1]} for jirahook', logs.output[-1])
            send_queue.send_gitlab_url(target_hook, mr_urls[1])
            send_queue.send_gitlab_url(target_hook, mr_urls[2])
            self.assertIn(f'Adding {mr_urls[2]} for jirahook', logs.output[-1])
            send_queue.send_gitlab_url(target_hook, mr_urls[1])
            self.assertTrue(msg[0] in send_queue.data)
            self.assertTrue(msg[1] in send_queue.data)
            self.assertTrue(msg[2] in send_queue.data)

        # Nothing to send as nothing has been in the queue more than SEND_DELAY.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('Nothing older than SEND_DELAY (1) in data.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])

        # Wait over SEND_DELAY and now we could send but send_function is not set.
        send_queue.send_function = None
        sleep(umb_bridge.SEND_DELAY)
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('send_function is not set!', logs.output[-1])
            self.assertEqual(len(send_queue.data), 3)

        # Now we can send.
        send_queue.send_function = send_function
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn(f'Sent {msg[0].data.gitlab_url} for jirahook.', logs.output[-5])
            self.assertIn(f'Sent {msg[1].data.gitlab_url} for jirahook.', logs.output[-3])
            self.assertIn(f'Sent {msg[2].data.gitlab_url} for jirahook.', logs.output[-4])
            self.assertIn('Empty.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])
            self.assertEqual(len(send_queue.data), 0)
            self.assertEqual(send_function.call_count, 3)
            # Confirm the expected headers and data is given to the send_function.
            self.assertEqual(len(msg), len(send_function.call_args_list))
            send_function_call_kwargs = [call.kwargs for call in send_function.call_args_list]
            for test_kwf_message in msg:
                self.assertIn(test_kwf_message.asdict(), send_function_call_kwargs)

        # send_function raises an amqp exception?
        send_queue.send_gitlab_url(target_hook, mr_urls[0])
        send_function.side_effect = AMQPError('oh no!')
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='ERROR') as logs:
            exception_hit = False
            try:
                send_queue._send_message(msg[0])
            except AMQPError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error sending message: oh no!', logs.output[-1])
        send_function.reset_mock(side_effect=True)

        # Test check_status.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            with mock.patch('sys.exit') as mock_exit:
                send_queue.send_gitlab_url(target_hook, mr_urls[0])
                send_queue.check_status()
                self.assertIn('Sender thread not running as expected.', logs.output[-2])
                self.assertIn(f'Sent {mr_urls[0]} for jirahook.', logs.output[-1])
                mock_exit.assert_called_with(1)

        # Test start.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            # Test start()
            self.assertFalse(send_queue._thread.is_alive())
            send_queue.start()
            self.assertIn('Starting sender thread.', logs.output[-1])
            self.assertTrue(send_queue._thread.is_alive())

    @mock.patch('webhook.umb_bridge.SendQueue')
    @mock.patch('webhook.umb_bridge.MRInfoCache')
    @mock.patch.object(session.SessionRunner, 'consume_messages')
    def test_main(self, mock_consume_messages, mock_MrInfoCache, mock_SendQueue):
        args = ['--rabbitmq-routing-key',
                'umb.VirtualTopic.bugzilla gitlab.com.group.project.merge_request',
                '--rabbitmq-sender-exchange', 'cki.exchange.test',
                '--rabbitmq-sender-route', 'cki.route.test'
                ]

        umb_bridge.main(args)
        mock_SendQueue.assert_called_with('cki.exchange.test', 'cki.route.test', fake_queue=False)
        mock_SendQueue.return_value.start.assert_called_once()
        mock_MrInfoCache.new.assert_called_once()
        mock_consume_messages.assert_called_once_with(
            mr_info_cache=mock_MrInfoCache.new.return_value, send_queue=mock_SendQueue.return_value
        )

    def mock_gl_mr(self, namespace, id, target_branch, jiras):
        """Return a mock gl_mr."""
        mr_spec = ['target_branch', 'web_url', 'description']
        web_url = 'https://gitlab.com/%s/-/merge_requests/%d'
        jira_url = 'https://issues.redhat.com/browse/%s'
        return mock.Mock(
            spec_set=mr_spec,
            target_branch=target_branch,
            web_url=web_url % (namespace, id),
            description='\n'.join(f'JIRA: {jira_url % jira_id}' for jira_id in jiras)
        )

    def test_get_project_mr_infos(self):
        """Return a list of MRInfo objects derived from the given Project."""
        class GetMRInfosTest(typing.NamedTuple):
            project: list
            mrs: dict
            expected_infos: dict[int, list[int]]

        projects = Projects()
        proj_c9s = projects.projects[24152864]

        mr123_c9s_main = \
            self.mock_gl_mr(proj_c9s.namespace, 123, 'main', ['RHEL-1111', 'RHEL-2222'])
        mr456_c9s_main = self.mock_gl_mr(proj_c9s.namespace, 456, 'main', ['RHEL-5555'])
        mr789_c9s_bad = self.mock_gl_mr(proj_c9s.namespace, 789, 'bad', ['RHEL-7777'])

        tests = [
            GetMRInfosTest(
                project=proj_c9s,
                mrs={123: mr123_c9s_main, 456: mr456_c9s_main, 789: mr789_c9s_bad},
                expected_infos={123: ['RHEL-1111', 'RHEL-2222'], 456: ['RHEL-5555']}
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                mock_gl_instance = mock.Mock()
                mock_graphql = mock.Mock()
                with mock.patch('webhook.umb_bridge.get_project_mrs') as mock_get_mrs:
                    mock_get_mrs.return_value = list(test.mrs.values())
                    results = umb_bridge.get_project_mr_infos(
                        mock_gl_instance, mock_graphql, test.project
                    )
                    self.assertEqual(len(results), len(test.expected_infos))
                    while results:
                        mr_info = results.pop()
                        self.assertIn(mr_info.url.id, test.expected_infos)
                        self.assertCountEqual(mr_info.jiras, test.expected_infos[mr_info.url.id])
                    self.assertEqual(len(results), 0)

    def test_get_project_mrs(self):
        """Returns all the open MRs on the project."""
        class GetMrsTest(typing.NamedTuple):
            mr_list_side_effect: typing.Union[Exception, None]
            exception: typing.Union[Exception, None]

        tests = [
            # Working case. Not much to it...
            GetMrsTest(
                mr_list_side_effect=None,
                exception=False
            ),
            # Raises a 403 so returns an empty list.
            GetMrsTest(
                mr_list_side_effect=GitlabListError(response_code=403),
                exception=False
            ),
            # Raises some other error.
            GetMrsTest(
                mr_list_side_effect=GitlabListError(response_code=500),
                exception=True
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                mock_user = mock.Mock()
                mock_user.username = 'test_user'
                mock_instance = mock.Mock(user=mock_user)
                mock_gl_project = mock.Mock()
                mock_instance.projects.get.return_value = mock_gl_project
                if test.mr_list_side_effect:
                    mock_gl_project.mergerequests.list.side_effect = test.mr_list_side_effect
                    if test.exception:
                        with self.assertRaises(GitlabListError):
                            umb_bridge.get_project_mrs(mock_instance, 'namespace')
                    else:
                        self.assertEqual(umb_bridge.get_project_mrs(mock_instance, 'namespace'), [])
                else:
                    mock_gl_project.mergerequests.list.return_value = [1, 2, 3]
                    self.assertEqual(
                        umb_bridge.get_project_mrs(mock_instance, 'namespace'), [1, 2, 3]
                    )


class TestMRInfoCache(KwfTestCase):
    """Tests for the MRInfoCache thing."""

    @mock.patch('webhook.umb_bridge.get_projects_mr_infos')
    def test_mrinfocache_new(self, mock_get_projects_mr_infos):
        """Creates a new MRInfoCache object populated with MRInfos derived from the Session."""
        mock_mr_info = mock.Mock(
            url='https://123',
            cves=['CVE-1993-53266', 'CVE-2023-42344'],
            jiras=['RHEL-4254', 'RHEL-999']
        )
        mock_get_projects_mr_infos.return_value = [mock_mr_info]
        mr_cache = umb_bridge.MRInfoCache.new(mock.Mock())
        self.assertEqual(mr_cache.mr_infos, {mock_mr_info.url: mock_mr_info})
        for cve in mock_mr_info.cves:
            self.assertEqual(mr_cache.cves[cve], {mock_mr_info})
        for jira in mock_mr_info.jiras:
            self.assertEqual(mr_cache.jiras[jira], {mock_mr_info})

    def test_mrinfocache_new_raises(self):
        """Raises an error when no valid projects are derived from the session."""
        mock_session = mock.Mock(spec_set=['rh_projects'])
        mock_session.rh_projects.projects = {}

        with self.assertRaises(RuntimeError):
            umb_bridge.MRInfoCache.new(mock_session)


TEST_TASK_SUMMARY = """*This Task is created to track the testing of RHEL-61357 on the Kernel RT
 variant. Task will transition to In-progress once builds are ready and please find build
 information in the Testable build field in RHEL-61357. To facilitate the merging of the main
 kernel ticket, please transition the Task to CLOSED status after verifying or addressing any
 blocking issues.*

*If variant has failed QA, please indicate Preliminary Testing: Failed in RHEL-61357.*


Update drivers/thermal core functionality to upstream 6.12 to allow for easier backporting of
other thermal updates. """


class TestHelpers(KwfTestCase):
    """Tests for the internal umb_bridge functions."""

    def setUp(self) -> None:
        """Provide a umb_bridge SessionRunner."""
        self.test_session = self.session_runner('umb_bridge')

    def test_get_issue_keys_non_task(self) -> None:
        """Returns the issue's id (key) and that's it."""
        test_issue = KwfIssueTracker(session=self.test_session, id=JiraKey('RHEL-123'))
        self.assertEqual(
            umb_bridge._get_issue_keys(test_issue),
            {JiraKey('RHEL-123')}
        )

        # Ignores `blocks` links for non-tasks.
        test_issue = KwfIssueTracker(
            session=self.test_session,
            id=JiraKey('RHEL-123'),
            type=TrackerType.STORY,
            links=TrackerLinks(blocks=(JiraKey('RHEL-555'), ))
        )
        self.assertEqual(
            umb_bridge._get_issue_keys(test_issue),
            {JiraKey('RHEL-123')}
        )

    def test_get_issue_keys_task_blocks(self) -> None:
        """Returns the Task's id (key) plus any TrackerLinks.blocks keys."""
        test_issue = KwfIssueTracker(
            session=self.test_session,
            id=JiraKey('RHEL-123'),
            type=TrackerType.TASK,
            links=TrackerLinks(blocks=(JiraKey('RHEL-555'), ))
        )
        self.assertEqual(
            umb_bridge._get_issue_keys(test_issue),
            {JiraKey('RHEL-123'), JiraKey('RHEL-555')}
        )

    def test_get_issue_keys_task_summary(self) -> None:
        """Returns the Task's id o (key) plus the parent key from the summary."""
        test_issue = KwfIssueTracker(
            session=self.test_session,
            id=JiraKey('RHEL-123'),
            description=TEST_TASK_SUMMARY,
            type=TrackerType.TASK
        )
        self.assertEqual(
            umb_bridge._get_issue_keys(test_issue),
            {JiraKey('RHEL-123'), JiraKey('RHEL-61357')}
        )
