"""Tests for the commits module."""
from copy import deepcopy
import datetime
from pathlib import Path
import typing
from unittest import mock

from cki_jira.description import Description
from gitlab.v4.objects.commits import ProjectCommit
from gitlab.v4.objects.commits import ProjectCommitManager
from gitlab.v4.objects.projects import Project

from tests.helpers import KwfTestCase
from webhook.commits import Commit
from webhook.commits import Diff
from webhook.commits import MrVersion
from webhook.commits import VersionManager


class TestDiff(KwfTestCase):
    """Tests for the commits.Diff class."""

    def test_affected_files(self) -> None:
        """Returns the set of files (paths) modified by the Diff."""
        test_diff = Diff(
            diff='',
            old_path='include/kernel/file1.c',
            new_path='include/kernel/file2.c',
            a_mode='',
            b_mode='',
            new_file=True,
            renamed_file=True,
            deleted_file=False,
        )

        self.assertEqual(
            test_diff.affected_files,
            {'include/kernel/file1.c', 'include/kernel/file2.c'}
        )


TEST_COMMITS = [
    {'author_email': 'author_email@example.com',
     'author_name': 'Commit Author',
     'authored_date': '2024-11-19T09:54:25.000Z',
     'committed_date': '2024-11-19T09:54:25.000Z',
     'committer_email': 'committer_email@example.com',
     'committer_name': 'Commit Committer',
     'created_at': '2024-11-19T09:54:25.000Z',
     'extended_trailers': {},
     'id': '77df8df3726e7a646fd31b132c8993183ae63fc7',
     'message': 'a cool commit\n'
                '\n'
                'this is the description\n',
     'parent_ids': [],
     'short_id': '77df8df3',
     'title': 'a cool commit',
     'trailers': {},
     },
]

TEST_DIFFS = [
    {'a_mode': '100644',
     'b_mode': '100644',
     'deleted_file': False,
     'diff': '@@ -287,6 +287,7 @@\n'
             ' \tEM(rxrpc_call_see_input,\t\t"SEE input   ") \\\n'
             ' \tEM(rxrpc_call_see_release,\t\t"SEE release ") \\\n'
             ' \tEM(rxrpc_call_see_userid_exists,\t"SEE u-exists") \\\n'
             '+\tEM(rxrpc_call_see_waiting_call,\t\t"SEE q-conn  ") \\\n'
             ' \tE_(rxrpc_call_see_zap,\t\t\t"SEE zap     ")\n'
             ' \n'
             ' #define rxrpc_txqueue_traces \\\n',
     'generated_file': None,
     'new_file': False,
     'new_path': 'include/trace/events/rxrpc.h',
     'old_path': 'include/trace/events/rxrpc.h',
     'renamed_file': False},
    {'a_mode': '100644',
     'b_mode': '100644',
     'deleted_file': False,
     'diff': '@@ -516,6 +516,7 @@ void rxrpc_connect_client_calls(struct '
             'rxrpc_local *local)\n'
             ' \n'
             ' \t\tspin_lock(&local->client_call_lock);\n'
             ' \t\tlist_move_tail(&call->wait_link, &bundle->waiting_calls);\n'
             '+\t\trxrpc_see_call(call, rxrpc_call_see_waiting_call);\n'
             ' \t\tspin_unlock(&local->client_call_lock);\n'
             ' \n'
             ' \t\tif (rxrpc_bundle_has_space(bundle))\n'
             '@@ -586,7 +587,10 @@ void rxrpc_disconnect_client_call(struct '
             'rxrpc_bundle *bundle, struct rxrpc_call\n'
             ' \t\t_debug("call is waiting");\n'
             ' \t\tASSERTCMP(call->call_id, ==, 0);\n'
             ' \t\tASSERT(!test_bit(RXRPC_CALL_EXPOSED, &call->flags));\n'
             '+\t\t/* May still be on ->new_client_calls. */\n'
             '+\t\tspin_lock(&local->client_call_lock);\n'
             ' \t\tlist_del_init(&call->wait_link);\n'
             '+\t\tspin_unlock(&local->client_call_lock);\n'
             ' \t\treturn;\n'
             ' \t}\n'
             ' \n',
     'generated_file': None,
     'new_file': False,
     'new_path': 'net/rxrpc/conn_client.c',
     'old_path': 'net/rxrpc/conn_client.c',
     'renamed_file': False},
]

UNIFIED_DIFF = \
    ('diff --git a/include/trace/events/rxrpc.h b/include/trace/events/rxrpc.h\n'
     'index blahblah..blahblah 100644\n'
     '--- a/include/trace/events/rxrpc.h\n'
     '+++ b/include/trace/events/rxrpc.h\n'
     '@@ -287,6 +287,7 @@\n'
     ' \tEM(rxrpc_call_see_input,\t\t"SEE input   ") \\\n'
     ' \tEM(rxrpc_call_see_release,\t\t"SEE release ") \\\n'
     ' \tEM(rxrpc_call_see_userid_exists,\t"SEE u-exists") \\\n'
     '+\tEM(rxrpc_call_see_waiting_call,\t\t"SEE q-conn  ") \\\n'
     ' \tE_(rxrpc_call_see_zap,\t\t\t"SEE zap     ")\n'
     ' \n'
     ' #define rxrpc_txqueue_traces \\\n'
     'diff --git a/net/rxrpc/conn_client.c b/net/rxrpc/conn_client.c\n'
     'index blahblah..blahblah 100644\n'
     '--- a/net/rxrpc/conn_client.c\n'
     '+++ b/net/rxrpc/conn_client.c\n'
     '@@ -516,6 +516,7 @@ void rxrpc_connect_client_calls(struct rxrpc_local *local)\n'
     ' \n'
     ' \t\tspin_lock(&local->client_call_lock);\n'
     ' \t\tlist_move_tail(&call->wait_link, &bundle->waiting_calls);\n'
     '+\t\trxrpc_see_call(call, rxrpc_call_see_waiting_call);\n'
     ' \t\tspin_unlock(&local->client_call_lock);\n'
     ' \n'
     ' \t\tif (rxrpc_bundle_has_space(bundle))\n'
     '@@ -586,7 +587,10 @@ void rxrpc_disconnect_client_call(struct rxrpc_bundle *bundle, struct rxrpc_call\n'  # noqa: E501
     ' \t\t_debug("call is waiting");\n'
     ' \t\tASSERTCMP(call->call_id, ==, 0);\n'
     ' \t\tASSERT(!test_bit(RXRPC_CALL_EXPOSED, &call->flags));\n'
     '+\t\t/* May still be on ->new_client_calls. */\n'
     '+\t\tspin_lock(&local->client_call_lock);\n'
     ' \t\tlist_del_init(&call->wait_link);\n'
     '+\t\tspin_unlock(&local->client_call_lock);\n'
     ' \t\treturn;\n'
     ' \t}\n'
     ' \n')


class TestCommit(KwfTestCase):
    """Tests for the commits.Commit class."""

    def setUp(self) -> None:
        """Set up a mocked gitlab.v4.objects.projects.Project and test Commit."""
        super().setUp()

        mock_gl_project = mock.Mock(spec=Project)
        test_commit = Commit.new(mock_gl_project, TEST_COMMITS[0])

        self.mock_gl_project = mock_gl_project
        self.test_commit = test_commit

    def test_new(self) -> None:
        """Constructs a new Commit with the expected values."""
        test_commit = self.test_commit

        self.assertEqual(test_commit.author, ('Commit Author', 'author_email@example.com'))
        self.assertEqual(test_commit.committer, ('Commit Committer', 'committer_email@example.com'))

        self.assertIsInstance(test_commit.date, datetime.datetime)
        self.assertEqual(test_commit.description, 'this is the description\n')
        self.assertEqual(test_commit.sha, '77df8df3726e7a646fd31b132c8993183ae63fc7')
        self.assertEqual(test_commit.short_sha, '77df8df3726e')
        self.assertEqual(test_commit.title, 'a cool commit')

        self.assertIs(test_commit._gl_project, self.mock_gl_project)
        self.assertIsNone(test_commit._diffs)
        self.assertEqual(test_commit._parent_ids, [])

        self.assertIn(test_commit.short_sha, repr(test_commit))

    def test_diffs(self) -> None:
        """Fetches the diffs as needed and caches the results."""
        mock_gl_project = self.mock_gl_project
        test_commit = self.test_commit

        # Make sure the current _diffs is empty or this rest of this test is kinda useless.
        self.assertIsNone(test_commit._diffs)

        mock_gl_project.commits = mock.Mock()
        mock_gl_project.commits.get().diff.return_value = TEST_DIFFS

        result = test_commit.diffs

        self.assertEqual(len(result), 2)
        for diff in result:
            self.assertIsInstance(diff, Diff)

        self.assertEqual(
            test_commit.files,
            {'net/rxrpc/conn_client.c', 'include/trace/events/rxrpc.h'}
        )

        self.assertEqual(test_commit.unified_diff, UNIFIED_DIFF)

    def test_parent_ids(self) -> None:
        """Fetches the full commit data from gitlab REST just so we get a populated parent_ids."""
        mock_gl_project = self.mock_gl_project
        test_commit = self.test_commit

        # Make sure the current _parent_ids is empty or this rest of this test is kinda useless.
        self.assertEqual(test_commit._parent_ids, [])

        # Make a copy of the commit payload to populate our ProjectCommit with and set parent_ids
        # for it.
        gl_commit_attrs = deepcopy(TEST_COMMITS[0])
        gl_commit_attrs['parent_ids'] = ['abc123']

        test_manager = ProjectCommitManager(gl=None)
        test_gl_commit = ProjectCommit(manager=test_manager, attrs=gl_commit_attrs)

        mock_gl_project.commits = mock.Mock()
        mock_gl_project.commits.get.return_value = test_gl_commit

        result = test_commit.parent_ids

        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], 'abc123')

    def test_is_merge_commit(self) -> None:
        """Returns True if Commit.parent_ids has more than one item."""
        test_commit = self.test_commit

        test_commit.parent_ids = ['abc']
        self.assertFalse(test_commit.is_merge_commit)

        test_commit.parent_ids = ['abc', '123']
        self.assertTrue(test_commit.is_merge_commit)


class TestMrVersion(KwfTestCase):
    """Tests for the commits.MrVersion & commits.VersionManager classes."""

    files = (
        'drivers/net/ethernet/intel/iavf/iavf_ethtool.c',
        'drivers/net/ethernet/intel/igb/Makefile',
        'drivers/net/ethernet/intel/e1000e/netdev.c',
        'drivers/net/ethernet/intel/iavf/iavf_virtchnl.c',
        'drivers/net/ethernet/intel/i40e/i40e_prototype.h',
        'drivers/net/ethernet/intel/iavf/Makefile',
        'drivers/net/ethernet/intel/iavf/iavf_type.h',
        'drivers/net/ethernet/intel/igc/igc_main.c',
        'drivers/net/ethernet/intel/libeth/Makefile',
        'drivers/net/ethernet/intel/iavf/iavf_fdir.c',
        'drivers/net/ethernet/intel/libeth/rx.c',
        'include/net/libeth/types.h',
        'drivers/net/ethernet/intel/igbvf/netdev.c',
        'drivers/net/ethernet/intel/igc/Makefile',
        'drivers/net/ethernet/intel/i40e/i40e_common.c',
        'drivers/net/ethernet/intel/libeth/Kconfig',
        'drivers/net/ethernet/intel/e100.c',
        'drivers/net/ethernet/intel/ixgbe/Makefile',
        'drivers/net/ethernet/intel/iavf/iavf_main.c',
        'drivers/net/ethernet/intel/iavf/iavf_prototype.h',
        'drivers/net/ethernet/intel/iavf/iavf_common.c',
        'drivers/net/ethernet/intel/iavf/iavf_txrx.c',
        'drivers/net/ethernet/intel/ixgbevf/ixgbevf_main.c',
        'drivers/net/ethernet/intel/libie/Makefile',
        'drivers/net/ethernet/intel/igbvf/Makefile',
        'include/linux/net/intel/libie/rx.h',
        'drivers/net/ethernet/intel/ixgbevf/Makefile',
        'drivers/net/ethernet/intel/e1000e/Makefile',
        'drivers/net/ethernet/intel/libie/rx.c',
        'drivers/net/ethernet/intel/igb/igb_main.c',
        'include/net/libeth/tx.h',
        'drivers/net/ethernet/intel/ice/ice_main.c',
        'drivers/net/ethernet/intel/ice/ice_txrx_lib.c',
        'drivers/net/ethernet/intel/iavf/iavf_fdir.h',
        'drivers/net/ethernet/intel/Kconfig',
        'drivers/net/ethernet/intel/iavf/iavf.h',
        'drivers/net/ethernet/intel/ice/ice_lan_tx_rx.h',
        'drivers/net/ethernet/intel/ixgbe/ixgbe_main.c',
        'drivers/net/ethernet/intel/i40e/i40e_type.h',
        'drivers/net/ethernet/intel/iavf/iavf_txrx.h',
        'drivers/net/ethernet/intel/e1000/e1000_main.c',
        'include/net/libeth/cache.h',
        'include/net/libeth/rx.h',
        'drivers/net/ethernet/intel/Makefile',
        'drivers/net/ethernet/intel/i40e/i40e_main.c',
        'drivers/net/ethernet/intel/i40e/i40e_txrx.c',
        'drivers/net/ethernet/intel/libie/Kconfig',
        'drivers/net/ethernet/intel/fm10k/fm10k_main.c',
        'drivers/net/ethernet/intel/e1000/Makefile'
    )

    deleted_files = (
    )

    files_without_depends = (
        'drivers/net/ethernet/intel/iavf/iavf_type.h',
        'drivers/net/ethernet/intel/iavf/iavf.h',
        'drivers/net/ethernet/intel/iavf/iavf_fdir.c',
        'drivers/net/ethernet/intel/iavf/iavf_main.c',
        'drivers/net/ethernet/intel/iavf/iavf_txrx.c',
        'drivers/net/ethernet/intel/iavf/iavf_virtchnl.c',
        'drivers/net/ethernet/intel/iavf/iavf_fdir.h',
        'drivers/net/ethernet/intel/iavf/iavf_txrx.h',
        'include/linux/net/intel/libie/rx.h',
        'drivers/net/ethernet/intel/iavf/iavf_ethtool.c'
    )

    deleted_files_without_depends = (
    )

    class DepMR(typing.NamedTuple):
        """The few properties from BaseMR that MrVersion needs in a depends_mr."""

        iid: int
        kwf_description: Description
        head_sha: str

    def make_mr_responses(self, project_id: tuple[int, str], mr_id: int) -> None:
        """Set up the gitlab REST responses for the given project/mr."""
        self.responses.get(
            f'{self.GITLAB_API}/projects/{project_id[1]}',
            json=self.load_yaml_asset(f'c9s_{mr_id}_project.json', sub_module='gitlab_rest_api')
        )

        self.responses.get(
            f'{self.GITLAB_API}/projects/{project_id[0]}/merge_requests/{mr_id}',
            json=self.load_yaml_asset(f'c9s_{mr_id}.json', sub_module='gitlab_rest_api')
        )

        self.responses.get(
            f'{self.GITLAB_API}/projects/{project_id[0]}/merge_requests/{mr_id}/versions',
            json=self.load_yaml_asset(f'c9s_{mr_id}_versions.json', sub_module='gitlab_rest_api')
        )

        for vid in (1196750625, 1191445933, 1191439001, 1191436909):
            self.responses.get(
                f'{self.GITLAB_API}/projects/{project_id[0]}/merge_requests/{mr_id}/versions/{vid}',
                json=self.load_yaml_asset(
                    f'c9s_{mr_id}_versions_{vid}.json', sub_module='gitlab_rest_api'
                )
            )

    def setUp(self) -> None:
        """Set up the gitlab REST auth response."""
        super().setUp()
        self.response_gl_auth()

        self.project_id = (24152864, 'redhat%2Fcentos-stream%2Fsrc%2Fkernel%2Fcentos-stream-9')

    def test_mr_version_without_depends(self) -> None:
        """The _without_depends properties match their regular counterparts."""
        self.make_mr_responses(self.project_id, 5858)

        test_gl_project = \
            self.base_session().get_gl_project('redhat/centos-stream/src/kernel/centos-stream-9')
        test_gl_diff = \
            test_gl_project.mergerequests.get(5858, lazy=True).diffs.list(get_all=True)[0]

        depends_mrs = []

        test_version = MrVersion.new(test_gl_project, test_gl_diff, depends_mrs)

        self.assertEqual(test_version.id, 1196750625)
        self.assertEqual(test_version.base_commit_sha, 'ab82ee54a3b7d736bc3a77407f25d5232fa24d28')
        self.assertEqual(test_version.head_commit_sha, 'ce56b02210545323e947dfdcae1120cdb140bfc6')
        self.assertEqual(test_version.start_commit_sha, 'fe283c7e887b33418a20639edf103878400a8ac5')
        self.assertEqual(test_version.patch_id_sha, '63d290d8a44b1cfdb2b7533333dc8011286afde0')
        self.assertEqual(test_version.real_size, '49')
        self.assertIsInstance(test_version.created_at, datetime.datetime)
        self.assertEqual(test_version.state, 'collected')
        self.assertEqual(test_version.mr_iid, 5858)
        self.assertIs(test_version.gl_project, test_gl_project)
        self.assertIs(test_version.depends_mrs, depends_mrs)

        self.assertIn('ab82ee54a3b7…ce56b0221054', repr(test_version))

        # We created this MrVersion with an empty list of depends_mrs so there is no first_dep_sha.
        self.assertIsNone(test_version.first_dep_sha)

        self.assertCountEqual(test_version.files, self.files)
        self.assertCountEqual(test_version.deleted_files, self.deleted_files)
        self.assertIs(test_version.files, test_version.files_without_depends)
        self.assertIs(test_version.deleted_files, test_version.deleted_files_without_depends)

        commits = test_version.commits
        diffs = test_version.diffs

        self.assertEqual(len(commits), 15)
        self.assertEqual(len(diffs), 49)

        self.assertTrue(all(isinstance(c, Commit) for c in commits))
        self.assertTrue(all(isinstance(d, Diff) for d in diffs))

        # Since there were no depends_mrs given, the _without_depends properties just return their
        # regular counterpart value.
        self.assertIs(test_version.commits_without_depends, commits)
        self.assertIs(test_version.diffs_without_depends, diffs)

    def test_mr_version_with_depends(self) -> None:
        """The _without_depends properties return a subset of their regular counterparts."""
        project_id = self.project_id
        self.make_mr_responses(project_id, 5858)

        start = '3ce0b2c330a41dd577c11e50bd822ba91cdc3421'
        end = 'ce56b02210545323e947dfdcae1120cdb140bfc6'
        self.responses.get(
            f'{self.GITLAB_API}/projects/{project_id[0]}/repository/compare?from={start}&to={end}',
            json=self.load_yaml_asset(
                'c9s_5858_compare_3ce0b2c330a4_ce56b0221054.json',
                sub_module='gitlab_rest_api'
            )
        )

        test_gl_project = \
            self.base_session().get_gl_project('redhat/centos-stream/src/kernel/centos-stream-9')
        test_gl_diff = \
            test_gl_project.mergerequests.get(5858, lazy=True).diffs.list(get_all=True)[0]

        depends_mrs = [
            self.DepMR(
                5517,
                Description('JIRA: https://issues.redhat.com/browse/RHEL-59099'),
                '3ce0b2c330a41dd577c11e50bd822ba91cdc3421'
            )
        ]

        test_version = MrVersion.new(test_gl_project, test_gl_diff, depends_mrs)

        self.assertEqual(test_version.id, 1196750625)
        self.assertEqual(test_version.base_commit_sha, 'ab82ee54a3b7d736bc3a77407f25d5232fa24d28')
        self.assertEqual(test_version.head_commit_sha, 'ce56b02210545323e947dfdcae1120cdb140bfc6')
        self.assertEqual(test_version.start_commit_sha, 'fe283c7e887b33418a20639edf103878400a8ac5')
        self.assertEqual(test_version.patch_id_sha, '63d290d8a44b1cfdb2b7533333dc8011286afde0')
        self.assertEqual(test_version.real_size, '49')
        self.assertIsInstance(test_version.created_at, datetime.datetime)
        self.assertEqual(test_version.state, 'collected')
        self.assertEqual(test_version.mr_iid, 5858)
        self.assertIs(test_version.gl_project, test_gl_project)
        self.assertIs(test_version.depends_mrs, depends_mrs)

        self.assertIn('ab82ee54a3b7…ce56b0221054', repr(test_version))

        # The first_dep_sha is the sha of the first commit which has a sha matching
        # a depends_mr head_sha or has a JIRA tag matching it.
        self.assertEqual(
            test_version.first_dep_sha,
            '3ce0b2c330a41dd577c11e50bd822ba91cdc3421'
        )

        self.assertCountEqual(test_version.files, self.files)
        self.assertCountEqual(test_version.deleted_files, self.deleted_files)
        self.assertCountEqual(test_version.files_without_depends, self.files_without_depends)
        self.assertCountEqual(test_version.deleted_files_without_depends,
                              self.deleted_files_without_depends)

        commits = test_version.commits
        diffs = test_version.diffs

        self.assertEqual(len(commits), 15)
        self.assertEqual(len(diffs), 49)

        self.assertTrue(all(isinstance(c, Commit) for c in commits))
        self.assertTrue(all(isinstance(d, Diff) for d in diffs))

        # There are 15 commits total, but only eight after excluding the dependency MR content.
        self.assertEqual(test_version.commits_without_depends, commits[:8])

        self.assertEqual(len(test_version.diffs_without_depends), 10)

        expected_unified_diff = Path('tests/assets/c9s_5858_unified.diff').read_text()
        self.assertEqual(test_version.unified_diff, expected_unified_diff)

    def test_version_manager(self) -> None:
        """Presents the list of MrVersion objects for the MR."""
        self.make_mr_responses(self.project_id, 5858)

        test_session = self.base_session()
        test_gl_project = \
            self.base_session().get_gl_project('redhat/centos-stream/src/kernel/centos-stream-9')
        test_gl_mr = test_gl_project.mergerequests.get(5858)
        depends_mrs = []

        version_manager = VersionManager(test_session, test_gl_mr, depends_mrs)

        self.assertEqual(len(version_manager.versions), 4)

        latest = version_manager.latest

        self.assertEqual(latest.id, 1196750625)

        self.assertEqual(
            list(version_manager.latest_commits.values()),
            latest.commits_without_depends
        )

        self.assertEqual(version_manager.latest_diffs, latest.diffs_without_depends)
        self.assertEqual(version_manager.latest_files, latest.files_without_depends)
        self.assertEqual(version_manager.latest_deleted_files, latest.deleted_files_without_depends)
        self.assertEqual(version_manager.latest_first_dep_sha, latest.first_dep_sha)

        self.assertEqual(version_manager.latest_code_changes_version, 3)
