"""Fake JIRA Issue objects."""
from unittest import mock

from webhook.defs import EXT_TYPE_URL
from webhook.defs import JPFX


class FakeJI:
    """A fake jira.JIRA.issue object."""

    def __init__(self, id, **kwargs):
        """Set up the basic values."""
        self.id = self.issue_id = id
        # This list of attributes should match defs.JIRA_FIELDS.
        self.alias = []
        self.key = ''
        self.depends_on = []
        self.external_issues = []
        self.update = mock.Mock()
        self.fields = mock.Mock()
        self.fields.components = []
        self.fields.fixVersions = []
        self.fields.priority = 'unspecified'
        self.fields.resolution = ''
        self.fields.status = 'New'
        self.fields.summary = ''
        self.fields.labels = []
        self.fields.description = ''
        # Testing
        self.fields.customfield_12321540 = mock.Mock()
        self.fields.customfield_12321540.value = ''
        self.fields.customfield_12316142 = mock.Mock()
        self.fields.customfield_12316142.value = ''

        # Set values from kwargs.
        for item, kwarg in kwargs.items():
            if not hasattr(self, item):
                raise ValueError(f"'{item}' is not an expected attribute for FakeJI.")
            setattr(self, item, kwarg)

        # Add the 'jira' attribute since much of the code expects it to be set.
        self.jira = mock.Mock()
        self.fields.issuetype.name = 'Bug'

    def __repr__(self):
        """Return a useful identifier."""
        return f'<FakeJI #{self.id}>'


# Some dummy JIs follow

# A y-stream CVE clone in IN_PROGRESS, not Verified:Tested.
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-9.1.0'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.status = 'IN_PROGRESS'
fields.summary = 'CVE-1235-13516 kernel: a cve for the kernel i guessa'
fields.customfield_12321540.value = "foo"
fields.labels = ['security', 'CVE-1235-13516']
fields.customfield_12324749 = None
JI1234567 = FakeJI(id=1234567, key=f'{JPFX}1234567', fields=fields)

# A y-stream JI in READY_FOR_QA with Testing: Pass and external tracker set.
JI2323232_EB1 = {'ext_bz_bug_id': 'group/project/-/merge_requests/777',
                 'type': {'url': EXT_TYPE_URL}
                 }

fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-9.1.0'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.customfield_12321540.value = 'Pass'
fields.status = 'READY_FOR_QA'
fields.summary = 'some other bug'
fields.labels = []
fields.customfield_12324749 = None
JI2323232 = FakeJI(id=2323232, key=f'{JPFX}2323232', external_issues=[JI2323232_EB1], fields=fields)

# A y-stream JI in IN_PROGRESS.
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-9.1'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.status = 'IN_PROGRESS'
fields.summary = 'kernel: another bug'
fields.customfield_12321540.value = 'Not Yet Tested'
fields.labels = []
fields.customfield_12324749 = None
JI7777777 = FakeJI(id=7777777, key=f'{JPFX}7777777', fields=fields)

# A z-stream kernel-rt CVE JI in READY_FOR_QA, Testing: Pass set.
fields = mock.Mock()
fields.customfield_12321540.value = 'Pass'
version = mock.Mock()
version.name = 'rhel-9.1.0'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel-rt'
fields.components = [comp]
fields.priority = 'low'
fields.status = 'READY_FOR_QA'
fields.summary = 'CVE-2022-43210 kernel-rt: a different bug [rhel-9.1.0]'
fields.labels = ['CVE-2022-43210', 'CVE-1235-13516', 'boop']
fields.customfield_12324041 = 'abcd1234abcd is a great commit I want'
fields.customfield_12316142 = mock.Mock(value='Critical')
fields.description = 'Security Tracking Issue\n\n' \
                     'Do not make this issue public.\n\n' \
                     'Impact: Critical\n' \
                     'Reported Date: 4-Jan-2022\n' \
                     'Resolve Bug By: 8-Feb-2022\n'
fields.customfield_12324749 = None
JI2345678 = FakeJI(id=2345678, key=f'{JPFX}2345678', alias=[], fields=fields)

# A y-stream JI in TRIAGED state that has Verified:FailedQA set.
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-9.1'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.status = 'Planning'
fields.priority = 'medium'
fields.summary = 'kernel: another bug 2.0'
fields.customfield_12321540.value = 'FailedQA'
fields.labels = []
JI2673283 = FakeJI(id=2673283, key=f'{JPFX}2673283', fields=fields)

# A top-level CVE tracking bug.
fields = mock.Mock()
fields.status = 'NEW'
fields.priority = 'high'
fields.product = 'Security Response'
fields.fixVersions = []
comp = mock.Mock()
comp.name = 'vulnerability'
fields.components = [comp]
fields.summary = 'CVE-1235-13516 kernel: a bad problem'
fields.labels = ['help', 'CVE-1235-13516']
fields.customfield_12324749 = None
fields.customfield_12316142 = mock.Mock(value='Important')
fields.description = 'Security Tracking Issue\n\n' \
                     'Do not make this issue public.\n\n' \
                     'Impact: Important\n' \
                     'Reported Date: 4-Jan-2022\n' \
                     'Resolve Bug By: 8-Feb-2022\n'
JI3456789 = FakeJI(id=3456789, key=f'{JPFX}3456789', alias=['CVE-1235-13516'], fields=fields)

# A top-level CVE tracking bug with two CVEs :/.
fields = mock.Mock()
fields.status = 'CLOSED'
fields.resolution = 'DONEERRATA'
fields.priority = 'urgent'
fields.product = 'Security Response'
fields.fixVersions = []
comp = mock.Mock()
comp.name = 'vulnerability'
fields.components = [comp]
fields.summary = 'CVE-2022-7549 CVE-2022-7550 kernel: a worse problem'
fields.labels = ['bing', 'CVE-2022-7549', 'bong', 'CVE-2022-7550']
fields.customfield_12324749 = None
fields.description = ''
JI4567890 = FakeJI(id=4567890, key=f'{JPFX}4567890',
                   alias=['MegaFlaw', 'CVE-2022-7549', 'CVE-2022-7550'], fields=fields)

# An IN_PROGRESS rhel-8.7.0 Jira.
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-8.7.0'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.status = 'IN_PROGRESS'
fields.summary = 'kernel: another bug'
fields.labels = []
JI8787878 = FakeJI(id=8787878, key=f'{JPFX}8787878', fields=fields)

# A dummy issue being used by the backporter self-tests
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-8.10'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.customfield_12324041 = 'abcdef012345'
fields.summary = "jira bug title"
fields.labels = ['CVE-1984-2001']
fields.customfield_12324749 = None
JI101 = FakeJI(id=101, key=f'{JPFX}101', fields=fields)

# Another dummy issue being used by the backporter self-tests
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-8.10'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.customfield_12324041 = 'abcd1234567890'
fields.summary = "jira bug title"
fields.labels = ['CVE-1999-2000']
fields.status = 'New'
assignee = mock.Mock()
assignee.name = 'cki-kwf-bot'
fields.assignee = assignee
JI666 = FakeJI(id=666, key=f'{JPFX}666', fields=fields)

# Another dummy issue being used by the backporter self-tests
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-8.10'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.customfield_12324041 = 'abcd1234567890'
fields.summary = "jira bug title"
fields.labels = ['CVE-1999-2000', 'kwf-backport-fail']
fields.status = 'New'
assignee = mock.Mock()
assignee.name = 'cki-kwf-bot'
fields.assignee = assignee
JI667 = FakeJI(id=667, key=f'{JPFX}667', fields=fields)

# Another dummy issue being used by the backporter self-tests
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-8.10'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.customfield_12324041 = 'abcd1234567890'
fields.summary = "jira bug title"
fields.labels = ['CVE-1999-2000', 'kwf-backport-success']
fields.status = 'New'
assignee = mock.Mock()
assignee.name = 'cki-kwf-bot'
fields.assignee = assignee
JI668 = FakeJI(id=668, key=f'{JPFX}668', fields=fields)

# Another dummy issue being used by the backporter self-tests
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-8.10'
fields.fixVersions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.customfield_12324041 = ''
fields.summary = "jira bug title"
fields.labels = ['CVE-1999-2000']
fields.status = 'New'
assignee = mock.Mock()
assignee.name = 'cki-kwf-bot'
fields.assignee = assignee
JI669 = FakeJI(id=669, key=f'{JPFX}669', fields=fields)

# Another dummy issue being used by the backporter self-tests
fields = mock.Mock()
version = mock.Mock()
version.name = 'rhel-8.10'
fields.fixVersions = []
fields.versions = [version]
comp = mock.Mock()
comp.name = 'kernel'
fields.components = [comp]
fields.customfield_12324041 = 'abcd1234567890'
fields.summary = "jira bug title"
fields.labels = ['CVE-1999-2000']
fields.status = 'New'
assignee = mock.Mock()
assignee.name = 'cki-kwf-bot'
fields.assignee = assignee
JI670 = FakeJI(id=670, key=f'{JPFX}670', fields=fields)
