"""Webhook interaction tests."""
import copy
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import cdlib
from webhook import common
from webhook import fixes
from webhook import session
from webhook.base_mr_mixins import DependsMixin
from webhook.session_events import create_event


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestFixes(KwfTestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'archived': False,
                                 'web_url': 'https://web.url/g/p',
                                 'path_with_namespace': 'g/p'
                                 },
                     'object_attributes': {'target_branch': 'main', 'iid': 2,
                                           'url': 'https://web.url/g/p/-/merge_requests/2'},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'description': 'dummy description',
                     'state': 'opened',
                     'user': {'username': 'test_user'}
                     }

    def test_unsupported_object_kind(self):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        self._test_payload(False, payload=payload)

    @mock.patch('webhook.fixes.FixesMR.find_potential_missing_fixes')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    def test_merge_request(self, dep_data, ext_deps, fpmf):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = 'os-build'
        payload['object_attributes']['action'] = 'open'
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        self._test_payload(True, payload=payload)

    def test_find_intentionally_omitted_fixes(self):
        """Check the results we get from the tested function."""
        fixes_mr = mock.Mock()
        fixes_mr.omitted = set()
        description = "Omitted-fix: abcdef012345"
        fixes.FixesMR.find_intentionally_omitted_fixes(fixes_mr, description)
        self.assertEqual({'abcdef012345'}, fixes_mr.omitted)

    @mock.patch('git.Repo')
    def test_map_fixes_to_commits(self, mocked_repo):
        """Make sure we're mapping RH-Fixes entries correctly."""
        mocked_repo.git.log.return_value = ("commit aaaabbbbccccdddd\n"
                                            "Author: someone\n\n"
                                            "title\n")
        fixes_mr = mock.Mock()
        fixes_mr.repo = mocked_repo
        fixes_mr.fixes = {'1234abcd1234': 'aaaabbbbccccdddd'}
        fixes_mr.fixes = fixes.Fixes(possible={'1234abcd1234': 'aaaabbbbccccdddd'},
                                     in_mr=set(), in_omitted=set(), in_tree=set())
        rhcommit = mock.Mock()
        rhcommit.fixes = ""
        commit = mock.Mock(id='deadbeef1234', title="this is a fix")
        rhcommit.commit = commit
        rhcommit.ucids = ['1234abcd1234']
        fixes_mr.rhcommits = {'deadbeef1234': rhcommit}
        fixes.FixesMR.map_fixes_to_commits(fixes_mr)
        self.assertIn("commit aaaabbbbccccdddd", rhcommit.fixes)
        self.assertIn("RH-Fixes: deadbeef1234 (\"this is a fix\")", rhcommit.fixes)

    def test_filter_fixes(self):
        """Make sure we're filtering out existing, omitted and committed fixes correctly."""
        fixes_mr = mock.Mock()
        fixes_mr.kernel_src = "/usr/src/linux"
        fixes_mr.fixes = fixes.Fixes(possible={}, in_mr=set(), in_omitted=set(), in_tree=set())
        fixes_mr.rhcommits = {}
        fixes_mr.session.args.no_kerneloscope = False
        fixes_mr.fix_already_in_tree.return_value = []

        # Case 1: fix found in included commits
        fixes_mr.ucids = {'abcdef012345', '112233445566', 'deadbeef1234'}
        fixes_mr.omitted = {}
        mapped_fixes = {'abcdef012345': ['112233445566']}
        with self.assertLogs('cki.webhook.fixes', level='INFO') as logs:
            fixes.FixesMR.filter_fixes(fixes_mr, mapped_fixes)
            self.assertIn("Found fix 112233445566 for abcdef012345 in ucids", ' '.join(logs.output))
            self.assertEqual({}, fixes_mr.fixes.possible)

        # Case 2: fix found in Omitted-fix: list in MR desc
        fixes_mr.ucids = {}
        fixes_mr.omitted = {'8675309abcde'}
        mapped_fixes = {'abcdef012345': ['8675309abcde']}
        with self.assertLogs('cki.webhook.fixes', level='INFO') as logs:
            fixes.FixesMR.filter_fixes(fixes_mr, mapped_fixes)
            self.assertIn("Found fix 8675309abcde for abcdef012345 in omitted",
                          ' '.join(logs.output))
            self.assertEqual({}, fixes_mr.fixes.possible)

        # Case 3: fix already in tree
        fixes_mr.ucids = {}
        fixes_mr.omitted = {}
        fixes_mr.fix_already_in_tree.return_value = 'asdfasdfasdf'
        mapped_fixes = {'abcdef012345': ['8675309abcde']}
        with self.assertLogs('cki.webhook.fixes', level='INFO') as logs:
            fixes.FixesMR.filter_fixes(fixes_mr, mapped_fixes)
            self.assertIn("Found fix 8675309abcde already in tree as asdfasdfasdf",
                          ' '.join(logs.output))
            self.assertEqual({}, fixes_mr.fixes.possible)

        # Case 4: fix appears to be missing
        fixes_mr.ucids = {}
        fixes_mr.omitted = {}
        fixes_mr.fix_already_in_tree.return_value = []
        mapped_fixes = {'deadbeef1234': ['asdf8675309e']}
        with self.assertLogs('cki.webhook.fixes', level='INFO') as logs:
            fixes.FixesMR.filter_fixes(fixes_mr, mapped_fixes)
            self.assertIn("Found fix asdf8675309e for deadbeef1234 not included or referenced",
                          ' '.join(logs.output))
            self.assertEqual({'deadbeef1234': ['asdf8675309e']}, fixes_mr.fixes.possible)

    def test_fix_already_in_tree(self):
        """Make sure we find a valid commit that matches a fix."""
        fixes_mr = mock.Mock()
        fixes_mr.session.args.no_kerneloscope = True
        my_cid = '5aced5cb30b7065cdb4bf83c5adabfa37823a1e3'
        commits = [{'id': my_cid,
                    'message': ('[patch] fix a thing\n'
                                'commit asdf8675309easdf8675309easdf8675309e1234\n'
                                'Author: Joe Upstream <joe@upstream.org>\n'
                                'Date:   Fri Dec 31 23:59:59 1999 -0500\n'
                                '\n'
                                '    This is my great little fix\n'
                                '\n'
                                '    Something about this commit just feels right\n'
                                '\n'
                                'Fixes: deadbeef1234 ("The thing that was broken")\n')}]

        fixes_mr.gl_project.search.return_value = commits
        fix = 'asdf8675309e'
        fixee = 'deadbeef1234'
        commit_id = fixes.FixesMR.fix_already_in_tree(fixes_mr, fix, fixee)
        self.assertEqual(commit_id, my_cid)

        fixes_mr.gl_project.search.return_value = []
        commit_id = fixes.FixesMR.fix_already_in_tree(fixes_mr, fix, fixee)
        self.assertEqual(commit_id, None)

        fixes_mr.session.args.no_kerneloscope = False
        fixes_mr.find_already_backported.return_value = [{'commit': my_cid}]
        commit_id = fixes.FixesMR.fix_already_in_tree(fixes_mr, fix, fixee)
        self.assertEqual(commit_id, my_cid)

        fixes_mr.find_already_backported.return_value = []
        commit_id = fixes.FixesMR.fix_already_in_tree(fixes_mr, fix, fixee)
        self.assertEqual(commit_id, None)

    def test_find_missing_fixes_orig(self):
        """Test the old git-log-based fallback for finding potential missing fixes."""
        mock_fmr = mock.Mock()
        mock_fmr.repo.git.log.return_value = 'deadbeef fix broken thing\n'
        found = fixes.FixesMR.find_missing_fixes_orig(mock_fmr, 'somehash')
        self.assertEqual(found, ['deadbeef'])

        mock_fmr.repo.git.log.return_value = ''
        found = fixes.FixesMR.find_missing_fixes_orig(mock_fmr, 'somehash')
        self.assertEqual(found, [])

    def test_find_potential_missing_fixes(self):
        """Check the results we get from the tested function."""
        fixes_mr = mock.Mock()
        fixes_mr.kernel_src = "/usr/src/linux"
        fixes_mr.ucids = {'abcdef012345'}
        fixes_mr.fixes = fixes.Fixes(possible={}, in_mr=set(), in_omitted=set(), in_tree=set())
        fixes_mr.rhcommits = {}
        fixes_mr.omitted = {'8675309abcde'}
        fixes_mr.session.args.no_kerneloscope = False
        fixes_mr.find_missing_fixes.return_value = ['abcdef012345']
        fixes_mr.find_missing_fixes_orig.return_value = ['012345abcdef']
        with self.assertLogs('cki.webhook.fixes', level='DEBUG') as logs:
            fixes.FixesMR.find_potential_missing_fixes(fixes_mr)
            self.assertEqual({}, fixes_mr.fixes.possible)
            self.assertIn("Upstream commits with Fixes: ['abcdef012345']", ' '.join(logs.output))

        fixes_mr.session.args.no_kerneloscope = True
        with self.assertLogs('cki.webhook.fixes', level='DEBUG') as logs:
            fixes.FixesMR.find_potential_missing_fixes(fixes_mr)
            self.assertEqual({}, fixes_mr.fixes.possible)
            self.assertIn("Upstream commits with Fixes: ['012345abcdef']", ' '.join(logs.output))

    @mock.patch('webhook.cdlib.extract_ucid', mock.Mock())
    def test_find_upstream_commit_ids(self):
        """Make sure we can find upstream commit IDs."""
        fixes_mr = mock.Mock()
        rhcommit = mock.Mock()
        commit = mock.Mock(id='deadbeef1234', title="this is a fix")
        rhcommit.commit = commit
        rhcommit.ucids = ['1234abcd1234']
        fixes_mr.rhcommits = {'deadbeef1234': rhcommit}
        d1 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1", parent_ids=['abcd'], description=d1)
        fixes_mr.commits = {'abc123abc123': c1, 'deafdeafdeaf': mock.Mock()}
        fixes_mr.commits_without_depends = {'abc123abc123': c1}
        fixes_mr.ucids = set()

        with self.assertLogs('cki.webhook.fixes', level='DEBUG') as logs:
            fixes.FixesMR.find_upstream_commit_ids(fixes_mr)
            self.assertIn("List of 1 ucids: {'1234567890abcdef1234567890abcdef12345678'}",
                          ' '.join(logs.output))

    def test_build_fixes_comment(self):
        """Make sure we get back a sane looking fixes message."""
        fixes_mr = mock.Mock()
        fixes_mr.iid = '666'
        rhcommit = mock.Mock()
        rhcommit.fixes = "commit abcd1234\n"
        fixes_mr.rhcommits = {'deadbeef1234': rhcommit}

        fixes_mr.fixes = fixes.Fixes(possible={}, in_mr=set(), in_omitted=set(), in_tree=set())
        ret = fixes.FixesMR.build_fixes_comment(fixes_mr)
        exp = ('No missing upstream fixes for MR 666 found at this time.\n')
        self.assertEqual(ret, exp)

        fixes_mr.fixes = fixes.Fixes(possible={'deadbeef1234': 'abcd1234'},
                                     in_mr={'asdfasdf'}, in_omitted={'deadbeef'},
                                     in_tree={'feedfeed as 10101010101010101010'})
        ret = fixes.FixesMR.build_fixes_comment(fixes_mr)
        expected = ('\nPossible missing Fixes detected upstream:  \n'
                    '```\n'
                    'commit abcd1234\n'
                    '```\n'
                    'These can be resolved by either backporting and adding the referenced '
                    'commit(s) to your MR, or by adding Omitted-fix: lines to your MR '
                    'description, where appropriate.\n'
                    '\n'
                    'Upstream Fixes included in MR:\n'
                    ' - asdfasdf\n'
                    '\n'
                    'Upstream Fixes omitted via MR description:\n'
                    ' - deadbeef\n'
                    '\n'
                    'Upstream Fixes already in tree:\n'
                    ' - feedfeed as 10101010101010101010\n')
        self.assertEqual(ret, expected)

    def _test_payload(self, result, payload):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self.clear_caches()
            self._test(result, payload, [], None)
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self.clear_caches()
            self._test(result, payload, [], None)

    @mock.patch.object(DependsMixin, '_set_mr_blocks', mock.Mock())
    @mock.patch.object(fixes.FixesMR, '_commits', mock.Mock())
    @mock.patch.object(fixes.FixesMR, 'query', mock.Mock())
    @mock.patch('webhook.comments.CommentCache.update_comment', mock.Mock())
    @mock.patch('webhook.base_mr.BaseMR.add_labels', mock.Mock())
    @mock.patch.object(fixes.FixesMR, '_build_commits')
    def _test(self, result, payload, labels, assert_labels, mock_commits):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        elif payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        else:
            return
        merge_request = mock.Mock(target_branch=target)
        merge_request.author = {'id': 1, 'username': 'jdoe'}
        merge_request.iid = 2
        merge_request.state = payload["state"]
        merge_request.description = payload["description"]
        d1 = mock.Mock(text="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1", parent_ids=['abcd'],
                       description=d1)
        c1.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d2 = mock.Mock(text="2\ncommit a01234567890abcdef1234567890abcdef123456")
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2", parent_ids=['abce'],
                       description=d2)
        c2.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d3 = mock.Mock(text="3\nUpstream Status: RHEL-only")
        c3 = mock.Mock(id="890a", author_email="jdoe@redhat.com",
                       title="This is commit 3", parent_ids=['abcf'],
                       description=d3)
        c3.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d4 = mock.Mock(text="4\n"
                       "(cherry picked from commit abcdef0123456789abcdef0123456789abcdef01)")
        c4 = mock.Mock(id="deadbeef", author_email="zzzzzz@redhat.com",
                       description=d4,
                       title="This is commit 4", parent_ids=['abc0'])
        c4.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d5 = mock.Mock(text="5\nUpstream-status: Posted")
        c5 = mock.Mock(id="0ff0ff00", author_email="spam@redhat.com",
                       title="This is commit 5", parent_ids=['abc1'],
                       description=d5)
        c5.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        d6 = mock.Mock(text="6\nUpstream-status: Embargoed")
        c6 = mock.Mock(id="f00df00d", author_email="spam@redhat.com",
                       title="This is commit 6", parent_ids=['abc2'],
                       description=d6)
        c6.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])

        merge_request.labels = labels
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6]
        merge_request.pipeline = {'status': 'success'}
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "deadbeef": c4, "00f00f00": c5,
                           "f00df00d": c6}
        mock_commits.return_value = project.commits
        fixes_mr = mock.Mock()
        fixes_mr.ucids = set()
        fixes_mr.omitted = set()
        self.assertEqual(cdlib.extract_ucid(c1.description.text),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(cdlib.extract_ucid(c2.description.text),
                         ["a01234567890abcdef1234567890abcdef123456"])
        self.assertEqual(cdlib.extract_ucid(c3.description.text), [])
        self.assertEqual(cdlib.extract_ucid(c4.description.text),
                         ["abcdef0123456789abcdef0123456789abcdef01"])
        self.assertEqual(cdlib.extract_ucid(c5.description.text), [])
        self.assertEqual(cdlib.extract_ucid(c6.description.text), [])

        parser = common.get_arg_parser('FIXES')
        parser.add_argument('--linux-src')
        parser.add_argument('--kerneloscope-server-url')
        argstr = '--linux-src /src/linux --kerneloscope-server-url http://kerneloscope.example.com'
        args = parser.parse_args(argstr.split())
        args.disable_user_check = True

        mock_session = session.SessionRunner.new('fixes', args, fixes.HANDLERS)
        mock_session.get_graphql = mock.Mock()
        mock_session.rh_projects = mock.Mock()

        event = create_event(mock_session, {'message-type': 'gitlab'}, payload)

        fixes.HANDLERS[event.__class__]({}, mock_session, event)

        if result and assert_labels:
            self.assertEqual(sorted(merge_request.labels), sorted(assert_labels))
