"""Test the SAST webhook."""

from contextlib import contextmanager
import os
from unittest.mock import ANY
from unittest.mock import Mock
from unittest.mock import patch

from cki_jira.common import GitlabURL
from gitlab.v4.objects.notes import ProjectMergeRequestNote
from requests import HTTPError
import responses

from tests.fakes import FakeGitLabProject
from tests.helpers import KwfTestCase
from webhook import sast

TEST_ENVIRON = {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}


@patch.dict(os.environ, TEST_ENVIRON, clear=True)
class TestSastGetSastItem(KwfTestCase):
    """Test the get_sast_item function in the SAST webhook."""

    def test_not_numeric_mr_id(self):
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    "asdasdsad")
        self.assertEqual(result, None)

    def test_not_valid_url(self):
        result = sast.get_sast_item("lakhsdlahdlashd",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_requests_get_returns_none(self, mock_req):
        mock_req.return_value = None
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_requests_get_throws_exception(self, mock_req):
        mock_req.side_effect = NotImplementedError
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_requests_req_is_none(self, mock_sess):
        ret_val = Mock()
        ret_val.get.return_value = None
        mock_sess.return_value = ret_val
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    def _dummy_raise_for_status(self):
        raise HTTPError("Oh no! An errror occurred!")

    def _dummy_json(self):
        return {"success": True}

    @patch("webhook.sast.get_session")
    def test_req_raise_for_status_throws_exception(self, mock_req):
        req = Mock()
        req.status_code = 1
        req.raise_for_status.side_effect = NotImplementedError
        ret_val = Mock()
        ret_val.get.return_value = req
        mock_req.return_value = ret_val
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_successful_json(self, mock_req):
        req = Mock()
        req.status_code = 200
        req.raise_for_status.return_value = self._dummy_json()
        req.json.return_value = self._dummy_json()
        ret_val = Mock()
        ret_val.get.return_value = req
        mock_req.return_value = ret_val
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, self._dummy_json())


@patch.dict(os.environ, TEST_ENVIRON, clear=True)
class TestFindingsToGitlabHtml(KwfTestCase):
    """Test the findings_to_gitlab_html function in the SAST webhook"""

    @patch.dict(os.environ, TEST_ENVIRON, clear=True)
    def setUp(self) -> None:
        """Provide a test SessionRunner and sast.MR instance."""
        super().setUp()

        session = self.session_runner("sast")
        sast_mr = sast.MR(
            session=session,
            url=GitlabURL("https://gitlab.com/redhat/rhel/src/kernel/rhel-8/-/merge_requests/5"),
            target_branch='main'
        )

        self.session = session
        self.sast_mr = sast_mr

    def test_empty_findings_list(self) -> None:
        """Returns an empty string when the input list of findings is empty."""
        self.assertEqual(
            sast._findings_to_gitlab_html([], self.sast_mr),
            ""
        )

    def test_bad_findings_list(self) -> None:
        """Logs an error when some item in the findings_list cannot be parsed."""
        with self.assertLogs(level="ERROR"):
            sast._findings_to_gitlab_html([123], self.sast_mr)

        with self.assertLogs(level="ERROR"):
            sast._findings_to_gitlab_html([""], self.sast_mr)

    def test_correct_items(self):
        """Returns an HTML string with the expected content."""
        test_finding = """Error: OVERRUN (CWE-119):
aa/bbbb/dddd.c:5432:2: overrun-buffer-val: Overrunning struct type page of 1 64-byte """ + \
"""elements by passing it to a function which accesses it at element index 1 (byte offset 127).
# 5430|   	struct very_cool_struct *struct_ptr;
# 5431|   
# 5432|-> 	struct_ptr = some_init_function(another_function(*param, 8));
# 5433|   	if (struct_ptr) {
# 5434|   		return return_value;"""  # noqa: E128, W291
        info_txt = "The following list contains findings from performing a Static Analysis" + \
                   " Security Testing (SAST) on this MR. For more details on what needs to " + \
                   "be done, please review [our FAQ]" + \
                   "(https://docs.google.com/document/d/" + \
                   "1giNDNRzDh2C8EO5zdNaadACYGlWzhRgUkpMJujNFCZc). " + \
                   "This activity is currently non-gating, but the RHEL " + \
                   "Security Architects encourage you to perform it prior to " + \
                   "merging.\n\n"
        expected_result = info_txt + """\n<details>
    <summary>
        Error: OVERRUN (<a href="https://cwe.mitre.org/data/definitions/119.html" target="_blank">CWE-119</a>): aa/bbbb/dddd.c:5432&nbsp;&nbsp;&nbsp;<a href="https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?pid=12331126&issuetype=3&labels=RHEL&labels=SecArch&security=11697&priority=10200&summary=False+positive+finding+in+kernel+MR&description=Please%2C+add+the+following+OSH+error+from+%5Brhel-8+MR+5%7Chttps%3A%2F%2Fgitlab.com%2Fredhat%2Frhel%2Fsrc%2Fkernel%2Frhel-8%2F-%2Fmerge_requests%2F5%5D+to+the+list+of+known+false+positives%3A%0A%0AError%3A+OVERRUN+%28CWE-119%29%3A+aa%2Fbbbb%2Fdddd.c%3A5432%0A%0AReason%3A+%7BTYPE_THE_REASON_FOR_FALSE_POSITIVE%7D">&#10007;</a>
        &nbsp;&nbsp;&nbsp;<a href="https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?pid=12332745&issuetype=1&labels=SAST&labels=Kernel&security=11694&priority=10200&summary=Kernel+rhel-8+MR+5%3A+Error%3A+OVERRUN+%28CWE-119%29%3A+aa%2Fbbbb%2Fdddd.c%3A5432">&#10003;</a>
    </summary>
    <pre>
Error: OVERRUN (CWE-119):
aa/bbbb/dddd.c:5432:2: overrun-buffer-val: Overrunning struct type page of 1 64-byte """ + \
"""elements by passing it to a function which accesses it at element index 1 (byte offset 127).
# 5430|   	struct very_cool_struct *struct_ptr;
# 5431|   
# 5432|-> 	struct_ptr = some_init_function(another_function(*param, 8));
# 5433|   	if (struct_ptr) {
# 5434|   		return return_value;
    </pre>
</details>"""  # noqa: E501, E128, W291
        result = sast._findings_to_gitlab_html([test_finding], self.sast_mr)
        self.assertEqual(result, expected_result)


@patch.dict(os.environ, TEST_ENVIRON, clear=True)
@patch('webhook.sast._check_update_comment')
@patch('webhook.common.add_label_to_merge_request')
class TestUpdateMr(KwfTestCase):
    """Test the _update_mr function in the SAST webook."""

    @patch.dict(os.environ, TEST_ENVIRON, clear=True)
    def setUp(self) -> None:
        """Set up a dummy gl_project with gl_mr."""
        super().setUp()

        mock_gl_project = FakeGitLabProject(
            project_id=999, path_with_namespace="group/project"
        )

        mock_gl_mr = mock_gl_project.add_mr(
            mr_id=123, actual_attributes={"labels": []}
        )

        self.mock_gl_project = mock_gl_project
        self.mock_gl_mr = mock_gl_mr
        self.test_session = self.session_runner("sast")

    def test_no_label_change(self, mock_add_label, mock_check_update_comment) -> None:
        """Does not call add_label_to_merge_request because the desired label is already set."""
        mock_gl_mr = self.mock_gl_mr
        mock_gl_mr.labels = ["SAST::OK"]

        sast._update_mr(
            self.test_session,
            self.mock_gl_project,
            123,
            "OK",
            "bla bla bla"
        )

        mock_add_label.assert_not_called()
        mock_check_update_comment.assert_called_once()

    def test_with_label_change(self, mock_add_label, mock_check_update_comment) -> None:
        """Calls add_label_to_merge_request because the label needs an update."""
        mock_gl_mr = self.mock_gl_mr
        mock_gl_mr.labels = ["SAST::NeedsReview"]

        sast._update_mr(
            self.test_session,
            self.mock_gl_project,
            123,
            "OK",
            "bla bla bla"
        )

        mock_add_label.assert_called_once_with(ANY, ANY, ["SAST::OK"])
        mock_check_update_comment.assert_called_once()


@patch.dict(os.environ, TEST_ENVIRON, clear=True)
class TestCheckUpdateComment(KwfTestCase):
    """Test the _check_update_comment function in the SAST webook."""

    @patch.dict(os.environ, TEST_ENVIRON, clear=True)
    def setUp(self) -> None:
        """Set up a dummy gl_project/gl_mr and patch out SessionRunner.update_webhook_comment."""
        super().setUp()

        mock_gl_project = FakeGitLabProject(
            project_id=999, path_with_namespace="group/project"
        )
        mock_gl_project.manager.gitlab.user.username = 'bot_user_555'

        mock_gl_mr = mock_gl_project.add_mr(
            mr_id=123, actual_attributes={"labels": []}
        )
        mock_gl_mr.notes = Mock(spec=["list"], list=Mock(spec=[], return_value=[]))

        self.mock_gl_project = mock_gl_project
        self.mock_gl_mr = mock_gl_mr

        test_session = self.session_runner("sast")
        test_session.update_webhook_comment = Mock()
        self.test_session = test_session

        self.expected_report_text = sast.REPORT_HEADER + ' ~"SAST::{label}"\n\n{comment_text}'

    def make_note(
        self,
        body: str,
        username: str,
        note_id: int = 777,
        system: bool = False
    ) -> ProjectMergeRequestNote:
        """Make a dummy Note with the given values."""
        return Mock(
            spec=ProjectMergeRequestNote,
            body=body,
            author={"username": username},
            id=note_id,
            system=system
        )

    def test_no_existing_comment(self) -> None:
        """Calls update_webhook_comment to post a new comment when no existing one matches."""
        test_session = self.test_session
        mock_gl_mr = self.mock_gl_mr

        comment_text = "this is a great comment oh boy."
        expected_report_text = self.expected_report_text.format(
            label="OK", comment_text=comment_text
        )

        sast._check_update_comment(
            test_session,
            comment_text,
            mock_gl_mr,
            self.mock_gl_project,
            "OK"
        )

        test_session.update_webhook_comment.assert_called_once_with(
            mock_gl_mr,
            expected_report_text,
            bot_name="bot_user_555",
            identifier=sast.REPORT_HEADER
        )

    def test_existing_comment(self) -> None:
        """Does not call update_webhook_comment because an existing one matches the input."""
        test_session = self.test_session
        mock_gl_mr = self.mock_gl_mr

        comment_text = "this is a great comment oh boy."
        expected_report_text = self.expected_report_text.format(
            label="OK", comment_text=comment_text
        )

        test_note1 = self.make_note("hey", "example_user", note_id=5432)
        test_note2 = self.make_note(expected_report_text + "footer", "bot_user_555", note_id=73654)

        mock_gl_mr.notes.list.return_value = [test_note1, test_note2]

        with self.assertLogs('cki.webhook.sast') as logs:
            sast._check_update_comment(
                test_session,
                comment_text,
                mock_gl_mr,
                self.mock_gl_project,
                "OK"
            )

            self.assertTrue(
                any(log for log in logs.records if "Matches note" in log.msg and 73654 in log.args)
            )

        test_session.update_webhook_comment.assert_not_called()


@patch.dict(os.environ, TEST_ENVIRON, clear=True)
class TestProcessGlEvent(KwfTestCase):
    """Test the process_gl_event function in the SAST webhook."""

    class TestMr():
        """Mocks the webhook.sast.MR class"""
        project = Mock()
        gl_project = Mock()

        @classmethod
        def new(cls, session, url):
            return cls(None, None, None, url)

        def __init__(self, gql, gli, rhp, url):
            items = url.split("/-/merge_requests/")
            self.project.name = items[0]
            self.iid = int(items[1])

    def _dummy_get_sast_item(url, typ, iid):
        if typ == sast.UrlSubitem.STATUS:
            if iid == 0:
                return {"status": "finished", "amount": 0}
            elif iid == -1:
                return None
            elif iid < -1:
                return {"status": "pending", "amount": 0}
            else:
                return {"status": "finished", "amount": iid}
        else:
            if iid == 1:
                return {"findings": ["\n"]}
            elif iid == 2:
                return None
            else:
                return {"findings": ["Item1", "Item2", "Item3"]}

    @patch('webhook.sast.MR', wraps=TestMr)
    @patch('webhook.sast.get_sast_item', wraps=_dummy_get_sast_item)
    @patch('webhook.sast._update_mr')
    def test_mocked_mr(self, mr, get_sast_item, update_mr_fn):
        session = Mock()
        session.graphql = Mock()
        session.gl_instance = Mock()
        session.rh_projects = Mock()
        session.args.sast_target_rhel = "rhel-9"
        event = Mock()
        event.kind = Mock()
        event.kind.name = ""

        event.mr_url = "some_url/-/merge_requests/34"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/-1"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/-2"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/0"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/1"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/2"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/3"
        sast.process_gl_event(None, session, event)


@patch.dict(os.environ, TEST_ENVIRON, clear=True)
class TestMain(KwfTestCase):
    """Test the main function of the SAST webhook."""

    @contextmanager
    def assertNotRaises(self, exc_type):
        try:
            yield None
        except exc_type:
            raise self.failureException('{} raised'.format(exc_type.__name__))

    def test_main_sast_rest_api_url_set(self):
        with self.assertRaises(RuntimeError):
            sast.main(["--sast-rest-api-url", "some-url"])

    @patch("webhook.session.SessionRunner.run", return_value=Mock())
    def test_main_all_needed_set(self, sr_run):
        with self.assertNotRaises(Exception):
            sast.main(["--sast-rest-api-url", "some-url", "--rabbitmq-routing-key", "some-key"])

    @responses.activate
    @patch('webhook.sast.process_mr')
    def test_main_run_nudger_correct(self, patched_process_mr):
        responses.get("https://gitlab.com/api/v4/user", json={})
        responses.get("https://gitlab.com/api/v4/projects/redhat%2Fcentos-stream%2Fsrc%2F" +
                      "kernel%2Fcentos-stream-9", json={"id": 3})
        responses.get("https://gitlab.com/api/v4/projects/3/merge_requests?state=opened&" +
                      "per_page=100", json=[{"iid": 4652, 'web_url':
                                             "https://gitlab.com/redhat/centos-stream/src/" +
                                             "kernel/centos-stream-9/-/merge_requests/4652"}])
        CURRENT_USER = {'email': 'bot@example.com',
                        'gid': 'gid://gitlab/User/7654321',
                        'name': 'Bot User',
                        'username': 'bot_user'}
        AUTHOR1 = {'email': 'test_user1@example.com',
                   'gid': 'gid://gitlab/User/1111',
                   'name': 'Test User1',
                   'username': 'test_user1'}
        MR1_QUERY = {'author': AUTHOR1,
                     'approved': False,
                     'commitCount': 2,
                     'description': 'This is a dummy NR',
                     'draft': False,
                     'files': [{'path': 'include/linux.h'}],
                     'headPipeline': {'id': 'gid://gitlab/Ci::Pipeline/12345678'},
                     'global_id': 'gid://gitlab/MergeRequest/12121212',
                     'labels': {'nodes': []},
                     'project': {'id': 'gid://gitlab/Project/24152864'},
                     'state': 'opened',
                     'sourceBranch': 'feature',
                     'targetBranch': 'main',
                     'title': 'Another test MR'}
        MR1_QUERY_RESULT = {'currentUser': CURRENT_USER, 'project': {'mr': MR1_QUERY}}
        responses.post("https://gitlab.com/api/graphql", json={"data": MR1_QUERY_RESULT})
        responses.get("https://gitlab.com/api/v4/projects/3/merge_requests/4652",
                      json={"labels": []})
        with self.assertNotRaises(Exception):
            sast.main(["--sast-rest-api-url", "some-url", "--gl-projects",
                      ["redhat/centos-stream/src/kernel/centos-stream-9"]])

        patched_process_mr.assert_called_once()

    def test_main_run_nudger_proj_not_in_metadata(self):
        with self.assertRaises(ValueError):
            sast.main(["--sast-rest-api-url", "some-url", "--gl-projects",
                      ["some/custom/namespace/project"]])
