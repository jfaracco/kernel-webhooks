"""Tests for the base_mr mixins."""
from dataclasses import asdict
from dataclasses import dataclass
from datetime import datetime
import random
from string import ascii_letters
from unittest import mock

from cki_lib import yaml
from cki_lib.owners import Parser
import responses
from responses.matchers import json_params_matcher

from tests import fake_payloads
from tests import fakes
from tests.helpers import KwfTestCase
from tests.test_approval_rules import ALL_MEMBERS_RULE
from tests.test_approval_rules import X86_RULE
from tests.test_users import USERS
from webhook import base_mr_mixins
from webhook.base_mr import BaseMR
from webhook.defs import ALL_MEMBERS_APPROVAL_RULE
from webhook.defs import GITFORGE
from webhook.defs import GitlabGID
from webhook.defs import GitlabURL
from webhook.graphql import GitlabGraph
from webhook.pipelines import PipelineResult
from webhook.session import BaseSession
from webhook.users import User

API_URL = f'{GITFORGE}/api/graphql'
NAMESPACE = 'group/project'
MR_ID = 123
MR_URL = GitlabURL(f'{GITFORGE}/{NAMESPACE}/-/merge_requests/{MR_ID}')

OWNERS_HEADER = "subsystems:\n"

OWNERS_ENTRY_1 = (" - subsystem: A dummy subsystem\n"
                  "   maintainers: &group0\n"
                  "     - name: User 0\n"
                  "       email: user0@redhat.com\n")

OWNERS_ENTRY_2 = (" - subsystem: Some Subsystem\n"
                  "   labels:\n"
                  "     name: redhat\n"
                  "     readyForMergeDeps:\n"
                  "       - testDep\n"
                  "   status: Maintained\n"
                  "   requiredApproval: true\n"
                  "   maintainers:\n"
                  "     - name: User 1\n"
                  "       email: user1@redhat.com\n"
                  "     - name: User 2\n"
                  "       email: user2@redhat.com\n"
                  "   reviewers:\n"
                  "     - *group0\n"
                  "     - name: User 3\n"
                  "       email: user3@redhat.com\n"
                  "     - name: User 4\n"
                  "       email: user4@redhat.com\n"
                  "       restricted: true\n"
                  "   devel-sst:\n"
                  "     - rhel-sst-something\n"
                  "   qe-sst:\n"
                  "     - rhel-sst-somethingelse\n"
                  "   paths:\n"
                  "       includes:\n"
                  "          - makefile\n"
                  "          - redhat/\n"
                  "       includeRegexes:\n"
                  "          - bpf\n"
                  "       excludes:\n"
                  "          - redhat/configs/\n"
                  "   scm: git https://gitlab.com/cki-project/kernel-ark.git\n"
                  "   mailingList: https://somelist.redhat.com/xxx/\n")


class TestApprovalsMixin(KwfTestCase):
    """Tests for the ApprovalsMixin."""

    @dataclass(repr=False)
    class BasicApprovals(base_mr_mixins.ApprovalsMixin, BaseMR):
        """A dummy class for testing the ApprovalsMixin."""

    def test_approval_rules_property(self):
        """Returns a dict of the expected ApprovalRule objects."""
        self.response_gql_user_data()
        test_session = self.base_session()
        match_query = base_mr_mixins.ApprovalsMixin.APPROVALS_QUERY.strip('\n')

        rules_list = [ALL_MEMBERS_RULE, X86_RULE]
        json_response = {'project': {'mr': {'approvalState': {'rules': rules_list}}}}
        self.responses.post(self.GITLAB_GRAPHQL, json={'data': json_response},
                            match=[json_params_matcher({'query': match_query,
                                                        'variables': {'namespace': NAMESPACE,
                                                                      'mr_id': str(MR_ID)}},
                                                       strict_match=False)])

        basic_approvals = self.BasicApprovals(session=test_session, url=MR_URL)
        found_rules = basic_approvals.approval_rules

        self.assertEqual(len(found_rules), 2)
        self.assertTrue('x86' in found_rules)
        self.assertTrue(ALL_MEMBERS_APPROVAL_RULE in found_rules)

    def test_approved_by_property(self):
        """Returns a list of of all users that have approved in some form."""
        self.response_gql_user_data()
        test_session = self.base_session()
        match_query = base_mr_mixins.ApprovalsMixin.APPROVALS_QUERY.strip('\n')

        user1 = USERS[1]
        approved_by_list = [{'gid': user1['gid'], 'name': user1['name'],
                             'username': user1['username']}]
        json_response = {'project': {'mr': {'approvedBy': {'nodes': approved_by_list}}}}
        self.responses.post(self.GITLAB_GRAPHQL, json={'data': json_response},
                            match=[json_params_matcher({'query': match_query,
                                                        'variables': {'namespace': NAMESPACE,
                                                                      'mr_id': str(MR_ID)}},
                                                       strict_match=False)])

        basic_approvals = self.BasicApprovals(session=test_session, url=MR_URL)
        approver = User(username=user1['username'], name=user1['name'], gid=user1['gid'])
        self.assertEqual({approver}, basic_approvals.approved_by)


class TestPipelinesMixin(KwfTestCase):
    """Tests for the PipelinessMixin."""

    @dataclass(repr=False)
    class BasicPipelines(base_mr_mixins.PipelinesMixin, BaseMR):
        """A dummy class for testing the PipelinesMixin."""

    @responses.activate
    def test_pipelines(self):
        """Returns a dict of the expected ApprovalRule objects."""
        test_session = self.base_session()
        test_session.get_graphql = mock.Mock(return_value=GitlabGraph())
        match_query = base_mr_mixins.PipelinesMixin.PIPELINES_QUERY.strip('\n')

        ds_pipe = {'id': 'gid://Gitlab/pipeline/456', 'status': 'SUCCESS'}
        job_nodes = [{'id': 123, 'name': 'c9s_merge_request', 'downstreamPipeline': ds_pipe}]
        json_response = {'project': {'mr': {'headPipeline': {'jobs': {'nodes': job_nodes}}}}}
        rsp = responses.post(API_URL, json={'data': json_response},
                             match=[json_params_matcher({'query': match_query,
                                                         'variables': {'namespace': NAMESPACE,
                                                                       'mr_id': str(MR_ID)}},
                                                        strict_match=False)])

        basic_pipelines = self.BasicPipelines(session=test_session, url=MR_URL)
        found_pipes1 = basic_pipelines.pipelines
        self.assertEqual(len(found_pipes1), 1)
        self.assertTrue(isinstance(found_pipes1[0], PipelineResult))
        self.assertEqual(rsp.call_count, 1)
        # Confirm the result is cached.
        found_pipes2 = basic_pipelines.pipelines
        self.assertEqual(rsp.call_count, 1)
        self.assertIs(found_pipes1, found_pipes2)
        # Confirm fresh_pipelines is the same but not the cached value.
        found_pipes3 = basic_pipelines.fresh_pipelines
        self.assertEqual(rsp.call_count, 2)
        self.assertIsNot(found_pipes3, found_pipes2)
        self.assertEqual(found_pipes3, found_pipes2)
        self.assertEqual(len(found_pipes3), 1)
        self.assertTrue(isinstance(found_pipes3[0], PipelineResult))


class TestOwnersMixin(KwfTestCase):
    """Tests for the OwnersMixin."""

    RH_YAML = (" - subsystem: RH Subsystem\n"
               "   labels:\n"
               "     name: redhat\n"
               "     readyForMergeDeps:\n"
               "       - testDep\n"
               "   status: Maintained\n"
               "   requiredApproval: false\n"
               "   testVariants:\n"
               "     - kernel-foobar\n"
               "   maintainers:\n"
               "     - name: User 10\n"
               "       email: user10@redhat.com\n"
               "     - name: User 20\n"
               "       email: user20@redhat.com\n"
               "   reviewers:\n"
               "     - name: User 30\n"
               "       email: user30@redhat.com\n"
               "       restricted: true\n"
               "     - name: User 40\n"
               "       email: user40@redhat.com\n"
               "   paths:\n"
               "       includes:\n"
               "          - makefile\n"
               "          - redhat/\n")

    @dataclass(repr=False)
    class BasicOwners(base_mr_mixins.OwnersMixin, BaseMR):
        """A dummy class for testing the OwnerssMixin."""

    @mock.patch.object(base_mr_mixins.OwnersMixin, 'kconfig_manager',
                       new_callable=mock.PropertyMock)
    @mock.patch('webhook.session.BaseSession.owners', new_callable=mock.PropertyMock)
    def test_owners_mixin_unmerged(self, mock_owners, mock_kconfig_manager):
        """Provides expected values for the given files list."""
        parser = Parser(yaml.load(
            contents=OWNERS_HEADER + self.RH_YAML + OWNERS_ENTRY_1 + OWNERS_ENTRY_2))

        mock_owners.return_value = parser
        test_session = self.base_session()
        test_session.get_graphql = mock.Mock()

        # This unittest passes in an empty `source_path` string so the existing matching of CONFIG
        # names to Kconfig path locations silently did nothing. Here I make the patched out
        # kconfig_manager do the same thing :shrug:.
        mock_kconfig_manager().return_value = []

        files = ['redhat/Makefile', 'redhat/configs/rhel/generic/CONFIG_ITEM']
        basic_owners = self.BasicOwners(
            session=test_session,
            url=GitlabURL(fake_payloads.MR_URL),
            files=files,
            source_path='',
            merge_subsystems=False
        )
        self.assertEqual(basic_owners.all_files, set(basic_owners.files))
        self.assertEqual(basic_owners.config_items, ['CONFIG_ITEM'])
        self.assertIs(basic_owners.merge_subsystems, False)
        self.assertEqual(basic_owners.session.owners, mock_owners.return_value)
        self.assertEqual(len(basic_owners.owners_subsystems), 2)
        self.assertEqual(basic_owners.test_variants, ['kernel-foobar'])

    @mock.patch.object(base_mr_mixins.OwnersMixin, 'kconfig_manager',
                       new_callable=mock.PropertyMock)
    @mock.patch('webhook.session.BaseSession.owners', new_callable=mock.PropertyMock)
    def test_owners_mixin_merged(self, mock_owners, mock_kconfig_manager):
        """Provides expected values for the given files list."""
        parser = Parser(
            yaml.load(contents=OWNERS_HEADER + self.RH_YAML + OWNERS_ENTRY_1 + OWNERS_ENTRY_2)
        )
        # delete the paths excludes so we match config items
        del parser.subsystems[2]._data['paths']['excludes']

        mock_owners.return_value = parser
        test_session = self.base_session()
        test_session.get_graphql = mock.Mock()

        # This unittest passes in an empty `source_path` string so the existing matching of CONFIG
        # names to Kconfig path locations silently did nothing. Here I make the patched out
        # kconfig_manager do the same thing :shrug:.
        mock_kconfig_manager().return_value = []

        files = ['redhat/Makefile', 'redhat/configs/rhel/generic/CONFIG_ITEM']
        basic_owners = self.BasicOwners(
            session=test_session,
            url=GitlabURL(fake_payloads.MR_URL),
            files=files,
            source_path='',
            merge_subsystems=True
        )
        self.assertEqual(basic_owners.all_files, set(basic_owners.files))
        self.assertEqual(basic_owners.config_items, ['CONFIG_ITEM'])
        self.assertIs(basic_owners.merge_subsystems, True)
        self.assertEqual(basic_owners.session.owners, mock_owners.return_value)
        self.assertEqual(len(basic_owners.owners_subsystems), 1)
        self.assertEqual(basic_owners.test_variants, ['kernel-foobar'])
        entry = basic_owners.owners_subsystems[0]
        self.assertEqual(len(entry.maintainers), 4)
        self.assertEqual(len(entry.reviewers), 5)
        self.assertIs(entry.required_approvals, True)


class TestDiscussionsMixin(KwfTestCase):
    """Tests for the Discussions Mixin."""

    EMPTY_NOTE = {'id': '',
                  'author': {},
                  'body': '',
                  'system': False,
                  'updatedAt': ''}

    USERNAMES = ['steve', 'vicky', 'bill', 'linus', 'mark', 'karl', 'fran']

    @dataclass(repr=False)
    class BasicDisc(base_mr_mixins.DiscussionsMixin, BaseMR):
        """A dummy class for testing the DiscussionsMixin."""

    def users(self, usernames=None):
        """Return a dict of users."""
        if not usernames:
            usernames = self.USERNAMES
        return [{'username': user,
                 'name': user.capitalize(),
                 'gid': f'gid://gitlab/User/{1 + usernames.index(user)}',
                 'email': f'{user}@example.com'} for user in usernames]

    @staticmethod
    def random_string(length=10):
        """Returns a random string of the given length."""
        return ''.join(random.choice(ascii_letters) for i in range(length))

    def new_notes(self, count=None):
        """Return a list of note dicts with random values."""
        if not count:
            count = random.randint(1, 15)
        # 2016-01-27T12:00:00 🤷
        last_note_time = datetime.fromtimestamp(1453892400)
        now_time = datetime.now()
        users = self.users()
        new_notes = []
        while count:
            count -= 1
            new_note = self.EMPTY_NOTE.copy()
            new_note['id'] = f'gid://gitlab/Note/{random.randint(1, 1000000)}'
            new_note['author'] = random.choice(users)
            new_note['body'] = self.random_string()
            last_note_time = datetime.fromtimestamp(
                random.randint(int(last_note_time.timestamp()),
                               int(now_time.timestamp()))
            )
            new_note['updatedAt'] = f'{last_note_time.isoformat()}Z'
            new_notes.append(new_note)
        return new_notes

    def new_discussions(self, count=None):
        """Return a list of discussion dicts with random values."""
        if not count:
            count = random.randint(1, 50)
        new_discs = []
        while count:
            count -= 1
            resolvable = random.choice([True, False])
            resolved = random.choice([True, False]) if resolvable else False
            new_disc = {'resolvable': resolvable,
                        'resolved': resolved,
                        'notes': {'nodes': self.new_notes()}}
            new_discs.append(new_disc)
        return new_discs

    @staticmethod
    def disc_asdict(disc_obj):
        """Return the given Discussion object as a dict."""
        disc_dict = asdict(disc_obj)
        fixed_notes = []
        for note in disc_dict['notes']:
            note['author']['email'] = note['author'].pop('emails')[0]
            fixed_notes.append(note)
        disc_dict['notes'] = {'nodes': fixed_notes}
        return disc_dict

    @responses.activate
    def test_discussions_loading(self):
        """Loads all the discussions."""
        match_query = base_mr_mixins.DiscussionsMixin.DISCUSSIONS_QUERY.strip('\n')

        test_data = [self.new_discussions() for _ in range(0, 50)]
        for count, test in enumerate(test_data):
            with self.subTest(test_no=count, data=test):
                responses.reset()
                test_session = BaseSession.new()
                test_session.get_graphql = mock.Mock(return_value=GitlabGraph())

                page_info = {'hasNextPage': False, 'endCursor': 123456}
                discs = self.new_discussions()
                json_response = {'project': {'mr': {'discussions': {'pageInfo': page_info,
                                                                    'nodes': discs}
                                                    }}}
                variables = {'namespace': NAMESPACE, 'mr_id': str(MR_ID)}
                responses.post(API_URL, json={'data': json_response},
                               match=[json_params_matcher({'query': match_query,
                                                           'variables': variables},
                                                          strict_match=False)])

                basic_disc = self.BasicDisc(session=test_session, url=MR_URL)
                # Make sure the overall number of discussions matches the input.
                discussions = basic_disc.discussions
                self.assertEqual(len(discussions), len(discs))
                # Count the number of discussions per user and make sure those counts
                # match the number of discussions returned by the discussions_by_user method.
                user_discs_count = {user: len([disc for disc in discs if
                                    disc['notes']['nodes'][0]['author']['username'] == user]) for
                                    user in self.USERNAMES}
                for user, user_count in user_discs_count.items():
                    self.assertEqual(len(basic_disc.discussions_by_user(user)), user_count)
                # Count the number of system and user discussions in the input and make sure
                # the count matches the number of discussions returned by the
                # systems_discussions and user_discussions methods.
                sys_count = len([d for d in discs if d['notes']['nodes'][0]['system']])
                user_count = len([d for d in discs if not d['notes']['nodes'][0]['system']])
                self.assertEqual(len(basic_disc.system_discussions), sys_count)
                self.assertEqual(len(basic_disc.user_discussions), user_count)
                # For each discussion, finds the matching discussion.
                for disc in discs:
                    raw_note = disc['notes']['nodes'][0]
                    disc_obj = basic_disc.matching_discussion(raw_note['author']['username'],
                                                              raw_note['body'])
                    # Transform the matching object back to a dict and confirm it matches the
                    # query input.
                    disc_dict = self.disc_asdict(disc_obj)
                    self.assertEqual(disc, disc_dict)
                # Returns None when there is no matching discussion.
                self.assertIs(basic_disc.matching_discussion('marco', 'polo'), None)

    @responses.activate
    def _test_matching_discussions_loop(self):
        """Fetches a small set of discussions at a time and returns the first match."""
        match_query = base_mr_mixins.DiscussionsMixin.DISCUSSIONS_QUERY.strip('\n')
        per_page_limit = base_mr_mixins.DiscussionsMixin.QUERY_LIMIT

        # Build a list of pageInfo dicts, ensuring the last has hasNextPage = False.
        page_infos = [{'hasNextPage': True, 'endCursor': self.random_string(6)} for
                      _ in range(random.randint(3, 10))]
        page_infos[-1]['hasNextPage'] = False
        print(f'Set up {len(page_infos)} pageInfos')

        # For each pageInfo create discussions and register a response.
        raw_discs = []
        resps = []
        for index, page_info in enumerate(page_infos):
            discs = self.new_discussions(per_page_limit)
            json_response = {'project': {'mr': {'discussions': {'pageInfo': page_info,
                                                                'nodes': discs}
                                                }}}
            after = '' if index == 0 else page_infos[index-1]['endCursor']
            variables = {'namespace': NAMESPACE, 'mr_id': str(MR_ID), 'after': after,
                         'limit': per_page_limit}
            resp = responses.post(API_URL, json={'data': json_response},
                                  match=[json_params_matcher({'query': match_query,
                                                              'variables': variables},
                                                             strict_match=False)])
            raw_discs.append(discs)
            resps.append(resp)

        test_session = self.base_session()
        test_session.get_graphql = mock.Mock(return_value=GitlabGraph())
        basic_disc = self.BasicDisc(session=test_session, url=MR_URL)
        # For each of raw_discs lists of discussions, match each discussion using the expected
        # number of queries.
        for count, discussions in enumerate(raw_discs, start=1):
            # Confirm we find each discussion.
            for raw_disc in discussions:
                raw_note = raw_disc['notes']['nodes'][0]
                disc_obj = basic_disc.matching_discussion(raw_note['author']['username'],
                                                          raw_note['body'])
                disc_dict = self.disc_asdict(disc_obj)
                self.assertEqual(raw_disc, disc_dict)
            responses.assert_call_count(API_URL, count)

        # Confirm we have all the discussions cached.
        self.assertEqual(len(basic_disc._discussions),
                         len([disc for disc_list in raw_discs for disc in disc_list]))
        # Confirm the discussions property uses the cache.
        responses.stop()
        all_discussions = basic_disc.discussions
        self.assertEqual(all_discussions, basic_disc._discussions)

    def test_matching_discussions_loop(self):
        """Fetches a small set of discussions at a time and returns the first match."""
        for _ in range(100):
            self._test_matching_discussions_loop()


class TestDiffsMixin(KwfTestCase):
    """Tests for the DiffsMixin."""

    diff1 = {'base_commit_sha': '107f5d1cd4a2dd469c812f980a8ab115968d2c94',
             'created_at': '2023-08-03T07:53:24.944Z',
             'head_commit_sha': '54c93f440326f327957099b2ddb85e92c3a3910a',
             'id': 751599416,
             'merge_request_id': 240136232,
             'real_size': '4',
             'start_commit_sha': '044152cd38e9be1d36e9cb391a02a82b71540ddc',
             'state': 'collected'}

    diff1_full = {'base_commit_sha': '107f5d1cd4a2dd469c812f980a8ab115968d2c94',
                  'commits': [{'author_email': 'kernel-team@fedoraproject.org',
                               'author_name': 'Fedora Kernel Team',
                               'authored_date': '2023-07-30T10:18:15.000Z',
                               'committed_date': '2023-08-03T07:52:16.000Z',
                               'committer_email': 'ptalbert@redhat.com',
                               'committer_name': 'Patrick Talbert',
                               'created_at': '2023-08-03T07:52:16.000Z',
                               'id': '54c93f440326f327957099b2ddb85e92c3a3910a',
                               'message': '[redhat] New configs in fs/autofs\n'
                                          'Signed-off-by: Patrick Talbert '
                                          '<ptalbert@redhat.com>\n',
                               'parent_ids': [],
                               'short_id': '54c93f44',
                               'title': '[redhat] New configs in fs/autofs',
                               'trailers': {},
                               'web_url': 'https://gitlab.com/cki-project/kernel-ark/-/commit/'
                                          '54c93f440326f327957099b2ddb85e92c3a3910a'}],
                  'created_at': '2023-08-03T07:53:24.944Z',
                  'diffs': [{'a_mode': '100644',
                             'b_mode': '0',
                             'deleted_file': True,
                             'diff': '@@ -1 +0,0 @@\n-CONFIG_AUTOFS4_FS=y\n',
                             'new_file': False,
                             'new_path': 'redhat/configs/common/generic/CONFIG_AUTOFS4_FS',
                             'old_path': 'redhat/configs/common/generic/CONFIG_AUTOFS4_FS',
                             'renamed_file': False},
                            {'a_mode': '100644',
                             'b_mode': '100644',
                             'deleted_file': False,
                             'diff': '',
                             'new_file': False,
                             'new_path': 'redhat/configs/common/generic/CONFIG_AUTOFS_FS',
                             'old_path': 'redhat/configs/fedora/generic/CONFIG_AUTOFS_FS',
                             'renamed_file': True},
                            {'a_mode': '100644',
                             'b_mode': '0',
                             'deleted_file': True,
                             'diff': '@@ -1,11 +0,0 @@\n'
                                     '-# Symbol: AUTOFS_FS [=n]\n'
                                     '-# Type  : tristate\n'
                                     '-# Defined at fs/autofs/Kconfig:2\n'
                                     '-#   Prompt: Kernel automounter support (supports v3, v4 '
                                     'and v5)\n'
                                     '-#   Location:\n'
                                     '-#     -> File systems\n'
                                     '-#       -> Kernel automounter support (supports v3, v4 '
                                     'and v5) (AUTOFS_FS [=n])\n'
                                     '-# \n'
                                     '-# \n'
                                     '-# \n'
                                     '-# CONFIG_AUTOFS_FS is not set\n',
                             'new_file': False,
                             'new_path': 'redhat/configs/pending-rhel/generic/CONFIG_AUTOFS_FS',
                             'old_path': 'redhat/configs/pending-rhel/generic/CONFIG_AUTOFS_FS',
                             'renamed_file': False},
                            {'a_mode': '100644',
                             'b_mode': '0',
                             'deleted_file': True,
                             'diff': '@@ -1 +0,0 @@\n-# CONFIG_AUTOFS4_FS is not set\n',
                             'new_file': False,
                             'new_path': 'redhat/configs/rhel/generic/s390x/CONFIG_AUTOFS4_FS',
                             'old_path': 'redhat/configs/rhel/generic/s390x/CONFIG_AUTOFS4_FS',
                             'renamed_file': False}],
                  'head_commit_sha': '54c93f440326f327957099b2ddb85e92c3a3910a',
                  'id': 751599416,
                  'merge_request_id': 240136232,
                  'real_size': '4',
                  'start_commit_sha': '044152cd38e9be1d36e9cb391a02a82b71540ddc',
                  'state': 'collected'}

    diff2 = {'base_commit_sha': '107f5d1cd4a2dd469c812f980a8ab115968d2c94',
             'created_at': '2023-07-30T10:18:20.568Z',
             'head_commit_sha': '0a5ba0272662f7e05665b350442a10f47750c199',
             'id': 747797976,
             'merge_request_id': 240136232,
             'real_size': '2',
             'start_commit_sha': '107f5d1cd4a2dd469c812f980a8ab115968d2c94',
             'state': 'collected'}

    diff2_full = {'base_commit_sha': '107f5d1cd4a2dd469c812f980a8ab115968d2c94',
                  'commits': [{'author_email': 'kernel-team@fedoraproject.org',
                               'author_name': 'Fedora Kernel Team',
                               'authored_date': '2023-07-30T10:18:15.000Z',
                               'committed_date': '2023-07-30T10:18:15.000Z',
                               'committer_email': 'kernel-team@fedoraproject.org',
                               'committer_name': 'Fedora Kernel Team',
                               'created_at': '2023-07-30T10:18:15.000Z',
                               'id': '0a5ba0272662f7e05665b350442a10f47750c199',
                               'message': '[redhat] New configs in fs/autofs\n'
                                          'Signed-off-by: Fedora Kernel Team '
                                          '<kernel-team@fedoraproject.org>\n',
                               'parent_ids': [],
                               'short_id': '0a5ba027',
                               'title': '[redhat] New configs in fs/autofs',
                               'trailers': {},
                               'web_url': 'https://gitlab.com/cki-project/kernel-ark/-/commit/'
                                          '0a5ba0272662f7e05665b350442a10f47750c199'}],
                  'created_at': '2023-07-30T10:18:20.568Z',
                  'diffs': [],
                  'head_commit_sha': '0a5ba0272662f7e05665b350442a10f47750c199',
                  'id': 747797976,
                  'merge_request_id': 240136232,
                  'real_size': '2',
                  'start_commit_sha': '107f5d1cd4a2dd469c812f980a8ab115968d2c94',
                  'state': 'without_files'}

    @dataclass(repr=False)
    class BasicDiffs(base_mr_mixins.DiffsMixin):
        """A dummy class for testing the DiffsMixin."""

        def __post_init__(self):
            """"Set up needed attributes."""
            self.gl_project = mock.Mock()
            self.gl_mr = mock.Mock()
            self._description = base_mr_mixins.MRDescription(text='')

    @staticmethod
    def make_ProjectMergeRequestDiff(attrs):
        """Return a ProjectMergeRequestDiff."""
        mock_manager = mock.Mock()
        return base_mr_mixins.ProjectMergeRequestDiff(manager=mock_manager, attrs=attrs)

    def test_mrrev(self):
        """Represents a ProjectMergeRequestDiff."""
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(123, 'cki-project/kernel-ark')
        mock_project.repository_compare = mock.Mock()
        mr_desc = base_mr_mixins.MRDescription(text='hi')
        test_diff = self.make_ProjectMergeRequestDiff(self.diff1_full)

        test_mrrev = base_mr_mixins.MRRev(mock_project, mr_desc, test_diff)
        self.assertEqual(test_mrrev.diff, test_diff.diffs)
        self.assertEqual(test_mrrev.diff_without_depends, test_mrrev.diff)
        self.assertEqual(test_mrrev.head_commit_sha, test_diff.head_commit_sha)
        self.assertEqual(test_mrrev.start_commit_sha, test_diff.start_commit_sha)
        self.assertEqual(len(test_mrrev.commits), 1)
        repr_str = str(test_mrrev)
        self.assertIn(test_mrrev.head_commit_sha[:8], repr_str)

    def test_diffs_mixin(self):
        """Fetches and compares diffs."""
        diff1 = self.make_ProjectMergeRequestDiff(self.diff1)
        diff1_full = self.make_ProjectMergeRequestDiff(self.diff1_full)
        diff2 = self.make_ProjectMergeRequestDiff(self.diff2)
        diff2_full = self.make_ProjectMergeRequestDiff(self.diff2_full)

        test_DiffsMixin = self.BasicDiffs()
        # Make gl_mr.diffs.get() return the matching full diff and .list() return a list of small
        # diffs.
        mock_mr_diffs = mock.MagicMock()
        get_dict = {diff1.id: diff1_full, diff2.id: diff2_full}
        mock_mr_diffs.get = get_dict.get
        mock_mr_diffs.list.return_value = [diff2, diff1]
        test_DiffsMixin.gl_mr.diffs = mock_mr_diffs

        self.assertEqual(test_DiffsMixin.diffs, [diff1, diff2])
        self.assertEqual(test_DiffsMixin.full_diffs, [diff1_full, diff2_full])

        mr_revs = test_DiffsMixin.history
        self.assertEqual(len(mr_revs), 2)
        self.assertEqual(mr_revs[0].head_commit_sha, diff1_full.head_commit_sha)
        self.assertEqual(mr_revs[1].head_commit_sha, diff2_full.head_commit_sha)

        self.assertEqual(len(test_DiffsMixin.code_changes()), 42)


env_dict = {
    'CKI_DEPLOYMENT_ENVIRONMENT': 'staging',
    'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'
}


@mock.patch.dict('os.environ', env_dict, clear=True)
class TestKerneloscopeMixin(KwfTestCase):
    """Tests for the KerneloscopeMixin."""

    @dataclass(repr=False)
    class BasicKerneloscope(base_mr_mixins.KerneloscopeMixin, BaseMR):
        """A dummy class for testing the KerneloscopeMixin."""

    def test_fetch_data_method(self):
        """Returns a json response to an rpc query."""
        mock_kmr = mock.Mock()
        mock_kmr.kerneloscope_server_url = "https://kerneloscope.example.com"
        self.responses.post("https://kerneloscope.example.com/rpc/get_foo/",
                            json={'result': [{'name': 'foo', 'type': 'upstream'}]})

        data = self.BasicKerneloscope.fetch_data(mock_kmr, 'get_foo')
        self.assertEqual(data, [{'name': 'foo', 'type': 'upstream'}])

    def test_find_already_backported(self):
        """Returns any commits already in tree for a given upstream commit ID."""
        self.response_gql_user_data()
        namespace = 'redhat/rhel/src/kernel/rhel-9-sandbox'
        mr_id = 66
        mr_data = {'project': {'mr': {'targetBranch': 'main'}}}
        self.responses.post(self.GITLAB_GRAPHQL, json={'data': mr_data})
        test_session = self.base_session()
        kos_url = "https://kerneloscope.example.com"
        mr_url = f"https://gitlab.com/{namespace}/-/merge_requests/{mr_id}"
        bkmr = self.BasicKerneloscope.new(test_session, mr_url, kerneloscope_server_url=kos_url)
        trees = [{'name': 'rhel-9.4',
                  'url': 'https://gitlab.cee.redhat.com/kmaint/mirrors/kernel-rhel9.git#9.4',
                  'type': 'RHEL',
                  'origin': 'v5.14'},
                 {'name': 'rhel-9.5',
                  'url': 'https://gitlab.cee.redhat.com/kmaint/mirrors/kernel-rhel9.git#main',
                  'type': 'RHEL',
                  'origin': 'v5.14'}]
        commits = [{'commit': 'b74fb6596767dd8ce30487a7c81bf6cc672ab4dd',
                    'merge': False,
                    'trees': ['rhel-9.5'],
                    'subject': 'x86: Stop using weak symbols for __iowrite32_copy()'},
                   {'commit': '97d8c8d6063b91d8578aa87dec482d7f28839fd4',
                    'merge': False,
                    'trees': ['rhel-9.5'],
                    'subject': 'x86: Stop using weak symbols for __iowrite32_copy()'}]
        self.responses.post("https://kerneloscope.example.com/rpc/get_trees/",
                            json={'result': trees})
        self.responses.post("https://kerneloscope.example.com/rpc/get_downstream/",
                            json={'result': commits})
        data = bkmr.find_already_backported('abcd1234')
        self.assertEqual(data, commits)

    @mock.patch('webhook.base_mr_mixins.KerneloscopeMixin.kerneloscope_tree',
                new_callable=mock.PropertyMock)
    def test_find_missing_fixes(self, mock_tree):
        """Returns a list of commits that are fixes for the specified commit."""
        mock_tree.return_value = 'doesnotmatterhere'
        self.response_gql_user_data()
        namespace = 'redhat/rhel/src/kernel/rhel-9-sandbox'
        mr_id = 66
        mr_data = {'project': {'mr': {'targetBranch': 'main'}}}
        self.responses.post(self.GITLAB_GRAPHQL, json={'data': mr_data})
        test_session = self.base_session()
        kos_url = "https://kerneloscope.example.com"
        mr_url = f"https://gitlab.com/{namespace}/-/merge_requests/{mr_id}"
        bkmr = self.BasicKerneloscope.new(test_session, mr_url, kerneloscope_server_url=kos_url)
        commits = [{'commit': '31c89007285d365aa36f71d8fb0701581c770a27',
                    'merge': False,
                    'trees': ['linux'],
                    'subject': 'workqueue.c: Increase workqueue name length',
                    'added': False,
                    'kind': 'i',
                    'fixes': ['231035f18d6b80e5c28732a20872398116a54ecd'],
                    'fix_types': ['f']},
                   {'commit': '231035f18d6b80e5c28732a20872398116a54ecd',
                    'merge': False,
                    'trees': ['linux'],
                    'subject': "workqueue: Increase worker desc's length to 32",
                    'added': True,
                    'kind': 'f',
                    'fixes': [],
                    'fix_types': []}]
        self.responses.post("https://kerneloscope.example.com/rpc/get_missing_fixes/",
                            json={'result': commits})
        data = bkmr.find_missing_fixes('abcd1234')
        self.assertEqual(data, ['231035f18d6b80e5c28732a20872398116a54ecd'])


class TestDependsBlocks(KwfTestCase):
    """Test cases for the DependsMixin._set_mr_blocks() method."""

    MR778_BLOCKS = KwfTestCase.load_yaml_asset(
            'mr-778-rhel-8-sandbox-blocks.json',
            sub_module='gitlab_rest_api'
        )

    @dataclass(repr=False)
    class TestBaseWithDepends(base_mr_mixins.DependsMixin, BaseMR):
        """A BaseMR with DependsMixin for testing."""

    def setUp(self) -> None:
        """Set up a test BaseMR with DependsMixin instance."""
        super().setUp()

        test_session = self.session_runner('signoff')
        test_session.get_graphql = mock.Mock()

        test_mr = self.TestBaseWithDepends(
            session=test_session,
            url=GitlabURL(
                'https://gitlab.com/redhat/rhel/src/kernel/rhel-8-sandbox/-/merge_requests/778'
            ),
            project_gid=GitlabGID('gid://gitlab/Project/20363472')
        )

        # Mock out the BaseMR 'blocks' methods.
        test_mr._blocking_data = mock.Mock()
        test_mr.add_blocking_mr = mock.Mock()
        test_mr.remove_blocking_id = mock.Mock()

        self.test_session = test_session
        self.test_mr = test_mr

    def test_set_mr_blocks_no_depends_no_blocks(self) -> None:
        """Does nothing when there are no depends_mrs and no relevant 'blocks'."""
        self.test_mr._blocking_data.return_value = []
        self.test_mr._set_mr_blocks({})

        self.test_mr.add_blocking_mr.assert_not_called()
        self.test_mr.remove_blocking_id.assert_not_called()

    def test_set_mr_blocks_in_sync(self) -> None:
        """Does nothing when the depends_mr_iids matches the 'blocks' (for the same project)."""
        self.test_mr._blocking_data.return_value = self.MR778_BLOCKS.copy()

        self.test_mr._set_mr_blocks({777: GitlabGID('gid://gitlab/MergeRequest/213213')})

        self.test_mr.add_blocking_mr.assert_not_called()
        self.test_mr.remove_blocking_id.assert_not_called()

    def test_set_mr_blocks_to_add(self) -> None:
        """Calls BaseMR.add_blocking_mr() when there is a 'blocks' to add."""
        self.test_mr._blocking_data.return_value = self.MR778_BLOCKS.copy()

        self.test_mr._set_mr_blocks({
            777: GitlabGID('gid://gitlab/MergeRequest/213213'),
            555: GitlabGID('gid://gitlab/MergeRequests/123456')
        })

        self.test_mr.add_blocking_mr.assert_called_once_with(
            GitlabGID('gid://gitlab/MergeRequests/123456')
        )
        self.test_mr.remove_blocking_id.assert_not_called()

    def test_set_mr_blocks_to_remove(self) -> None:
        """Calls BaseMR.remove_blocking_id() when there is a 'blocks' to remove."""
        blocking_data = self.MR778_BLOCKS.copy()
        self.test_mr._blocking_data.return_value = blocking_data

        self.test_mr._set_mr_blocks({})

        self.test_mr.add_blocking_mr.assert_not_called()
        self.assertEqual(1466016, blocking_data[0]['id'])
        self.test_mr.remove_blocking_id.assert_called_once_with(1466016)
