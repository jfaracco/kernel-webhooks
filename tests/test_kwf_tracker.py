"""Tests for the webhook.tracker module."""
import os
from unittest import mock

from cki_jira.common import BugzillaID
from cki_jira.common import CveID
from cki_jira.common import JiraKey
from jira.resources import Issue

from tests.helpers import KwfTestCase
from tests.test_jira import make_remote_link
from webhook import kwf_tracker
from webhook.session import BaseSession


class TestTrackerLoading(KwfTestCase):
    """Tests for the KwfBaseTracker.new() constructor methods."""

    BUGZILLA_PAYLOADS = {
        'bz2170513': kwf_tracker.KwfIssueTracker,
        'bz2218844': kwf_tracker.KwfIssueTracker,
        'cve-2024-40998': kwf_tracker.KwfFlawTracker,
        'cve-2024-41090': kwf_tracker.KwfFlawTracker,
    }

    JIRA_PAYLOADS = {
        'RHEL-101': kwf_tracker.KwfIssueTracker,
        'RHEL-102': kwf_tracker.KwfIssueTracker,
        'RHEL-103': kwf_tracker.KwfIssueTracker,
        'RHEL-104': kwf_tracker.KwfIssueTracker,
        'RHEL-105': kwf_tracker.BasicTracker,
        'RHEL-106': kwf_tracker.KwfIssueTracker,
        'RHEL-107': kwf_tracker.KwfIssueTracker,
        'RHEL-108': kwf_tracker.KwfIssueTracker,
        'RHEL-14726': kwf_tracker.KwfIssueTracker,
        'RHEL-1559': kwf_tracker.KwfIssueTracker,
        'RHEL-26081': kwf_tracker.KwfIssueTracker,
        'RHEL-30580': kwf_tracker.KwfIssueTracker,
        'RHEL-36513': kwf_tracker.KwfIssueTracker,
        'RHEL-39925': kwf_tracker.KwfIssueTracker,
        'RHEL-44863': kwf_tracker.BasicTracker,
        'RHEL-47892': kwf_tracker.KwfIssueTracker,
        'RHEL-47894': kwf_tracker.KwfIssueTracker,
        'RHEL-48706': kwf_tracker.KwfIssueTracker,
        'RHEL-49673': kwf_tracker.KwfIssueTracker,
        'RHEL-49674': kwf_tracker.KwfIssueTracker,
        'RHEL-50200': kwf_tracker.KwfIssueTracker,
        'RHEL-50201': kwf_tracker.KwfIssueTracker,
        'RHEL-50203': kwf_tracker.KwfIssueTracker,
        'RHEL-50263': kwf_tracker.KwfIssueTracker,
        'RHEL-50264': kwf_tracker.KwfIssueTracker,
        'RHEL-50265': kwf_tracker.KwfIssueTracker,
    }

    def setUp(self) -> None:
        """Set up the basic jira responses."""
        super().setUp()

        self.response_bugzilla_setup()
        self.response_jira_setup()

    def test_from_dict(self) -> None:
        """Constructs the expected Kwf class instance from the given input."""
        test_session = self.base_session()

        for bz_id, expected_class in self.BUGZILLA_PAYLOADS.items():
            json_path = f'tests/assets/bugzilla_rest_api/{bz_id}.json'

            with self.subTest(json_path=json_path, expected_class=expected_class):
                test_tracker = kwf_tracker.make_tracker(json_path, test_session)

                self.assertIsInstance(test_tracker, expected_class)

                self.assertTrue(test_tracker.is_bugzilla)
                self.assertFalse(test_tracker.is_jira)

        for key, expected_class in self.JIRA_PAYLOADS.items():
            json_path = f'tests/assets/jira_rest_api/{key}.json'

            with self.subTest(json_path=json_path, expected_class=expected_class):
                test_tracker = kwf_tracker.make_tracker(json_path, test_session)

                self.assertEqual(test_tracker.id, key)
                self.assertIsInstance(test_tracker, expected_class)

                self.assertFalse(test_tracker.is_bugzilla)
                self.assertTrue(test_tracker.is_jira)

    def test_make_tracker_from_bad_id(self) -> None:
        """Raises TypeError when the input is not a recognized type."""
        # The only way this would happen is if someone called it directly with a bad value or
        # TrackerID is expanded to include more types that this function does not handle.
        with self.assertRaises(TypeError):
            assert kwf_tracker._make_tracker_from_id(123, self.base_session())

    def add_bz_response(self, path: str, bug_id: str | int, bug_jsons: list[dict]) -> None:
        """Add a rest/bug response for the given IDs."""
        self.responses.get(
            f'https://bugzilla.redhat.com/rest/{path}',
            json={'bugs': bug_jsons}
        )

    def test_make_tracker_bz_id(self) -> None:
        """Constructs the expected Kwf class instance from the given input."""
        bz2170513_dict = self.load_yaml_asset('bz2170513.json', sub_module='bugzilla_rest_api')
        bz_id = kwf_tracker.BugzillaID(2170513)

        self.add_bz_response(path='bug/2170513', bug_id=bz_id, bug_jsons=[bz2170513_dict])

        self.assertIsInstance(
            kwf_tracker.make_tracker(bz_id, self.base_session()),
            kwf_tracker.KwfIssueTracker
        )

        # Same but the ID is a "raw" int or str.
        self.assertIsInstance(
            kwf_tracker.make_tracker(2170513, self.base_session()),
            kwf_tracker.KwfIssueTracker
        )

    def test_make_tracker_bz_id_unknown(self) -> None:
        """Returns None when the API does not return any data for the given ID."""
        bz_id = kwf_tracker.BugzillaID(99999999)

        self.add_bz_response(path='bug/99999999', bug_id=bz_id, bug_jsons=[{}])

        self.assertIsNone(kwf_tracker.make_tracker(bz_id, self.base_session()))

    def test_make_tracker_jira_key(self) -> None:
        """Constructs the expected Kwf class instance from the given input."""
        rhel_36513_dict = self.load_yaml_asset('RHEL-36513.json', sub_module='jira_rest_api')
        jira_key = 'RHEL-36513'

        self.responses.get(
            'https://issues.redhat.com/rest/api/2/issue/RHEL-36513',
            json=rhel_36513_dict
        )

        self.assertIsInstance(
            kwf_tracker.make_tracker(jira_key, session=self.base_session()),
            kwf_tracker.KwfIssueTracker
        )

    def test_make_tracker_key_unknown(self) -> None:
        """Returns None when the API does not return any data for the given ID."""
        jira_key = kwf_tracker.JiraKey('RHEL-999999')

        self.responses.get(
            'https://issues.redhat.com/rest/api/2/issue/RHEL-999999',
            status=404,
            json={'errorMessages': ['Issue Does Not Exist'], 'errors': {}}
        )

        self.assertIsNone(kwf_tracker.make_tracker(jira_key, session=self.base_session()))

    def test_make_trackers_set_bugzilla(self) -> None:
        """Constructs the list of expected Kwf class instances from the given input bug ID."""
        bz2170513_dict = self.load_yaml_asset('bz2170513.json', sub_module='bugzilla_rest_api')
        bz_id = kwf_tracker.BugzillaID(2170513)

        self.add_bz_response(path='bug/2170513', bug_id=bz_id, bug_jsons=[bz2170513_dict])

        results = kwf_tracker.make_trackers({bz_id}, session=self.base_session())

        self.assertEqual(len(results), 1)

        self.assertCountEqual([bz_id], [result.id for result in results])

    @mock.patch('webhook.kwf_tracker.get_issues')
    def test_make_trackers_set_jira(self, mock_get_issues) -> None:
        """Constructs the list of expected Kwf class instances from the given input jira key."""
        test_session = self.base_session()

        rhel_50264_dict = self.load_yaml_asset('RHEL-50264.json', sub_module='jira_rest_api')
        jira_key = kwf_tracker.JiraKey('RHEL-50264')

        # How the `get_issues` function does its thing isn't relevant here.
        mock_get_issues.return_value = [mock.Mock(raw=rhel_50264_dict, spec=Issue)]

        results = kwf_tracker.make_trackers({jira_key}, test_session)

        self.assertEqual(len(results), 1)

        self.assertCountEqual([jira_key], [result.id for result in results])

        # Make sure get_issues is called with with_linked and filter_kwf set.
        self.assertTrue(
            mock_get_issues.call_args[1]['with_linked'],
        )
        self.assertTrue(
            mock_get_issues.call_args[1]['filter_kwf'],
        )

    def test_make_trackers_bad_input(self) -> None:
        """Does nothing if the input is not a recognized type."""
        self.assertEqual(
            kwf_tracker.make_trackers([None], self.base_session()),
            []
        )

    def test_api_object_no_data(self) -> None:
        """Raises a RuntimeError when _data attribute is empty."""
        test_issue = kwf_tracker.BasicTracker(id=JiraKey('RHEL-123'), session=self.base_session())

        with self.assertRaises(RuntimeError):
            assert test_issue._api_object

    def test_api_object_unknown(self) -> None:
        """Raises a TypeError when the ID is not of the expected type."""
        test_issue = kwf_tracker.make_tracker({'id': 123456}, session=self.base_session())

        # Make the 'id' attribute into something unexpected.
        test_issue.id = 'weird id'

        with self.assertRaises(TypeError):
            assert test_issue._api_object

        test_issue.id = 123456

        with self.assertRaises(TypeError):
            assert test_issue._api_object

    def test_remote_links_non_jira(self) -> None:
        """Returns self._remote_links."""
        bz2218844_dict = self.load_yaml_asset('bz2218844.json', sub_module='bugzilla_rest_api')

        tracker_bz = kwf_tracker.make_tracker(bz2218844_dict, session=self.base_session())

        self.assertIs(tracker_bz.remote_links, tracker_bz._remote_links)
        self.assertCountEqual(
            tracker_bz.remote_links,
            ['https://issues.redhat.com/browse/RHELPLAN-161304',
             'https://access.redhat.com/errata/RHSA-2023:6583',
             'https://gitlab.com/redhat/rhel/src/kernel/rhel-9/-/merge_requests/909']
        )

    def test_remote_links_jira(self) -> None:
        """Fetches the remote links from the API, once."""
        mock_remote_link = self.responses.get(
            'https://issues.redhat.com/rest/api/2/issue/RHEL-101/remotelink',
            json=self.load_yaml_asset('RHEL-101_remotelink.json', sub_module='jira_rest_api')
        )

        rhel_101_path = 'tests/assets/jira_rest_api/RHEL-101.json'
        test_issue = kwf_tracker.make_tracker(rhel_101_path, session=self.base_session())

        self.assertIsNone(test_issue._remote_links)

        expected_links = [
            'https://gitlab.com/redhat/rhel/src/kernel/rhel-8-sandbox/-/merge_requests/752',
            'https://gitlab.com/redhat/rhel/src/kernel/rhel-8-sandbox/-/merge_requests/813'
        ]

        self.assertEqual(test_issue.remote_links, expected_links)
        self.assertEqual(test_issue._remote_links, expected_links)

        # The response is cached so it should only ever be called once per instance.
        self.assertEqual(test_issue.remote_links, expected_links)
        self.assertEqual(mock_remote_link.call_count, 1)


def create_test_cache(
    session: 'BaseSession',
    jira_files: list[str] | None = None,
    bz_files: list[str] | None = None
) -> kwf_tracker.TrackerCache:
    """Return a new TrackerCache with the given test assets loaded into the cache."""
    if not jira_files:
        jira_files = ['RHEL-1559', 'RHEL-106', 'RHEL-49673', 'RHEL-50203', 'RHEL-47894',
                      'RHEL-107', 'RHEL-50263', 'RHEL-36513', 'RHEL-14726', 'RHEL-50264',
                      'RHEL-48706', 'RHEL-50265', 'RHEL-47892', 'RHEL-49674', 'RHEL-101',
                      'RHEL-102', 'RHEL-30580', 'RHEL-44863', 'RHEL-103', 'RHEL-108',
                      'RHEL-50201', 'RHEL-104', 'RHEL-105', 'RHEL-39925', 'RHEL-26081',
                      'RHEL-50200']

    if not bz_files:
        bz_files = ['cve-2024-41090']

    input_dict = {}

    for key in jira_files:
        asset_path = f'tests/assets/jira_rest_api/{key}.json'
        issue = kwf_tracker.make_tracker(asset_path, session=session)
        input_dict[issue.id] = issue

    for key in bz_files:
        asset_path = f'tests/assets/bugzilla_rest_api/{key}.json'
        bug = kwf_tracker.make_tracker(asset_path, session=session)
        input_dict[bug.id] = bug

    return kwf_tracker.TrackerCache(input_dict, session=session)


class TestKwfFlawTracker(KwfTestCase):
    """Tests for the KwfFlawTracker class."""

    def test_flaw_trackers_cve_2024_41090(self) -> None:
        """Returns the dict of KwfIssueTrackers associated with this CVE."""
        # We need a TrackerCache for this test.
        test_cache = create_test_cache(self.base_session())

        test_cve = test_cache['CVE-2024-41090']

        # These are the issues in the cache with a label 'CVE-2024-41090'.
        expected_trackers = ['RHEL-50263', 'RHEL-50264', 'RHEL-50265']

        self.assertCountEqual(expected_trackers, test_cve.trackers.keys())

    def test_flaw_trackers_no_cache_raises(self):
        """Raises a RuntimeError exception if the ._cache attribute is not set."""
        cve_2024_40998_dict = \
            self.load_yaml_asset('cve-2024-40998.json', sub_module='bugzilla_rest_api')

        test_cve = kwf_tracker.make_tracker(cve_2024_40998_dict, session=self.base_session())

        with self.assertRaises(RuntimeError):
            test_cve.trackers


class TestKwfIssueTracker(KwfTestCase):
    """Tests for the KwfIssueTracker class."""

    def setUp(self) -> None:
        """Create a TrackerCache."""
        # This file is quite outdated now and for some reason is missing all the non-sandbox rhel-9
        # project data. A lot of the test asset jira jsons have rhel-9 FixVersions so unfortunately
        # the KwfIssueTracker init will not find a project/branch for many of them.
        env_dict = {
            'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'
        }
        with mock.patch.dict(os.environ, env_dict, clear=True):
            test_session = self.base_session()

        self.test_session = test_session
        self.test_cache = create_test_cache(test_session)

    def test_rh_project_branch_attributes(self) -> None:
        """Sets the expected rh_project & rh_branch attribute values on creation."""
        # RHEL-106 has a fix version of 'rhel-8.7.0' which is the 'main' branch of our old
        # rh_projects yaml test asset.
        rhel_106 = self.test_cache['RHEL-106']
        self.assertEqual(rhel_106.fix_version, 'rhel-8.7.0')
        self.assertEqual(rhel_106.rh_project.name, 'rhel-8')
        self.assertEqual(rhel_106.rh_branch.name, 'main')

    def test_cves(self) -> None:
        """Returns a dict with the related KwfFlawTrackers."""
        rhel_50263 = self.test_cache.trackers['RHEL-50263']
        cve_2024_41090 = self.test_cache.cves['CVE-2024-41090']

        expected_cves = {cve_2024_41090.cve_id: cve_2024_41090}
        self.assertEqual(expected_cves, rhel_50263.cves)

    def test_testing_tasks(self) -> None:
        """Returns the list of testing tasks associated with the issue."""
        rhel_14726 = self.test_cache.trackers['RHEL-14726']

        expected_testing_tasks = [
            self.test_cache.trackers[task_id] for task_id in ['RHEL-36513', 'RHEL-49673']
        ]

        self.assertCountEqual(expected_testing_tasks, rhel_14726.testing_tasks)

    def test_variant_trackers(self) -> None:
        """Returns the list of variant trackers associated with the issue."""
        rhel_14726 = self.test_cache.trackers['RHEL-14726']

        expected_variant_trackers = [
            self.test_cache.trackers[tracker_id] for tracker_id in ['RHEL-36513', 'RHEL-49673']
        ]

        self.assertCountEqual(expected_variant_trackers, rhel_14726.variant_trackers)

    def test_ystream_tracker(self) -> None:
        """Returns the ystream tracker associated with the issue."""
        # RHEL-14726 does not have a ystream tracker since it is some goofy test issue.
        rhel_14726 = self.test_cache.trackers['RHEL-14726']
        self.assertIsNone(rhel_14726.ystream_tracker)

        # RHEL-47894 ystream is RHEL-47892.
        rhel_47894 = self.test_cache.trackers.get('RHEL-47894')
        rhel_47892 = self.test_cache.trackers.get('RHEL-47892')
        self.assertEqual(rhel_47894.ystream_tracker, rhel_47892)
        self.assertIn(rhel_47894, rhel_47892.zstream_trackers)

    def test_zstream_trackers(self) -> None:
        """Returns the list of zstream trackers associated with the issue."""
        rhel_50203 = self.test_cache.trackers['RHEL-50203']

        expected_zstream_trackers = [
            self.test_cache.trackers[tracker_id] for tracker_id in ['RHEL-50200', 'RHEL-50201']
        ]

        self.assertCountEqual(expected_zstream_trackers, rhel_50203.zstream_trackers)


REMOTE_LINKS = [
    {'application': {},
     'id': 654373,
     'object': {'icon': {'title': 'Gitlab Merge Request',
                         'url16x16': ('https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea'
                                      '56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
                         },
                'status': {'icon': {}},
                'title': 'Merge Request group/project!778: beep boop I am an MR',
                'url': 'https://gitlab.com/group/project/-/merge_requests/778'
                },
     'self': 'https://issues.redhat.com/rest/api/2/issue/RHEL-555/remotelink/654373'
     },
    {'application': {},
     'globalId': 'kwf-mr-link:https://gitlab.com/group/project/-/merge_requests/778',
     'id': 1845188,
     'object': {'icon': {'title': 'Gitlab Merge Request',
                         'url16x16': ('https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea'
                                      '56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
                         },
                'status': {'icon': {}},
                'title': 'Merge Request group/project!778: '
                         'kernel: add a file that indicates when you have been hacked',
                'url': 'https://gitlab.com/group/project/-/merge_requests/778'
                },
     'self': 'https://issues.redhat.com/rest/api/2/issue/RHEL-555/remotelink/1845188'
     },
    {'application': {},
     'globalId': 'kwf-mr-link:https://gitlab.com/group/project/-/merge_requests/123',
     'id': 2896327,
     'object': {'icon': {'title': 'Gitlab Merge Request',
                         'url16x16': ('https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea'
                                      '56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
                         },
                'status': {'icon': {}},
                'title': 'Merge Request group/project!123: my cool MR',
                'url': 'https://gitlab.com/group/project/-/merge_requests/123'
                },
     'self': 'https://issues.redhat.com/rest/api/2/issue/RHEL-555/remotelink/2896327'
     },
]


class TestKwfIssueTrackerJiraActions(KwfTestCase):
    """Tests for the KwfIssuetracker jira commenting & linking bits."""

    EXTRA_DESTINATION = {
        'icon': {'title': 'Gitlab Merge Request',
                 'url16x16': ('https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea'
                              '56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
                 }
    }

    def setUp(self) -> None:
        """Patch out the BaseSession.jira."""
        super().setUp()

        test_session = self.base_session()
        test_session.jira = mock.Mock()

        self.mock_comments = test_session.jira.comments
        self.mock_pinned_comments = test_session.jira.pinned_comments
        self.mock_add_comment = test_session.jira.add_comment
        self.mock_pin_comment = test_session.jira.pin_comment

        self.mock_add_remote_link = test_session.jira.add_remote_link

        self.bz_tracker = kwf_tracker.KwfIssueTracker(session=test_session, id=BugzillaID(12345))
        self.jira_tracker = \
            kwf_tracker.KwfIssueTracker(session=test_session, id=JiraKey('RHEL-123'))

        # Create some RemoteLinks and blot out their delete/update methods.
        self.link1 = make_remote_link(REMOTE_LINKS[0])
        self.link2 = make_remote_link(REMOTE_LINKS[1])
        self.link3 = make_remote_link(REMOTE_LINKS[2])

        remote_links = [self.link1, self.link2, self.link3]

        for link in remote_links:
            link.delete = mock.Mock()
            link.update = mock.Mock()

        # Attach the RemoteLinks to the KwfIssueTracker.
        self.jira_tracker._remote_links = [link.object.url for link in remote_links]
        self.jira_tracker._jira_remote_links = remote_links

    def test_comments_non_jira(self) -> None:
        """Trying to call comments() on a non-jira KwfIssueTracker raises an exception."""
        with self.assertRaises(NotImplementedError):
            self.bz_tracker.comments()

        self.mock_comments.assert_not_called()

    def test_comments_jira(self) -> None:
        """Returns JIRA.comments() called with the expected ID."""
        results = self.jira_tracker.comments()

        self.assertIs(results, self.mock_comments.return_value)
        self.mock_comments.assert_called_once_with('RHEL-123')

    def test_pinned_comments_non_jira(self) -> None:
        """Trying to call pinned_comments() on a non-jira KwfIssueTracker raises an exception."""
        with self.assertRaises(NotImplementedError):
            self.bz_tracker.pinned_comments()

        self.mock_pinned_comments.assert_not_called()

    def test_pinned_comments_jira(self) -> None:
        """Returns JIRA.pinned_comments() called with the expected ID."""
        results = self.jira_tracker.pinned_comments()

        self.assertIs(results, self.mock_pinned_comments.return_value)
        self.mock_pinned_comments.assert_called_once_with('RHEL-123')

    def test_post_comment_non_jira(self) -> None:
        """Trying to call post_comment() on a non-jira KwfIssueTracker raises an exception."""
        with self.assertRaises(NotImplementedError):
            self.bz_tracker.post_comment('hello')

        self.mock_add_comment.assert_not_called()
        self.mock_pin_comment.assert_not_called()

    def test_post_comment_jira_no_text(self) -> None:
        """Raises an exception if there is no comment_text."""
        with self.assertRaises(ValueError):
            self.jira_tracker.post_comment('')

        self.mock_add_comment.assert_not_called()
        self.mock_pin_comment.assert_not_called()

    def test_post_comment_jira_non_prod(self) -> None:
        """When not production/staging it just logs about it."""
        self.jira_tracker.session.is_production_or_staging = False

        with self.assertLogs('cki.webhook.tracker', 'INFO'):
            self.jira_tracker.post_comment('this is a faux comment')

        self.mock_add_comment.assert_not_called()
        self.mock_pin_comment.assert_not_called()

    def test_post_comment_jira_prod(self) -> None:
        """When in production/staging actually calls JIRA.add_comment() & JIRA.pin_comment()."""
        self.jira_tracker.session.is_production_or_staging = True

        self.jira_tracker.post_comment('this is a cool comment', pinned=True)

        self.mock_add_comment.assert_called_once_with('RHEL-123', 'this is a cool comment')
        self.mock_pin_comment.assert_called_once_with(
            'RHEL-123', self.mock_add_comment.return_value.id, True
        )

    def test_remove_remote_link_non_jira(self) -> None:
        """Trying to call remove_remote_link() on a non-jira KwfIssueTracker raises an exception."""
        with self.assertRaises(NotImplementedError):
            self.bz_tracker.remove_remote_link(
                'https://gitlab.com/group/project/-/merge_requests/123'
            )

        for link in self.jira_tracker._jira_remote_links:
            link.delete.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_remove_remote_link_jira_no_existing(self) -> None:
        """Does nothing and returns False when the URL does not seem to be linked."""
        self.jira_tracker._remote_links = []

        with self.assertLogs('cki.webhook.tracker', 'INFO') as logs:
            result = self.jira_tracker.remove_remote_link(
                'https://gitlab.com/group/project/-/merge_requests/999'
            )

            self.assertTrue(any('No existing' in record.message for record in logs.records))

        self.assertIs(result, False)

        for link in self.jira_tracker._jira_remote_links:
            link.delete.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=False))
    def test_remove_remote_link_jira_non_prod(self) -> None:
        """Just logs that it is removing links but doesn't actually do anything."""
        with self.assertLogs('cki.webhook.jira', 'INFO') as logs:
            result = self.jira_tracker.remove_remote_link(
                'https://gitlab.com/group/project/-/merge_requests/778'
            )

            # It should have pretended to delete two RemoteLinks.
            records = [record for record in logs.records if
                       'Pretending to delete resource' in record.message]
            self.assertEqual(len(records), 2)

            # These are the internal IDs of the links.
            self.assertIn('id=654373', records[0].message)
            self.assertIn('id=1845188', records[1].message)

        self.assertIs(result, True)

        for link in self.jira_tracker._jira_remote_links:
            link.delete.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_remove_remote_link_jira_prod(self) -> None:
        """Calls JIRA.delete_remote_link() on the matching links."""
        with self.assertLogs('cki.webhook.jira', 'INFO') as logs:
            result = self.jira_tracker.remove_remote_link(
                'https://gitlab.com/group/project/-/merge_requests/778'
            )

            # It should have pretended to delete two RemoteLinks.
            records = [record for record in logs.records if 'Deleting resource' in record.message]
            self.assertEqual(len(records), 2)

            # These are the internal IDs of the links.
            self.assertIn('id=654373', records[0].message)
            self.assertIn('id=1845188', records[1].message)

        self.assertIs(result, True)

        for link in self.jira_tracker._jira_remote_links:
            if link.id in (654373, 1845188):
                link.delete.assert_called_once()
            else:
                link.delete.assert_not_called()

    def test_set_remote_link_no_changes(self) -> None:
        """Does not do anything as the RemoteLink already exists and there is nothing to change."""
        with self.assertLogs('cki.webhook.tracker', 'INFO') as logs:
            result = self.jira_tracker.set_remote_link(
                url='https://gitlab.com/group/project/-/merge_requests/123',
                title='Merge Request group/project!123: my cool MR',
                link_gid='kwf-mr-link:https://gitlab.com/group/project/-/merge_requests/123',
                extra_destination=self.EXTRA_DESTINATION
            )

            self.assertTrue(any('matches input' in record.message for record in logs.records))

        self.assertFalse(result)

        for link in self.jira_tracker._jira_remote_links:
            link.delete.assert_not_called()
            link.update.assert_not_called()

        self.mock_add_remote_link.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_set_remote_link_updates_existing(self) -> None:
        """Updates the existing RemoteLink with new values."""
        with self.assertLogs('cki.webhook.tracker', 'INFO') as logs:
            result = self.jira_tracker.set_remote_link(
                url='https://gitlab.com/group/project/-/merge_requests/778',
                title='Merge Request group/project!778: AN UPDATED MR',
                link_gid='kwf-mr-link:https://gitlab.com/group/project/-/merge_requests/778',
                extra_destination=self.EXTRA_DESTINATION
            )

            self.assertTrue(any('Updating existing' in record.message for record in logs.records))

        self.assertFalse(result)

        for link in self.jira_tracker._jira_remote_links:
            if link.id == 654373:
                link.delete.assert_called_once()
            else:
                link.delete.assert_not_called()

            if link.id == 1845188:
                link.update.assert_called_once()
            else:
                link.update.assert_not_called()

        self.mock_add_remote_link.assert_not_called()

    @mock.patch('webhook.jira.is_production_or_staging', mock.Mock(return_value=True))
    def test_set_remote_link_adds_new(self) -> None:
        """Adds a new RemoteLink when no existing link has the same URL."""
        self.jira_tracker.session.is_production_or_staging = True

        with self.assertLogs('cki.webhook.tracker', 'INFO') as logs:
            result = self.jira_tracker.set_remote_link(
                url='https://gitlab.com/group/project/-/merge_requests/111',
                title='Merge Request group/project!111: a fresh mr',
                link_gid='kwf-mr-link:https://gitlab.com/group/project/-/merge_requests/111',
                extra_destination=self.EXTRA_DESTINATION
            )

            self.assertTrue(any('Adding new' in record.message for record in logs.records))

        self.assertTrue(result)

        for link in self.jira_tracker._jira_remote_links:
            link.delete.assert_not_called()
            link.update.assert_not_called()

        self.mock_add_remote_link.assert_called_once()


class TestTrackerCache(KwfTestCase):
    """Tests for the TrackerCache class."""

    def setUp(self):
        """Set up the responses for bugzilla and make a session."""
        super().setUp()

        self.response_bugzilla_setup()
        self.response_jira_setup()

        self.test_session = self.base_session()
        self.test_cache = create_test_cache(self.test_session)

    def test_creates_cache_from_input(self) -> None:
        """Creates a properly linked cache from the input data."""
        test_cache = self.test_cache

        # We have 26 test_keys and 1 cve.
        self.assertEqual(len(test_cache), 27)

        # Each entry is linked back to this cache instance.
        for key in test_cache.keys():
            self.assertIs(test_cache[key]._cache, test_cache)

        # Each input key string or int has been converted to a JiraKey or BugzillaID.
        for key in test_cache.trackers.keys():
            self.assertIsInstance(key, kwf_tracker.JiraKey)

        for key in test_cache.cves.keys():
            self.assertIsInstance(key, kwf_tracker.CveID)

        # There are 27 objects total. 24 are Kwf Trackers, and one is a CVE flaw.
        # The two "leftovers" are BasicTracker instances that we will generally ignore.
        self.assertEqual(len(test_cache.data), 27)
        self.assertEqual(len(test_cache.trackers), 24)
        self.assertEqual(len(test_cache.cves), 1)

        # Trying to 'get' an existing flaw by CVE ID works.
        cve_tracker = test_cache['CVE-2024-41090']
        self.assertIs(test_cache[2299240], cve_tracker)

        # 'Contains' does its thing.
        self.assertIn('CVE-2024-41090', test_cache)
        self.assertIn(2299240, test_cache)
        self.assertIn('RHEL-47894', test_cache)
        self.assertNotIn('RHEL-999999', test_cache)
        self.assertNotIn(1234567, test_cache)
        self.assertNotIn('CVE-2026-12453', test_cache)

        # Trying to 'get' with a key which is not the right type raises a TypeError.
        with self.assertRaises(TypeError):
            test_cache['abc']

        # Trying to set an entry with the wrong key type raises a TypeError.
        with self.assertRaises(TypeError):
            test_cache[(1, 2)] = cve_tracker

        # Trying to set an entry with the wrong value type raises a TypeError.
        with self.assertRaises(TypeError):
            test_cache[cve_tracker.id] = 'CVE-2024-41090'

    @mock.patch('webhook.kwf_tracker.get_issues')
    def test_get_issues(self, mock_get_issues) -> None:
        """Fetches and returns the requested trackers and their CVE flaws, if any."""
        empty_test_cache = kwf_tracker.TrackerCache(session=self.test_session)

        mock_get_issues.return_value = [self.test_cache.trackers['RHEL-1559']._api_object]

        result = empty_test_cache.get('RHEL-1559')

        self.assertIsInstance(result, kwf_tracker.KwfIssueTracker)
        self.assertEqual(result.id, 'RHEL-1559')

    def test_tracker_cache_get_default(self) -> None:
        """Returns the default when the given key cannot be found."""
        test_cache = self.test_cache

        # Patch out the get_bugs() call so get() falls back to returning the default.
        with mock.patch('webhook.kwf_tracker.get_bugs') as mock_get_bugs:
            mock_get_bugs.return_value = []
            self.assertEqual(test_cache.get(1111111, default='hamster'), 'hamster')

    @mock.patch('webhook.kwf_tracker.get_issues')
    @mock.patch('webhook.kwf_tracker.get_bugs')
    def test_cache_cves(self, mock_get_bugs, mock_get_issues) -> None:
        """Fetches and returns the requested CVE flaws and their trackers."""
        empty_test_cache = kwf_tracker.TrackerCache(session=self.test_session)

        # Does nothing if the input is empty.
        self.assertEqual(empty_test_cache.cache_cves([]), {})
        mock_get_bugs.assert_not_called()
        mock_get_issues.assert_not_called()

        # Steal the API response objects for our mock_get return values, ha.
        mock_get_bugs.return_value = [self.test_cache.cves['CVE-2024-41090']._api_object]
        mock_get_issues.return_value = [
            self.test_cache.trackers[tracker_id]._api_object for
            tracker_id in ['RHEL-50265', 'RHEL-50263', 'RHEL-50264']
        ]

        results = empty_test_cache.cache_cves([CveID('CVE-2024-41090')])

        # It returns a dict with the requested CVE IDs as keys.
        self.assertIsInstance(results, dict)
        self.assertCountEqual(['CVE-2024-41090'], results)

        # The new CVE is the only CVE in the now not-empty cache.
        self.assertEqual(results, empty_test_cache.cves)

        # The cache has the CVE flaw (2299240) and the three trackers from jira..
        self.assertCountEqual(['RHEL-50265', 'RHEL-50263', 'RHEL-50264', 2299240], empty_test_cache)

    @mock.patch('webhook.kwf_tracker.get_issues')
    @mock.patch('webhook.kwf_tracker.get_bugs')
    def test_cache_issues(self, mock_get_bugs, mock_get_issues) -> None:
        """Fetches and returns the requested trackers and their CVE flaws, if any."""
        empty_test_cache = kwf_tracker.TrackerCache(session=self.test_session)

        # Does nothing if the input is empty.
        self.assertEqual(empty_test_cache.cache_issues([]), {})
        mock_get_bugs.assert_not_called()
        mock_get_issues.assert_not_called()

        # Steal the API response objects for our mock_get return values, ha.
        mock_get_bugs.return_value = [self.test_cache.cves['CVE-2024-41090']._api_object]

        jira_api_objects = [
            self.test_cache.trackers[tracker_id]._api_object for
            tracker_id in ['RHEL-50265', 'RHEL-50263', 'RHEL-50264']
        ]

        mock_get_issues.side_effect = [jira_api_objects[:1], jira_api_objects[1:]]

        results = empty_test_cache.cache_issues([JiraKey('RHEL-50265')])

        # It returns a dict with the requested issue as the key.
        self.assertIsInstance(results, dict)
        self.assertCountEqual(['RHEL-50265'], results)

        # RHEL-50265 is a tracker for CVE-2024-41090 so that is now in the cache.
        self.assertCountEqual(['CVE-2024-41090'], empty_test_cache.cves)

        # The cache has the CVE flaw (2299240) and the three trackers from jira..
        self.assertCountEqual(['RHEL-50265', 'RHEL-50263', 'RHEL-50264', 2299240], empty_test_cache)
