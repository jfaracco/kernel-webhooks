"""Tests for the cvedb module."""
from pathlib import Path
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import cvedb

EXPECTED_DATA = {
    '844f104790bd69c2e4dbb9ee3eba46fde1fcea7b': 'CVE-2024-26596',
    '483ae90d8f976f8339cf81066312e1329f2d3706': 'CVE-2024-26586',
    'b33fb5b801c6db408b774a68e7c8722796b59ecc': 'CVE-2024-26597',
    '92e470163d96df8db6c4fa0f484e4a229edb903d': 'CVE-2024-26594',
    '8590541473188741055d27b955db0777569438e3': 'CVE-2024-26584',
    'e01e3934a1b2d122919f73bc6ddbe1cdafc4bbdb': 'CVE-2024-26585',
    '715d82ba636cb3629a6e18a33bb9dbe53f9936ee': 'CVE-2024-26591',
    'ea937f77208323d35ffe2f8d8fc81b00118bfcda': 'CVE-2024-26587',
    '32b55c5ff9103b8508c1e04bfa5a08c64e7a925f': 'CVE-2024-26582',
    '22c7fa171a02d310e3a3f6ed46a698ca8a0060ed': 'CVE-2024-26589',
    'ad362fe07fecf0aba839ff2cc59a3617bd42c33f': 'CVE-2024-26598',
    '36a87385e31c9343af9a4756598e704741250a67': 'CVE-2024-26588',
    'c1c9d0f6f7f1dbf29db996bd8e166242843a5f21': 'CVE-2024-26593',
    '38d20c62903d669693a1869aa68c4dd5674e2544': 'CVE-2024-26592',
    '118a8cf504d7dfa519562d000f423ee3ca75d2c4': 'CVE-2024-26590',
    'a297d07b9a1e4fb8cda25a4a2363a507d294b7c9': 'CVE-2024-26599',
    '60c0c230c6f046da536d3df8b39a20b9a9fd6af0': 'CVE-2024-26581',
    'aec7961916f3f9e88766e2688992da6980f11b8d': 'CVE-2024-26583',
    'efeb7dfea8ee10cdec11b6b6ba4e405edbe75809': 'CVE-2024-26595'
}


class TestVulnerabilityManagerData(KwfTestCase):
    """Tests for the methods of a VulnerabilityManager instance."""

    @mock.patch('webhook.cvedb.Repo')
    def test_loading(self, mock_git_repo) -> None:
        """Generates the expected data from the test assets."""
        # We are not operating this test with a real git repo so we have the fudge this part.
        mock_repo = mock.Mock()
        mock_repo.head.commit.hexsha = 'abc123'
        mock_git_repo.return_value = mock_repo

        vulns_path = 'tests/assets/vulns/'
        test_vulns_manager = cvedb.VulnerabilityManager.new(vulns_path)

        # This causes the cache to be loaded.
        test_vulns_manager._check_cache()

        self.assertCountEqual(test_vulns_manager._cve_commits, EXPECTED_DATA)

        self.assertEqual(
            test_vulns_manager('8590541473188741055d27b955db0777569438e3'),
            'CVE-2024-26584'
        )

        self.assertEqual(
            test_vulns_manager('aec7961916f3f9e88766e2688992da6980f11b8d'),
            'CVE-2024-26583'
        )

        # This hash doesn't exist so expect an empty list in return.
        self.assertIsNone(test_vulns_manager('abcd1234abcd1234abcd1234abcd1234abcd1234'))

    def test_new(self) -> None:
        """Raises exceptions on bad input."""
        with self.assertRaisesRegex(RuntimeError, 'No vulns_source path given'):
            cvedb.VulnerabilityManager.new('')

        with self.assertRaisesRegex(RuntimeError, r'Path .* does not exist'):
            cvedb.VulnerabilityManager.new('beep/boop')


class TestHelpers(KwfTestCase):
    """Tests for the module's internal functions."""

    def test_build_cve_commit_map(self) -> None:
        """Returns the expected data."""
        vulns_path = Path('tests/assets/vulns/')

        vulns_map = cvedb._build_cve_commit_map(vulns_path)

        self.assertEqual(vulns_map, EXPECTED_DATA)

        for sha, cve in vulns_map.items():
            with self.subTest(sha=sha, cve=cve):
                self.assertTrue(
                    cve.startswith('CVE-'),
                    'All cve values should start with `CVE-`.'
                )

                self.assertIsInstance(sha, str, 'The index values should be a string.')

    def test_build_cve_commit_map_bad_path(self) -> None:
        """Raises a RuntimeError when the path has no valid CVE data under it."""
        bad_path = Path('tests/assets/gitlab_rest_api/')

        with self.assertRaises(RuntimeError):
            cvedb._build_cve_commit_map(bad_path)

    def test_cve_json_url(self) -> None:
        """Ensure sane URL returned for a given CVE."""
        cve = 'CVE-2024-26595'
        vulns = 'https://git.kernel.org/pub/scm/linux/security/vulns.git'
        expected = f'{vulns}/tree/cve/published/2024/{cve}.json'
        ret = cvedb.cve_json_url(cve)
        self.assertEqual(ret, expected)
