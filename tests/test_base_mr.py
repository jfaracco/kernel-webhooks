"""Tests for the base library."""
from dataclasses import dataclass
import os
import typing
from unittest import mock

from cki_jira.common import BugzillaID
from cki_jira.common import GitlabURL
from cki_jira.common import JiraKey
from cki_jira.description import Description
from gitlab import GitlabHttpError
from jira.resources import Comment
from jira.resources import Issue
from jira.resources import PinnedComment

from tests import fakes
from tests.helpers import KwfTestCase
from webhook import base_mr
from webhook.base_mr_mixins import CommitsMixin
from webhook.base_mr_mixins import DependsMixin
from webhook.defs import GitlabGID
from webhook.description import MRDescription
from webhook.kwf_tracker import KwfIssueTracker
from webhook.kwf_tracker import TrackerCache
from webhook.rh_metadata import Projects
from webhook.session import BaseSession
from webhook.session import GitlabGraph
from webhook.session import SessionRunner
from webhook.users import User

if typing.TYPE_CHECKING:
    from responses import Response


MR_ID = 123
NAMESPACE = 'redhat/centos-stream/src/kernel/centos-stream-9'
MR_URL = GitlabURL(f'https://gitlab.com/{NAMESPACE}/-/merge_requests/{MR_ID}')
PROJECT_ID = 7654

CURRENT_USER = {'email': 'bot@example.com',
                'gid': 'gid://gitlab/User/7654321',
                'name': 'Bot User',
                'username': 'bot_user'}
AUTHOR1 = {'email': 'test_user1@example.com',
           'gid': 'gid://gitlab/User/1111',
           'name': 'Test User1',
           'username': 'test_user1'}
MR1_QUERY = {'author': AUTHOR1,
             'approved': False,
             'commitCount': 2,
             'description': 'JIRA: https://issues.redhat.com/browse/RHEL-12345\nDepends: !456',
             'draft': False,
             'files': [{'path': 'include/linux.h'}],
             'headPipeline': {'id': 'gid://gitlab/Ci::Pipeline/12345678'},
             'global_id': 'gid://gitlab/MergeRequest/12121212',
             'labels': {'nodes': [{'title': 'Acks::NeedsReview'},
                                  {'title': 'Bugzilla::NeedsReview'},
                                  {'title': 'CKI::Running'},
                                  {'title': 'CKI_RT::Running'},
                                  {'title': 'CommitRefs::OK'},
                                  {'title': 'Dependencies::OK'},
                                  {'title': 'Signoff::OK'}]},
             'project': {'id': 'gid://gitlab/Project/24152864'},
             'state': 'opened',
             'sourceBranch': 'feature',
             'targetBranch': 'main',
             'title': 'Another test MR'}
AUTHOR2 = {'email': 'test_user2@example.com',
           'gid': 'gid://gitlab/User/2222',
           'name': 'Test User2',
           'username': 'test_user2'}
MR2_QUERY = {'author': AUTHOR2,
             'approved': False,
             'commitCount': 1,
             'description': 'JIRA: https://issues.redhat.com/browse/RHEL-645323',
             'draft': True,
             'files': [{'path': 'include/linux.h'}],
             'headPipeline': {'id': 'gid://gitlab/Ci::Pipeline/7654321'},
             'global_id': 'gid://gitlab/MergeRequest/23232323',
             'labels': {'nodes': [{'title': 'Acks::NeedsReview'},
                                  {'title': 'Bugzilla::OK'},
                                  {'title': 'CKI::OK'},
                                  {'title': 'CKI_RT::Failed::merge'},
                                  {'title': 'CommitRefs::OK'},
                                  {'title': 'Dependencies::OK'},
                                  {'title': 'Signoff::OK'}]},
             'project': {'id': 'gid://gitlab/Project/24152864'},
             'state': 'opened',
             'sourceBranch': 'feature',
             'targetBranch': 'main',
             'title': 'The first test MR'}

MR1_QUERY_RESULT = {'currentUser': CURRENT_USER,
                    'project': {'mr': MR1_QUERY}}
MR2_QUERY_RESULT = {'currentUser': CURRENT_USER,
                    'project': {'mr': MR2_QUERY}}

MR1_DESCRIPTION_QUERY_RESULT = {456: MR2_QUERY['description']}

COMMITS = [{'authorEmail': AUTHOR1['email'],
            'authorName': AUTHOR1['name'],
            'committerEmail': AUTHOR1['email'],
            'committerName': AUTHOR1['name'],
            'author': AUTHOR1,
            'authoredDate': '2022-12-19T08:58:26+01:00',
            'description': 'JIRA: https://issues.redhat.com/browse/RHEL-12345',
            'sha': 'ba94fe49a2345372f6e59e5c7fc39335efed344d',
            'title': 'mr1 commit'},
           {'authorEmail': AUTHOR2['email'],
            'authorName': AUTHOR2['name'],
            'committerEmail': AUTHOR2['email'],
            'committerName': AUTHOR2['name'],
            'author': AUTHOR2,
            'authoredDate': '2022-12-19T08:58:26+01:00',
            'description': 'JIRA: https://issues.redhat.com/browse/RHEL-645323',
            'sha': '57498dee55913567669dd00c46bb07ecc2b711c3',
            'title': 'mr2 commit'}]

MR1_COMMITS_RESULT = {'project': {'mr': {'commits': {'nodes': COMMITS}}}}
MR2_COMMITS_RESULT = {'project': {'mr': {'commits': {'nodes': COMMITS[1:]}}}}


def create_mr(**kwargs) -> base_mr.BaseMR:
    """Return a fresh MR object."""
    test_session = BaseSession.new()
    graphql = kwargs.get('graphql') or mock.Mock()
    if kwargs.get('query_side_effect'):
        graphql.client.query.side_effect = kwargs['query_side_effect']
    else:
        graphql.client.query.return_value = kwargs.get('query_return_value') or {}
    test_session.get_graphql = mock.Mock(return_value=graphql)
    url = kwargs.get('mr_url') or MR_URL
    if not (gl_instance := kwargs.get('gl_instance')):
        gl_instance = fakes.FakeGitLab()
        gl_project = gl_instance.add_project(PROJECT_ID, NAMESPACE)
        gl_project.add_mr(int(url.rsplit('/', 1)[-1]))
    test_session.get_gl_instance = mock.Mock(return_value=gl_instance)
    mr_func = kwargs.get('mr_func') or base_mr.BaseMR
    extra_args = kwargs.get('extra_args', [])
    extra_kwargs = kwargs.get('extra_kwargs', {})
    test_session.rh_projects = kwargs.get('projects') or Projects()
    return mr_func.new(test_session, url, *extra_args, **extra_kwargs)


@mock.patch('webhook.session.get_gl_instance')
class TestBaseMR(KwfTestCase):
    """Tests for the BaseMR dataclass."""

    def test_basemr_empty(self, mock_get_gl_instance):
        """Returns a useles but sane object."""
        test_mr = create_mr()
        mock_get_gl_instance.return_value = test_mr.session.gl_instance
        # Basic attributes.
        self.assertEqual(test_mr.url, MR_URL)
        self.assertIs(test_mr.approved, False)
        self.assertIs(test_mr.author, None)
        self.assertEqual(test_mr.commit_count, 0)
        self.assertEqual(test_mr.description.text, '')
        self.assertIs(test_mr.draft, False)
        self.assertEqual(test_mr.files, [])
        self.assertEqual(test_mr.labels, [])
        self.assertIsNone(test_mr.head_pipeline_gid)
        self.assertIsNone(test_mr.global_id)
        self.assertIsNone(test_mr.project_gid)
        self.assertIs(test_mr.state, base_mr.defs.MrState.UNKNOWN)
        self.assertEqual(test_mr.source_branch, '')
        self.assertEqual(test_mr.target_branch, '')
        self.assertEqual(test_mr.title, '')
        # Derived properties.
        self.assertFalse(test_mr.author_can_merge)
        self.assertFalse(test_mr.has_depends)
        self.assertFalse(test_mr.is_build_mr)
        self.assertFalse(test_mr.is_autobot_mr)
        self.assertFalse(test_mr.ready_for_qa)
        self.assertFalse(test_mr.ready_for_merge)
        self.assertTrue(test_mr.manage_jiras)
        self.assertFalse(test_mr.merge_conflict)
        self.assertFalse(test_mr.merge_warning)
        self.assertEqual(test_mr.namespace, NAMESPACE)
        self.assertEqual(test_mr.iid, MR_ID)
        self.assertIsNone(test_mr.rh_branch)
        self.assertIsInstance(test_mr.description, MRDescription)
        self.assertIsInstance(test_mr.tracker_cache, TrackerCache)
        self.assertIs(test_mr.gl_project, test_mr.session.gl_instance.projects.get(NAMESPACE))
        self.assertIs(test_mr.gl_mr, test_mr.gl_project.mergerequests.get(MR_ID))
        self.assertIs(test_mr.rh_project, test_mr.session.rh_projects.get_project_by_id(24152864))

        # Test the __repr__.
        repr_str = str(test_mr)
        self.assertIn(f'{NAMESPACE}!{MR_ID}', repr_str)

    def test_basemr_with_content(self, mock_get_gl_instance):
        """Returns a working BaseMR object."""
        graphql = mock.Mock()
        graphql.client.query.side_effect = [MR1_QUERY_RESULT]
        graphql.get_mr_descriptions.return_value = MR1_DESCRIPTION_QUERY_RESULT
        mr_labels = [label['title'] for label in MR1_QUERY['labels']['nodes']]
        graphql.get_all_mr_labels.return_value = mr_labels

        mr1 = create_mr(graphql=graphql)
        mock_get_gl_instance.return_value = mr1.session.gl_instance

        # mr1 content
        self.assertEqual(mr1.url, MR_URL)
        self.assertEqual(mr1.author.name, AUTHOR1['name'])
        self.assertEqual(mr1.commit_count, 2)
        self.assertEqual(mr1.description.text, MR1_QUERY['description'])
        self.assertIs(mr1.draft, False)
        self.assertEqual(mr1.files, ['include/linux.h'])
        self.assertEqual(mr1.labels, mr_labels)
        self.assertEqual(mr1.global_id, MR1_QUERY['global_id'])
        self.assertEqual(mr1.project_gid.id, 24152864)
        self.assertEqual(mr1.head_pipeline_gid.id, 12345678)
        self.assertIs(mr1.state, base_mr.defs.MrState.OPENED)
        self.assertEqual(mr1.source_branch, 'feature')
        self.assertEqual(mr1.target_branch, 'main')
        self.assertEqual(mr1.title, MR1_QUERY['title'])
        self.assertFalse(mr1.ready_for_qa)
        self.assertFalse(mr1.ready_for_merge)
        self.assertFalse(mr1.merge_warning)
        self.assertFalse(mr1.merge_conflict)
        # mr1 derived properties.
        self.assertEqual(mr1.is_build_mr, False)
        self.assertEqual(mr1.namespace, NAMESPACE)
        self.assertEqual(mr1.iid, MR_ID)
        self.assertIs(mr1.rh_branch, mr1.session.rh_projects.get_target_branch(24152864, 'main'))
        self.assertIs(mr1.gl_project, mr1.session.gl_instance.projects.get(NAMESPACE))

    @mock.patch.object(DependsMixin, '_set_mr_blocks', mock.Mock())
    def test_basemr_with_commits_and_depends(self, mock_get_gl_instance):
        """Returns a working BaseMR object."""
        @dataclass(repr=False)
        class BaseWithCommits(CommitsMixin, DependsMixin, base_mr.BaseMR):
            pass

        graphql = mock.Mock()
        graphql.client.query.side_effect = [MR1_QUERY_RESULT, MR1_COMMITS_RESULT,
                                            MR2_QUERY_RESULT, MR2_COMMITS_RESULT]
        graphql.get_mr_descriptions.return_value = MR1_DESCRIPTION_QUERY_RESULT
        mr1_labels = [label['title'] for label in MR1_QUERY['labels']['nodes']]
        mr2_labels = [label['title'] for label in MR2_QUERY['labels']['nodes']]
        graphql.get_all_mr_labels.side_effect = [mr1_labels, mr2_labels]

        mr1 = create_mr(graphql=graphql, mr_func=BaseWithCommits)

        mr1 = typing.cast(BaseWithCommits, mr1)

        mock_get_gl_instance.return_value = mr1.session.gl_instance
        mr1_commits = mr1.commits
        mr1_dependencies = mr1.depends_mrs
        mr2 = mr1_dependencies[0]
        mr2_commits = mr2.commits

        mr1_diffs = [{'new_path': file['path']} for file in MR1_QUERY['files']]
        mr1.gl_project.repository_compare = mock.Mock(return_value={'diffs': mr1_diffs})

        # mr1 content
        self.assertEqual(mr1.namespace, NAMESPACE)
        self.assertEqual(mr1.iid, MR_ID)
        self.assertEqual(mr1.author.name, AUTHOR1['name'])
        self.assertEqual(mr1.description.text, MR1_QUERY['description'])
        self.assertIs(mr1.draft, False)
        self.assertEqual(mr1.labels, mr1_labels)
        self.assertEqual(mr1.global_id, MR1_QUERY['global_id'])
        self.assertIs(mr1.is_dependency, False)
        self.assertEqual(mr1.project_gid.id, 24152864)
        self.assertIs(mr1.state, base_mr.defs.MrState.OPENED)
        self.assertEqual(mr1.target_branch, 'main')
        self.assertEqual(mr1.title, MR1_QUERY['title'])
        # mr1 derived properties.
        self.assertEqual(mr1.is_build_mr, False)
        self.assertEqual(len(mr1_commits), 2)
        self.assertEqual(len(mr1.commits_without_depends), 1)
        self.assertEqual(len(mr1.all_commits), 3)
        self.assertEqual(len(mr1.all_descriptions), 3)
        self.assertEqual(mr1.files, ['include/linux.h'])
        self.assertIs(mr1.rh_branch, mr1.session.rh_projects.get_target_branch(24152864, 'main'))
        self.assertEqual(mr1.first_dep_sha, COMMITS[1]['sha'])
        self.assertIs(mr1.gl_project, mr1.session.gl_instance.projects.get(NAMESPACE))
        self.assertIs(mr1.has_internal, False)
        self.assertIs(mr1.has_untagged, False)

        # mr2 content
        self.assertEqual(mr2.namespace, NAMESPACE)
        self.assertEqual(mr2.iid, 456)
        self.assertEqual(mr2.author.name, AUTHOR2['name'])
        self.assertEqual(mr2.description.text, MR2_QUERY['description'])
        self.assertIs(mr2.draft, True)
        self.assertEqual(mr2.labels, mr2_labels)
        self.assertEqual(mr2.global_id, MR2_QUERY['global_id'])
        self.assertEqual(mr2.head_pipeline_gid.id, 7654321)
        self.assertIs(mr2.is_dependency, True)
        self.assertEqual(mr2.project_gid.id, 24152864)
        self.assertIs(mr2.state, base_mr.defs.MrState.OPENED)
        self.assertEqual(mr2.target_branch, 'main')
        self.assertEqual(mr2.title, MR2_QUERY['title'])
        # mr2 derived properties.
        self.assertEqual(mr2.is_build_mr, False)
        self.assertEqual(len(mr2_commits), 1)
        self.assertEqual(len(mr2.commits_without_depends), 1)
        self.assertEqual(len(mr2.all_commits), 2)
        self.assertEqual(len(mr2.all_descriptions), 2)
        self.assertIs(mr2.files, mr2.files)
        self.assertIs(mr2.rh_branch, mr2.session.rh_projects.get_target_branch(24152864, 'main'))
        self.assertEqual(mr2.first_dep_sha, '')
        self.assertIs(mr2.gl_project, mr1.session.gl_instance.projects.get(NAMESPACE))
        self.assertIs(mr2.has_internal, False)
        self.assertIs(mr2.has_untagged, False)

    def test_basemr_labels(self, mock_get_gl_instance):
        """Adds/removes labels and validates the results."""
        test_mr = create_mr()
        mock_get_gl_instance.return_value = test_mr.session.gl_instance
        to_add = ['label1']
        to_remove = ['label2']
        with mock.patch('webhook.common.add_label_to_merge_request') as mock_add:
            mock_add.return_value = to_add
            test_mr.add_labels(to_add)
            self.assertEqual(test_mr.labels, to_add)

        with mock.patch('webhook.common.add_label_to_merge_request') as mock_add:
            mock_add.return_value = []
            with self.assertRaises(RuntimeError):
                test_mr.add_labels(to_add)

        existing = test_mr.labels.copy()
        test_mr.labels.append(to_remove)
        with mock.patch('webhook.common.remove_labels_from_merge_request') as mock_remove:
            mock_remove.return_value = existing
            test_mr.remove_labels(to_remove)
            self.assertEqual(test_mr.labels, existing)

        test_mr.labels.append(to_remove)
        with mock.patch('webhook.common.remove_labels_from_merge_request') as mock_remove:
            mock_remove.return_value = to_remove
            with self.assertRaises(RuntimeError):
                test_mr.remove_labels(to_remove)


class TestNewDependencies(KwfTestCase):

    @dataclass(repr=False, kw_only=True)
    class TestBaseMR(base_mr.BaseMR):
        """A test subclass of BaseMR with an extra attribute."""

        attribute_a: str = ''

    def test_new_instance(self) -> None:
        """Calls self.new() to construct a new instance with the given URL."""
        test_session = self.session_runner('ack_nack')

        original_url = GitlabURL('https://gitlab.com/cki-project/kernel-ark/-/merge_requests/123')
        new_url = GitlabURL('https://gitlab.com/cki-project/kernel-ark/-/merge_requests/456')

        test_mr = self.TestBaseMR(session=test_session, url=original_url, attribute_a='hello')

        with mock.patch.object(base_mr.BaseMR, 'do_mr_query', mock.Mock(return_value={})):
            new_mr = test_mr._new_instance(new_url)

        self.assertEqual(new_mr.url, new_url)
        self.assertEqual(new_mr.attribute_a, 'hello')

    def test_dependency_mrs(self) -> None:
        """Constructs new instances from the Depends: tags in the MR description."""
        test_session = self.session_runner('ckihook')

        kwf_description = \
            Description('Depends: https://gitlab.com/cki-project/kernel-ark/-/merge_requests/555\n'
                        'Depends: https://gitlab.com/group/project/-/merge_requests/222\n'
                        'Depends: https://gitlab.com/cki-project/kernel-ark/-/merge_requests/333\n'
                        )

        url = GitlabURL('https://gitlab.com/cki-project/kernel-ark/-/merge_requests/123')

        test_mr = base_mr.BaseMR(session=test_session, url=url, kwf_description=kwf_description)

        # There are three depends tags ...
        self.assertEqual({555, 222, 333}, test_mr.kwf_description.depends)

        with mock.patch.object(base_mr.BaseMR, 'do_mr_query', mock.Mock(return_value={})):
            depends_mrs = test_mr.dependency_mrs

        # ... but only two of the depends tags are from a namespace matching the test_mr.
        self.assertEqual({555, 333}, {mr.iid for mr in depends_mrs})


def make_comment(
    message: str = 'test comment',
    author_name: str = 'example_username',
    comment_id: int = 54321,
    pinned: bool = False,
    jira_session: typing.Any = None
) -> Comment | PinnedComment:
    """Return a Comment or PinnedComment with the given properties."""
    comment_class = Comment if not pinned else PinnedComment

    raw = {
        'author': {
            'active': True,
            'name': author_name
        },
        'body': message,
        'id': str(comment_id)
    }

    if pinned:
        raw = {'comment': raw}

    comment = comment_class(options={}, session=jira_session, raw=raw)

    # Patch out the delete() method to see if it was called.
    comment.delete = mock.Mock()

    return comment


TEST_ENVIRON = {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'}


@mock.patch.dict(os.environ, TEST_ENVIRON, clear=True)
class BaseMrTrackerLinks(KwfTestCase):
    """Tests for the BaseMR link_to_tracker & unlink_from_tracker functions."""

    def setUp(self):
        """Provide a BaseMR & KwfIssueTracker instances.."""
        super().setUp()

        test_session = self.session_runner('ckihook')
        test_mr = base_mr.BaseMR(session=test_session, url=MR_URL)

        test_bz_tracker = KwfIssueTracker(id=BugzillaID(123456), session=test_session)
        test_bz_tracker.set_remote_link = mock.Mock()
        test_bz_tracker.post_comment = mock.Mock()
        test_bz_tracker.remove_remote_link = mock.Mock()
        test_bz_tracker.comments = mock.Mock()

        test_jira_tracker = KwfIssueTracker(id=JiraKey('RHEL-1234'), session=test_session)
        test_jira_tracker.set_remote_link = mock.Mock()
        test_jira_tracker.post_comment = mock.Mock()
        test_jira_tracker.remove_remote_link = mock.Mock()
        test_jira_tracker.comments = mock.Mock()

        self.test_session = test_session
        self.test_mr = test_mr
        self.test_bz_tracker = test_bz_tracker
        self.test_jira_tracker = test_jira_tracker

    def test_link_to_tracker_not_issue(self) -> None:
        """Does nothing when the input KwfIssueTracker is not a jira issue."""
        test_mr = self.test_mr
        test_tracker = self.test_bz_tracker
        self.assertFalse(test_tracker.is_jira)

        with self.assertLogs('cki.webhook.base_mr', 'WARNING'):
            test_mr.link_to_tracker(test_tracker)

        test_tracker.set_remote_link.assert_not_called()
        test_tracker.post_comment.assert_not_called()

    def test_link_to_tracker_jira_no_comment(self) -> None:
        """Calls KwfIssueTracker.set_remote_link and nothing else when it returns False."""
        test_mr = self.test_mr
        test_tracker = self.test_jira_tracker
        self.assertTrue(test_tracker.is_jira)

        # Make set_remote_link mock return False, indicating no new RemoteLink was created.
        test_tracker.set_remote_link.return_value = False

        test_mr.link_to_tracker(test_tracker)

        test_tracker.set_remote_link.assert_called_once()
        test_tracker.post_comment.assert_not_called()

    def test_link_to_tracker_jira_new_comment(self) -> None:
        """Calls KwfIssueTracker.set_remote_link & when it returns True, also calls post_comment."""
        test_mr = self.test_mr
        test_tracker = self.test_jira_tracker
        self.assertTrue(test_tracker.is_jira)

        # Make set_remote_link mock return True, indicating a new RemoteLink was created.
        test_tracker.set_remote_link.return_value = True

        test_mr.link_to_tracker(test_tracker)

        test_tracker.set_remote_link.assert_called_once()
        test_tracker.post_comment.assert_called_once()

    def test_unlink_from_tracker_not_issue(self) -> None:
        """Does nothing when the input KwfIssueTracker is not a jira issue."""
        test_mr = self.test_mr
        test_tracker = self.test_bz_tracker
        self.assertFalse(test_tracker.is_jira)

        with self.assertLogs('cki.webhook.base_mr', 'WARNING'):
            test_mr.unlink_from_tracker(test_tracker)

        test_tracker.remove_remote_link.assert_not_called()
        test_tracker.comments.assert_not_called()

    def test_unlink_from_tracker_jira_no_comment(self) -> None:
        """Calls KwfIssueTracker.remove_remote_link and nothing else when it returns False."""
        test_mr = self.test_mr
        test_tracker = self.test_jira_tracker
        self.assertTrue(test_tracker.is_jira)

        # Make remove_remote_link mock return False, indicating no RemoteLink existed.
        test_tracker.remove_remote_link.return_value = False

        test_mr.unlink_from_tracker(test_tracker)

        test_tracker.remove_remote_link.assert_called_once()
        test_tracker.comments.assert_not_called()

    def test_unlink_from_tracker_jira_no_matching_comment(self) -> None:
        """Calls KwfIssueTracker.remove_remote_link & does not find a matching comment to delete."""
        test_mr = self.test_mr
        test_tracker = self.test_jira_tracker
        self.assertTrue(test_tracker.is_jira)

        # Patch out the SessionRunner.jira instance so we can control the current_user().
        mock_jira = mock.Mock(spec_set=['current_user'])
        mock_jira.return_value.current_user.return_value = 'test_user'

        # Set the mock'd test_tracker.comments() return value.
        test_tracker.comments.return_value = []

        # Make remove_remote_link mock return True, indicating a RemoteLink existed.
        test_tracker.remove_remote_link.return_value = True

        with mock.patch.object(SessionRunner, 'jira', new_callable=mock_jira):
            test_mr.unlink_from_tracker(test_tracker)

        test_tracker.remove_remote_link.assert_called_once()
        test_tracker.comments.assert_called_once()

    def test_unlink_from_tracker_jira_with_deleted_comment(self) -> None:
        """Calls KwfIssueTracker.remove_remote_link and looks for comments to delete."""
        test_session = self.test_session
        test_mr = self.test_mr
        test_tracker = self.test_jira_tracker
        self.assertTrue(test_tracker.is_jira)
        self.assertTrue(test_session.is_production_or_staging)

        # Patch out the SessionRunner.jira instance so we can control the current_user().
        mock_jira = mock.Mock(spec_set=['current_user'])
        mock_jira.return_value.current_user.return_value = 'test_user'

        # Set the mock'd test_tracker.comments() return value.
        comment1 = make_comment(comment_id=1, message='hello')
        comment2 = make_comment(comment_id=2, message=f'mr is at {MR_URL}')
        comment3 = make_comment(comment_id=3, message=f'mr is at {MR_URL}', author_name='test_user')

        test_tracker.comments.return_value = [comment1, comment2, comment3]

        # Make remove_remote_link mock return True, indicating a RemoteLink existed.
        test_tracker.remove_remote_link.return_value = True

        with mock.patch.object(SessionRunner, 'jira', new_callable=mock_jira):
            test_mr.unlink_from_tracker(test_tracker)

        test_tracker.remove_remote_link.assert_called_once()
        test_tracker.comments.assert_called_once()

        comment1.delete.assert_not_called()
        comment2.delete.assert_not_called()
        comment3.delete.assert_called_once()

    def test_unlink_from_all(self) -> None:
        """Calls unlink_from_tracker() for each issue returned by get_issues_with_link()."""
        test_mr = self.test_mr
        test_mr.unlink_from_tracker = mock.Mock()

        # A jira.resources.Issue representing a KWF issue.
        test_rhel_issue = Issue(
            options={}, session=None, raw={
                'fields': {'components': [{'name': 'kernel'}], 'issuetype': {'name': 'Bug'}},
                'id': '1234567',
                'key': 'RHEL-456',
                'self': 'https://issues.redhat.com/rest/api/2/issue/1234567'
            }
        )

        # A jira.resources.Issue representing a non-KWF issue.
        test_notkernel_issue = Issue(
            options={}, session=None, raw={
                'fields': {'components': [{'name': 'not_kernel'}], 'issuetype': {'name': 'Bug'}},
                'id': '99999',
                'key': 'RHEL-999',
                'self': 'https://issues.redhat.com/rest/api/2/issue/99999'
            }
        )

        with mock.patch.object(SessionRunner, 'jira', new_callable=mock.Mock()):
            with mock.patch('webhook.base_mr.get_issues_with_link') as mock_get_issues_with_link:
                mock_get_issues_with_link.return_value = [test_rhel_issue, test_notkernel_issue]

                test_mr.unlink_from_all()

                mock_get_issues_with_link.assert_called_once()

        test_mr.unlink_from_tracker.assert_called_once()

        # Make sure the call to `unlink_from_tracker()` was done with RHEL-456 (test_rhel_issue).
        self.assertEqual(
            test_mr.unlink_from_tracker.call_args[0][0].id,
            'RHEL-456'
        )


class BaseMRBasic(KwfTestCase):
    """Tests for an "empty" BaseMR instance."""

    def setUp(self):
        """Provide a SessionRunner."""
        super().setUp()
        self.test_session = self.session_runner('ckihook')

    def test_basemr_labels_with_prefix(self) -> None:
        """Returns the labels of the MR which have the given prefix."""
        test_labels = ['CKI::x86_64::OK', 'CKI::OK', 'readyForQA', 'Blocked', 'Acks::OK']
        test_mr = base_mr.BaseMR(session=self.test_session, url=MR_URL, labels=test_labels)

        self.assertCountEqual(
            test_mr.labels_with_prefix('CKI'),
            ['CKI::x86_64::OK', 'CKI::OK']
        )

        self.assertCountEqual(
            test_mr.labels_with_prefix('Acks'),
            ['Acks::OK']
        )

    def test_basemr_labels_with_scope(self) -> None:
        """Returns the labels of the MR which have the given MrScope."""
        test_labels = ['CKI::x86_64::OK', 'CKI::OK', 'readyForQA', 'Blocked', 'Acks::OK']
        test_mr = base_mr.BaseMR(session=self.test_session, url=MR_URL, labels=test_labels)

        self.assertCountEqual(
            test_mr.labels_with_scope(base_mr.defs.MrScope.READY_FOR_MERGE),
            ['Acks::OK', 'CKI::x86_64::OK', 'CKI::OK']
        )

        self.assertCountEqual(
            test_mr.labels_with_scope(base_mr.defs.MrScope.NEEDS_REVIEW),
            []
        )


class TestCachedProperties(KwfTestCase):
    """Tests for the BaseMR cached properties."""

    @mock.patch.object(GitlabGraph, 'user_can_merge')
    def test_basemr_author_can_merge(self, mock_user_can_merge) -> None:
        """Returns True if GitlabGraph.user_can_merge says so, otherwise False."""
        test_session = self.base_session()
        test_session.get_graphql = mock.Mock(return_value=GitlabGraph(get_user=False))

        # No MR author, returns False.
        test_mr = base_mr.BaseMR(session=test_session, url=MR_URL)
        self.assertFalse(test_mr.author_can_merge)
        mock_user_can_merge.assert_not_called()

        # MR author, returns user_can_merge().
        test_author = User(username='test_user')
        test_mr = base_mr.BaseMR(session=test_session, url=MR_URL, author=test_author)
        self.assertEqual(test_mr.author_can_merge, mock_user_can_merge.return_value)


# Taken from cki-project/kernel-workflow!1705
READY_MRS_QUERY = """
query readyMRs($namespace: ID!, $branch: String!, $label_name: String!, $skip_files: Boolean = false, $after: String = "") {
  project(fullPath: $namespace) {
    mergeRequests(
      state: opened
      targetBranches: [$branch]
      labelName: [$label_name]
      not: {labelName: ["NoCommits", "Oversize", "ZStreamBuild"]}
    ) {
      nodes {
        ...BaseMR
      }
    }
  }
}
""" + base_mr.BASE_MR_FIELDS  # noqa: E501


class TestBaseMRLoading(KwfTestCase):
    """Test loading from json data."""

    def setUp(self) -> None:
        """Set up the basics."""
        super().setUp()

        self.test_session = self.base_session()
        self.response_gql_user_data()

    def setup_query_response(
        self,
        query_result: dict | str,
        query: str = base_mr.BaseMR.MR_QUERY,
        variables: typing.Optional[dict] = None,
    ) -> 'Response':
        """Set up a Response with the given query."""
        if isinstance(query_result, str):
            query_result = self.load_yaml_asset(sub_module='gitlab_graphql_api', path=query_result)

        return self.add_query(query, variables, query_result)

    def test_basemr_loading(self) -> None:
        """Loads the expected data."""
        mr_url = GitlabURL('https://gitlab.com/cki-project/kernel-ark/-/merge_requests/3204')

        # Set up the BaseMR query response.
        query_result = self.load_yaml_asset(
            path='basemr_kernel-ark_3204.json',
            sub_module='gitlab_graphql_api'
        )
        variables = {'namespace': mr_url.namespace, 'mr_id': str(mr_url.id)}

        # Add the base_mr query response to responses.
        self.setup_query_response(query_result=query_result, variables=variables)

        test_mr = base_mr.BaseMR.new(self.test_session, mr_url=mr_url)

        mr_dict = query_result['data']['project']['mr']

        self.assertIs(test_mr.session, self.test_session)
        self.assertEqual(test_mr.url, mr_url)
        self.assertEqual(test_mr.raw_data, mr_dict)

        self.assertTrue(test_mr.approved)
        self.assertEqual(test_mr.author.username, 'ptalbert')
        self.assertEqual(test_mr.commit_count, 1)
        self.assertEqual(test_mr.description.text, mr_dict['description'])
        self.assertFalse(test_mr.draft)
        self.assertEqual(test_mr.global_id, mr_dict['global_id'])
        self.assertEqual(test_mr.head_pipeline_gid.id, 1345634080)
        self.assertCountEqual(
            test_mr.labels,
            ['Acks::OK', 'CKI::OK', 'Merge::OK', 'readyForMerge']
        )
        self.assertEqual(test_mr.state, base_mr.defs.MrState.OPENED)
        self.assertEqual(test_mr.source_branch, mr_dict['sourceBranch'])
        self.assertEqual(test_mr.target_branch, mr_dict['targetBranch'])
        self.assertEqual(test_mr.title, mr_dict['title'])
        self.assertEqual(test_mr.project_gid.id, 13604247)
        self.assertEqual(test_mr.files, ['.gitlab-ci.yml'])
        self.assertFalse(test_mr.has_depends)
        self.assertEqual(test_mr.iid, mr_url.id)
        self.assertEqual(test_mr.namespace, mr_url.namespace)

    def test_new_from_query(self) -> None:
        """Constructs a list of BaseMR instances from the given query."""
        # Set up the BaseMR query response.
        variables = {
            'namespace': 'cki-project/kernel-ark',
            'branch': 'os-build',
            'label_name': 'readyForMerge'

        }

        # Add the base_mr query response to responses.
        self.setup_query_response(
            query_result='basemr_multi_query_kernel-ark_3591.json',
            query=READY_MRS_QUERY,
            variables=variables
        )

        mrs = base_mr.BaseMR.new_from_query(
            self.test_session,
            READY_MRS_QUERY,
            variables
        )

        self.assertEqual(len(mrs), 1)

        result_mr = mrs[0]

        self.assertIsInstance(result_mr, base_mr.BaseMR)
        self.assertEqual(
            result_mr.url,
            'https://gitlab.com/cki-project/kernel-ark/-/merge_requests/3591'
        )


class TestBaseMrBlockingMethods(KwfTestCase):
    """Tests for the BaseMR 'blocks' API stuff."""

    def setUp(self):
        """Set up a BaseMR instance."""
        super().setUp()

        test_session = self.session_runner('signoff')
        mock_gl = mock.Mock()
        test_session.get_gl_instance = mock_gl
        test_session.get_graphql = mock.Mock()

        test_mr = base_mr.BaseMR(
            session=test_session,
            url=GitlabURL('https://example.net/group/project/-/merge_requests/111'),
            project_gid=GitlabGID('gid://gitlab/Project/54321')
        )

        self.test_session = test_session
        self.mock_gl = mock_gl
        self.test_mr = test_mr

    def test_blocking_data(self) -> None:
        """Returns the raw blocking data response from the REST API."""
        self.assertEqual(
            self.test_mr._blocking_data(),
            self.mock_gl.return_value.http_list.return_value
        )

        self.mock_gl.assert_called_once_with('https://example.net')

        expected_endpoint = '/projects/54321/merge_requests/111/blocks'
        self.mock_gl.return_value.http_list.assert_called_once_with(expected_endpoint)

    def test_add_blocking_mr_production(self) -> None:
        """Returns the first result of the http post to the 'blocks' endpoint."""
        self.test_session.is_production_or_staging = True

        self.mock_gl.return_value.http_post.return_value = {'a': 'b'}

        self.assertEqual(
            self.test_mr.add_blocking_mr(GitlabGID('gid://gitlab/MergeRequest/55')),
            {'a': 'b'}
        )

        self.mock_gl.assert_called_once_with('https://example.net')

        expected_endpoint = '/projects/54321/merge_requests/111/blocks?blocking_merge_request_id=55'
        self.mock_gl.return_value.http_post.assert_called_once_with(expected_endpoint)

    def test_add_blocking_mr_bad_response(self) -> None:
        """Returns None if the API call raises an exception."""
        self.test_session.is_production_or_staging = True

        self.mock_gl.return_value.http_post.side_effect = GitlabHttpError(response_code=400)

        self.assertIsNone(self.test_mr.add_blocking_mr(GitlabGID('gid://gitlab/MergeRequest/22')))

        self.mock_gl.assert_called_once_with('https://example.net')

        expected_endpoint = '/projects/54321/merge_requests/111/blocks?blocking_merge_request_id=22'
        self.mock_gl.return_value.http_post.assert_called_once_with(expected_endpoint)

    def test_add_blocking_mr_development(self) -> None:
        """Returns an empty dict."""
        self.test_session.is_production_or_staging = False
        self.assertEqual(
            self.test_mr.add_blocking_mr(GitlabGID('gid://gitlab/MergeRequest/999')),
            {}
        )
        self.mock_gl.return_value.http_post.assert_not_called()

    def test_remove_blocking_id_development(self) -> None:
        """Does nothing at all."""
        self.test_session.is_production_or_staging = False
        self.test_mr.remove_blocking_id(9999999)
        self.mock_gl.return_value.http_delete.assert_not_called()

    def test_remove_blocking_id_bad_response(self) -> None:
        """Swallows any bad response Exception."""
        self.test_session.is_production_or_staging = True

        self.mock_gl.return_value.http_delete.side_effect = GitlabHttpError(response_code=400)
        self.test_mr.remove_blocking_id(53456)
        self.mock_gl.assert_called_once_with('https://example.net')

        expected_endpoint = '/projects/54321/merge_requests/111/blocks/53456'
        self.mock_gl.return_value.http_delete.assert_called_once_with(expected_endpoint)

    def test_remove_blocking_id_production(self) -> None:
        """Makes the http_delete call."""
        self.test_session.is_production_or_staging = True

        self.test_mr.remove_blocking_id(941545)

        self.mock_gl.assert_called_once_with('https://example.net')

        expected_endpoint = '/projects/54321/merge_requests/111/blocks/941545'
        self.mock_gl.return_value.http_delete.assert_called_once_with(expected_endpoint)


class TestHelpers(KwfTestCase):
    """Tests for the non-BaseMR class functions."""

    def test_check_mr_query_results(self) -> None:
        """Returns True if there is MR data in the input dict, otherwise False."""
        self.assertFalse(base_mr.check_mr_query_results({}))
        self.assertFalse(base_mr.check_mr_query_results({'project': {}}))
        self.assertFalse(base_mr.check_mr_query_results({'project': {'mr': None}}))
        self.assertFalse(base_mr.check_mr_query_results({'project': {'mr': {}}}))
        self.assertTrue(base_mr.check_mr_query_results({'project': {'mr': {'iid': 123}}}))
