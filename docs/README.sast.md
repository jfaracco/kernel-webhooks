# Kernel SAST webhook

## Notifications

This webhook labels the merge requests with the current status of the SAST
(Static Analysis Security Testing) scan performed by ProdSec.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.sast \
    --merge-request https://gitlab.com/group/repo/-/merge_requests/1

Before you execute this command, please set the following environment variable
to the REST API address of ProdSec's automation server:

    export KERNEL_SAST_AUTOMATION_SERVER_REST_API=...

The [main README][1] describes some common environment variables that can be set
that are applicable for all webhooks.

## Reporting

- Label prefix: `SAST::`
- Comment header: **Kernel SAST Scan Report**

The webhook reports the overall result of the scan by applying a scoped label
to the MR with the prefix `SAST::`.

Possible statuses are:

- pending - the scan has not started yet
- scanning - the scanning is in progress
- finished\_w\_findings - the scan has finished and there are >0 findings
- finished\_no\_findings - the scan has finished and there are no findings
- failed - the scan has failed. In such cases, please contact the ProdSec RHEL
  security architects at the prodsec-rhel-secarch mailing list / group.

Additionally the findings will be listed in an easy to review format as a
comment, marked by the title **Kernel SAST Scan Report**. Next to each finding
in the comment, there are an "X" and a checkmark, which are links for creating a
Jira issue. Pressing on the "X" will open a Jira issue to the ProdSec team, in
order to mark it as false positive. Pressing on the checkmark will open a Jira
issue to the RHEL project for correcting the finding, thus treating the finding
as a true positive.

## Triggering

To trigger reevaluation of an MR by the SAST webhook either remove any
existing `SAST::` scoped label or leave a comment with one of the following:

- `request-sast-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.

## Purpose

As part of [RH-SDL][2] ProdSec performs SAST scans of all RHEL components,
including the kernel. Due to its complexity, ProdSec has developed a special
algorithm, which performs SAST scans on MR basis. These are done using
[OpenScanHub][3] and a special automation running on a dedicated machine, where
also the results are stored. By reviewing the findings for each MR, the kernel
team fulfills the RH-SDL requirements.

[1]: README.md#running-a-webhook-for-one-merge-request
[2]: https://spaces.redhat.com/pages/viewpage.action?pageId=330178595
[3]: https://spaces.redhat.com/display/PRODSEC/OpenScanHub
