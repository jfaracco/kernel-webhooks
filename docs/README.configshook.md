# Red Hat Kernel Config Evaluation Webhook

## Purpose

This webhook evaluates kernel config snippets provided in merge requests,
checking to see if they match kernel-ark where expected, attempting to catch
obvious errors, and otherwise showing the MR author and reviewers what the
resulting merged kernel configs look like with the provided snippet(s).

## Reporting

- Label prefix: `Configs::`
- Comment header: **Kernel Configuration Evaluation**

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `Configs::`.

The hook will leave a comment on the MR with the details of the check. The
header of the comment is **Kernel Configuration Evaluation**. This comment will
be edited with updated information every time the hook runs. Refer to the timestamp
at the end of the comment to see when the hook last evaluated the MR.

## Triggering

To trigger reevaluation of an MR by the configshook webhook either remove any
existing `Configs::` scoped label or leave a comment with one of the following:

- `request-configs-evaluation`
- `request-configshook-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.

## Manual runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.configshook \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --rhkernel-src /usr/src/linux

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--rhkernel-src` argument can also be passed to the script via the `RHKERNEL_SRC`
environment variable.
